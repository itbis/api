package com.telkomsel.itvas.umbx.actions;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;
import com.telkomsel.itvas.umbx.actions.converter.AbstractConverter;
import com.telkomsel.itvas.umbx.actions.validator.AbstractValidator;

import net.n3.nanoxml.IXMLElement;

public class InputAction extends Action {
	private String content;
	private String var;
	private String validator;
	private String converter;
	private String backToStart;
	
	public InputAction(IXMLElement element) throws UmbAutoException {
		content = element.getContent();
		validator = element.getAttribute("validator", null);
		converter = element.getAttribute("converter", null);
		var = element.getAttribute("var", null);
		backToStart = element.getAttribute("backToStart", null);
		if (backToStart == null) {
			backToStart = "0";
		}
		if (content == null) {
			throw new UmbAutoException("[InputFlow] attr not complete : content");
		}
		content = content.trim().replaceAll("\\[BR\\]", NEWLINE);
		if (var == null) {
			throw new UmbAutoException("[InputFlow] attr not complete : var");
		}
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		if (backToStart.equals("1")) {
			first = true;
		}
		String strFirst = (first) ? "Yes" : "No";
		return formatResult(formatState("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
				+ "<data>"+content+"</data>", state)).trim();
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		if (backToStart.equals("1")) {
			return 99;
		}
		if (command.equals("9")) {
			return -1;
		} else {
			if (validator != null) {
				try {
					Class c = Class.forName(validator);
					Constructor constructor = c.getConstructor();
					AbstractValidator objValidator = (AbstractValidator) constructor.newInstance();
					objValidator = (AbstractValidator) constructor.newInstance();
					if (!objValidator.validateInput(command)) {
						return 0;
					}
				} catch (Exception e) {
					log.error("Validator error : " + validator, e);
				}
			}
			
			if (converter != null) {
				try {
					Class c = Class.forName(converter);
					Constructor constructor = c.getConstructor();
					AbstractConverter objConverter = (AbstractConverter) constructor.newInstance();
					objConverter = (AbstractConverter) constructor.newInstance();
					command = objConverter.convert(command);
				} catch (Exception e) {
					log.error("Validator error : " + validator, e);
				}
			}
			state.pushSessionData(var, command);
			return 1;
		}
	}
}
