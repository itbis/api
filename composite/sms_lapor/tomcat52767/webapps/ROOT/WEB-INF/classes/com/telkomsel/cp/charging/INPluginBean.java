package com.telkomsel.cp.charging;

import java.io.*;
import java.util.*;
import java.net.*;

public class INPluginBean
{
	String dodol;

	public INPluginBean()
	{
	}

    public static String PluginRequest(String address) throws MalformedURLException, IOException, ClassCastException
    {
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in = url.openStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while((line = br.readLine()) != null)
			{
				res = res.concat(line);
			}
			br.close();
			in.close();
		}
		catch(Exception e)
		{
			res = "dodol";
			System.out.println(new java.util.Date()+" [ERROR] com.telkomsel.cp.charging.INPluginBean.PluginRequest()=" + e.getMessage());
		}
        return res;
    }

    public String Charge(String msisdn, String appsid, String pwd, String cpname, String orderid, String bnum)
    {
		String url = "http://10.2.224.120:8000/inplugin/submit.jsp";
		String c = "";

    	try
    	{
			dodol = PluginRequest(url + "?appsid=" + appsid + "&password=" + pwd + "&msisdn=" + msisdn +"&cpname=" + cpname + "&bnumber=" + bnum + "&orderid=" + orderid);
			if(dodol.equals("dodol"))
			{
				c = msisdn + " 0 101";
			}
			else
			{
				c = dodol;
			}

			System.out.println(new java.util.Date()+" [INFO] com.telkomsel.cp.charging.INPluginBean.Charge(" + msisdn + ", " + cpname + ", " + appsid + ")=" + c);
		}
		catch(Exception e)
		{
			System.out.println(new java.util.Date()+" [ERROR] com.telkomsel.cp.charging.INPluginBean.Charge(" + msisdn + ", " + cpname + ", " + appsid + ")=" + e.getMessage());
			c = msisdn + " 0 101";
		}

		return c;
    }

    public void Refund(String msisdn, String appsid, String pwd, String cpname, String orderid, String bnum, String tid)
    {
		String url = "http://10.1.105.41:8000/inplugin/submit.jsp";
		String r = "";

    	try
    	{
			dodol = PluginRequest(url + "?appsid=" + appsid + "&password=" + pwd + "&msisdn=" + msisdn + "&cpname=" + cpname + "&bnumber=" + bnum + "&orderid=" + orderid + "&tid=" + tid);
			if(dodol.equals("dodol"))
			{
				r = msisdn + " 0 101";
			}
			else
			{
				r = dodol;
			}

			System.out.println(new java.util.Date()+" [INFO] com.telkomsel.cp.charging.INPluginBean.Refund(" + msisdn + ", " + cpname + ", " + appsid + ", " + tid + ")=" + r);
		}
		catch(Exception e)
		{
			System.out.println(new java.util.Date()+" [ERROR] com.telkomsel.cp.charging.INPluginBean.Refund(" + msisdn + ", " + cpname + ", " + appsid + ", " + tid + ")=" + e.getMessage());
			r = msisdn + " 0 101";
		}

		//return r;
    }

    public String Info(String msisdn, String appsid, String pwd, String cpname, String orderid, String bnum)
    {
		String url = "http://10.1.105.41:8000/inplugin/info.jsp";
		String c = "";

    	try
    	{
			dodol = PluginRequest(url + "?appsid=" + appsid + "&password=" + pwd + "&msisdn=" + msisdn +"&cpname=" + cpname + "&bnumber=" + bnum + "&orderid=" + orderid);
			if(dodol.equals("dodol"))
			{
				c = msisdn + " 0 101";
			}
			else
			{
				c = dodol;
			}

			System.out.println(new java.util.Date()+" [INFO] com.telkomsel.cp.charging.INPluginBean.Info(" + msisdn + ", " + cpname + ", " + appsid + ")=" + c);
		}
		catch(Exception e)
		{
			System.out.println(new java.util.Date()+" [ERROR] com.telkomsel.cp.charging.INPluginBean.Info(" + msisdn + ", " + cpname + ", " + appsid + ")=" + e.getMessage());
			c = msisdn + " 0 101";
		}

		return c;
    }
}
