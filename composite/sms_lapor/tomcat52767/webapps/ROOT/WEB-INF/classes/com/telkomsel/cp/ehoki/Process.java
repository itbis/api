package com.telkomsel.cp.ehoki;

import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class Process
{
	public Process()
	{
	}

    public String sendRT(String address) throws MalformedURLException, IOException, ClassCastException
    {
        URL url = new URL(address);
		InputStream in = url.openStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line = "";
		String res = "";
		while((line = br.readLine()) != null)
		{
			res = res.concat(line);
		}
		br.close();

        return res;
    }

    public String Parse(String url, String msisdn, String smscode, String smsid, String type) throws IOException, JDOMException, NullPointerException
    {
		String result = "";

		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document myDocument = builder.build(url);

			Element root = myDocument.getRootElement();

			List allChild = root.getChildren();
			Iterator it = allChild.iterator();

			Element stChild = root.getChild("status");
			String status = stChild.getText();
			Element rtChild = root.getChild("rtpacket");
			//String totalpacket = rtChild.getAttributeValue("totalpacket");

			String to = "";
			String toNotif = "";
			String textError = "";
			String textError1 = "";
			String textNotif1 = "";
			String textNotif = "";
			String urls = "";
			String urlsNotif = "";


			if(status.equals("02"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "Sorry, there is a problem in our internal system. Please try again later.";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK02";
			}
			else if(status.equals("01"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "You are entered invalid keycode ("+smscode+")";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK01";
			}
			else if(status.equals("00"))
			{
				List rtp = rtChild.getChildren();
				Iterator i = rtp.iterator();
				while(i.hasNext())
				{
					Element data = (Element)i.next();

					String udh = data.getAttributeValue("udh");
					String src = data.getAttributeValue("msgsrc");

					String u = "";
					String d = "";

					for(int m = 0; m < udh.length(); m++)
					{
						if((m % 2) == 0)
						{
							u = u + "%" + udh.charAt(m);
						}
						else
						{
							u = u + udh.charAt(m);
						}
					}

					for(int n = 0; n < src.length(); n++)
					{
						if((n % 2) == 0)
						{
							d = d + "%" + src.charAt(n);
						}
						else
						{
							d = d + src.charAt(n);
						}
					}

					to = URLEncoder.encode(msisdn, "UTF-8");
					urls = "http://10.1.84.12:13013/cgi-bin/sendsms?username=dodol&password=dodol&to=" + to + "&from=6767&coding=2&udh=" + u + "&text=" + d;
					String send = sendRT(urls);
				}

				toNotif = URLEncoder.encode(msisdn, "UTF-8");
				textNotif1 = "Your request "+type+" "+smscode+" has been sent";
				textNotif = URLEncoder.encode(textNotif1, "UTF-8");
				urlsNotif = "http://10.1.84.12:13014/cgi-bin/sendsms?username=dodol&password=dodol&to="+toNotif+"&from=6767&text="+textNotif;
				sendRT(urlsNotif);

				result = "OK";
			}
		}
		catch(Exception se)
		{
			se.printStackTrace();
			result = "NOK";
		}
		return result;
    }

    public String ParseFriend(String url, String msisdn, String smscode, String smsid, String msisdnTo, String type) throws IOException, JDOMException, NullPointerException
    {
		String result = "";

		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document myDocument = builder.build(url);

			Element root = myDocument.getRootElement();

			List allChild = root.getChildren();
			Iterator it = allChild.iterator();

			Element stChild = root.getChild("status");
			String status = stChild.getText();
			Element rtChild = root.getChild("rtpacket");
			//String totalpacket = rtChild.getAttributeValue("totalpacket");

			String to = "";
			String toNotif = "";
			String textError = "";
			String textError1 = "";
			String textNotif1 = "";
			String textNotif = "";
			String urls = "";
			String urlsNotif = "";


			if(status.equals("02"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "Sorry, there is a problem in our internal system. Please try again later.";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK02";
			}
			else if(status.equals("01"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "You are entered invalid keycode ("+smscode+")";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK01";
			}
			else if(status.equals("00"))
			{
				List rtp = rtChild.getChildren();
				Iterator i = rtp.iterator();
				while(i.hasNext())
				{
					Element data = (Element)i.next();

					String udh = data.getAttributeValue("udh");
					String src = data.getAttributeValue("msgsrc");

					String u = "";
					String d = "";

					for(int m = 0; m < udh.length(); m++)
					{
						if((m % 2) == 0)
						{
							u = u + "%" + udh.charAt(m);
						}
						else
						{
							u = u + udh.charAt(m);
						}
					}

					for(int n = 0; n < src.length(); n++)
					{
						if((n % 2) == 0)
						{
							d = d + "%" + src.charAt(n);
						}
						else
						{
							d = d + src.charAt(n);
						}
					}

					to = URLEncoder.encode(msisdnTo, "UTF-8");
					urls = "http://10.1.84.12:13013/cgi-bin/sendsms?username=dodol&password=dodol&to=" + to + "&from=6767&coding=2&udh=" + u + "&text=" + d;
					String send = sendRT(urls);
				}

				toNotif = URLEncoder.encode(msisdn, "UTF-8");
				textNotif1 = "Your request "+type+" "+smscode+" has been sent";
				textNotif = URLEncoder.encode(textNotif1, "UTF-8");
				urlsNotif = "http://10.1.84.12:13014/cgi-bin/sendsms?username=dodol&password=dodol&to="+toNotif+"&from=6767&text="+textNotif;
				sendRT(urlsNotif);

				result = "OK";
			}
		}
		catch(Exception se)
		{
			se.printStackTrace();
			result = "NOK";
		}
		return result;
    }

    public String ParseCT(String url, String msisdn, String smscode, String smsid, String type) throws IOException, JDOMException, NullPointerException
    {
		String result = "";

		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document myDocument = builder.build(url);

			Element root = myDocument.getRootElement();

			List allChild = root.getChildren();
			Iterator it = allChild.iterator();

			Element stChild = root.getChild("status");
			String status = stChild.getText();
			Element rtChild = root.getChild("rtpacket");
			//String totalpacket = rtChild.getAttributeValue("totalpacket");

			String to = "";
			String toNotif = "";
			String textError = "";
			String textError1 = "";
			String textNotif1 = "";
			String textNotif = "";
			String urls = "";
			String urlsNotif = "";


			if(status.equals("02"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "Sorry, there is a problem in our internal system. Please try again later.";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK02";
			}
			else if(status.equals("01"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "You are entered invalid keycode ("+smscode+")";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK01";
			}
			else if(status.equals("00"))
			{
				List rtp = rtChild.getChildren();
				Iterator i = rtp.iterator();
				while(i.hasNext())
				{
					Element data = (Element)i.next();

					String udh = data.getAttributeValue("udh");
					String src = data.getAttributeValue("msgsrc");

					to = URLEncoder.encode(msisdn, "UTF-8");
					urls = "http://10.1.84.12:13013/cgi-bin/sendsms?username=dodol&password=dodol&to=" + to + "&from=6767&text=" + src;
					String send = sendRT(urls);
				}

				toNotif = URLEncoder.encode(msisdn, "UTF-8");
				textNotif1 = "Your request "+type+" "+smscode+" has been sent";
				textNotif = URLEncoder.encode(textNotif1, "UTF-8");
				urlsNotif = "http://10.1.84.12:13014/cgi-bin/sendsms?username=dodol&password=dodol&to="+toNotif+"&from=6767&text="+textNotif;
				sendRT(urlsNotif);

				result = "OK";
			}
		}
		catch(Exception se)
		{
			se.printStackTrace();
			result = "NOK";
		}
		return result;
    }

    public String ParseFriendCT(String url, String msisdn, String smscode, String smsid, String msisdnTo, String type) throws IOException, JDOMException, NullPointerException
    {
		String result = "";

		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document myDocument = builder.build(url);

			Element root = myDocument.getRootElement();

			List allChild = root.getChildren();
			Iterator it = allChild.iterator();

			Element stChild = root.getChild("status");
			String status = stChild.getText();
			Element rtChild = root.getChild("rtpacket");
			//String totalpacket = rtChild.getAttributeValue("totalpacket");

			String to = "";
			String toNotif = "";
			String textError = "";
			String textError1 = "";
			String textNotif1 = "";
			String textNotif = "";
			String urls = "";
			String urlsNotif = "";


			if(status.equals("02"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "Sorry, there is a problem in our internal system. Please try again later.";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK02";
			}
			else if(status.equals("01"))
			{
				to = URLEncoder.encode(msisdn, "UTF-8");
				textError1 = "You are entered invalid keycode ("+smscode+")";
				textError = URLEncoder.encode(textError1, "UTF-8");
				urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&from=6767&text="+textError;
				sendRT(urls);

				result = "OK01";
			}
			else if(status.equals("00"))
			{
				List rtp = rtChild.getChildren();
				Iterator i = rtp.iterator();
				while(i.hasNext())
				{
					Element data = (Element)i.next();

					String udh = data.getAttributeValue("udh");
					String src = data.getAttributeValue("msgsrc");

					to = URLEncoder.encode(msisdnTo, "UTF-8");
					urls = "http://10.1.84.12:13013/cgi-bin/sendsms?username=dodol&password=dodol&to=" + to + "&from=6767&text=" + src;
					String send = sendRT(urls);
				}

				toNotif = URLEncoder.encode(msisdn, "UTF-8");
				textNotif1 = "Your request "+type+" "+smscode+" has been sent";
				textNotif = URLEncoder.encode(textNotif1, "UTF-8");
				urlsNotif = "http://10.1.84.12:13014/cgi-bin/sendsms?username=dodol&password=dodol&to="+toNotif+"&from=6767&text="+textNotif;
				sendRT(urlsNotif);

				result = "OK";
			}
		}
		catch(Exception se)
		{
			se.printStackTrace();
			result = "NOK";
		}
		return result;
    }

	public String Notif(String msisdn, String msg) throws IOException, NullPointerException
    {
		String result = "";

		try
		{
			String to = "";
			String urls = "";
			to = URLEncoder.encode(msisdn, "UTF-8");
			urls = "http://10.1.84.12:13015/cgi-bin/sendsms?username=dodol&password=dodol&to="+to+"&text="+msg;
			sendRT(urls);

			result = "OK";
		}
		catch(Exception se)
		{
			se.printStackTrace();
			result = "NOK";
		}
		return result;
    }
}