package com.telkomsel.itvas.umbx;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.log4j.Logger;
import net.n3.nanoxml.IXMLElement;
import net.n3.nanoxml.IXMLParser;
import net.n3.nanoxml.IXMLReader;
import net.n3.nanoxml.StdXMLReader;
import net.n3.nanoxml.XMLParserFactory;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;
import com.telkomsel.itvas.database.MysqlFacade;
import com.telkomsel.itvas.umbx.actions.Action;
import com.telkomsel.itvas.webstarter.WebStarterProperties;

public class Flow {
	private ArrayList<Action> actions = new ArrayList<Action>();
	private Logger log = Logger.getLogger(this.getClass());
	private QueryRunner qr;
	private String flowId;
	private String checkRequest = null;
	public static Hashtable<String, CState> states = new Hashtable<String, CState>();
	public static int sessionTimeout = 20;
	
	static {
		CleanupThread ct = new CleanupThread();
		ct.start();
		
		sessionTimeout = WebStarterProperties.getInstance().getIntProperty("umbx.session.timeout");
	}
	
	public Flow(String flowId) throws Exception {
		
		Connection conn = null;
		String strXml = "";
		try {
			conn = MysqlFacade.getConnection();
			String q = "SELECT xml FROM xml_resource WHERE flow_id=?";
			PreparedStatement ps = conn.prepareStatement(q);
			ps.setString(1, flowId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				strXml = rs.getString("xml");
			} else {
				throw new Exception("appId not defined in DB : " + flowId);
			}
			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in FlowScenario constructor : " + e.getMessage());
		} finally {
			DbUtils.closeQuietly(conn);
		}
		this.flowId = flowId;
		process(strXml);
	}

	private void process(String strXml) throws Exception {
		IXMLParser parser = XMLParserFactory.createDefaultXMLParser();
		IXMLReader reader = StdXMLReader.stringReader(strXml);
		parser.setReader(reader);
		IXMLElement xml = (IXMLElement) parser.parse();
		checkRequest = xml.getAttribute("checkRequest", null);
		Enumeration enumeration = xml.enumerateChildren();
		while (enumeration.hasMoreElements()) {
			IXMLElement element = (IXMLElement) enumeration.nextElement();
			if (element.getFullName().equalsIgnoreCase("flow") || element.getFullName().equalsIgnoreCase("action")) {
				Action f = ActionFactory.getAction(element);
				actions.add(f);
			}
		}
		if (actions.size() == 0) {
			throw new UmbAutoException("No flow entry found");
		}
	}

	public String exec(String msisdn, String command, HttpServletRequest request) throws Exception {
		String retval = null;
		int step = 0;
		try {
			qr = new QueryRunner();
			
			String strRequest = "";
			if (checkRequest != null) {
				String[] p = checkRequest.split(",");
				for (String pp : p) {
					strRequest += ("|" + request.getParameter(pp));
				}
			}
			String actualFlowId = (strRequest.equals("")) ? flowId : flowId + strRequest;
			
			if (command.equals("0")) {
				states.remove(msisdn);
//				String q = "UPDATE c_state SET expired_ts='0000-00-00 00:00:00' WHERE msisdn=?";
//				PreparedStatement ps = conn.prepareStatement(q);
//				ps.setString(1, msisdn);
//				ps.executeUpdate();
//				ps.close();
				return "EXPIRED";
			}
			
			CState state = states.get(msisdn);
			int i = -1;
			if (state == null) {
				state = new CState(msisdn, actualFlowId);
				state.setState(-1);
				state.setCurrentPage(0);
			} else {
				if (!state.getAppId().equals(actualFlowId) || (state.getCurMilis() + (sessionTimeout * 1000) < System.currentTimeMillis())) {
					state.setAppId(actualFlowId);
					state.setState(-1);
					state.setCurrentPage(0);
				} else {
					i = state.getState();
				}
			}
			
			
			
//			String q = "SELECT state,msisdn,flow_id AS appID, current_page AS currentPage, session_data as sessionData FROM c_state WHERE msisdn=? AND flow_id=? AND expired_ts > NOW()";
//			PreparedStatement ps = conn.prepareStatement(q);
//			ps.setString(1, msisdn);
//			ps.setString(2, actualFlowId);
//			curMilis = System.currentTimeMillis();
//			ResultSet rs = ps.executeQuery();
//			log.info("LOG Flow exec() SQL 1 : " + (System.currentTimeMillis() - curMilis) + ", msisdn : " + msisdn);
//			CState state = new CState(msisdn, flowId);
			
//			if (rs.next()) {
//				state.setState(rs.getInt("state"));
//				state.setCurrentPage(rs.getInt("currentPage"));
//				state.setSessionBytes(rs.getBytes("sessionData"));
//				i = state.getState();
//			}
//			rs.close();
//			ps.close();
			
			if (checkRequest != null) {
				String[] p = checkRequest.split(",");
				log.debug("checkRequest : " + checkRequest);
				for (String pp : p) {
					String value = request.getParameter(pp);
					if (value == null) {
						throw new Exception("GET Parameter not supplied : " + pp);
					} else {
						state.pushSessionData(pp, request.getParameter(pp));
					}
				}
			}
			
			boolean first = true;
			if (i > -1) { // not first
				first = false;
				Action f = null;
				f = actions.get(i);
				Monitor mon = MonitorFactory.startPrimary("umbx.post." + flowId + "." + i);
				int stepAddition = f.postProcess(state, command, first);
				mon.stop();
				if (stepAddition == 99) {
					i = 0;
				} else {
					i += stepAddition;
				}
				if (i == actions.size()) {
					i = 0;
				} else if (i == -1) {
					i = 0;
				}
			} else {
				i = 0;
			}
			step = i;
			Action f = actions.get(i);
			Monitor mon = MonitorFactory.startPrimary("umbx.pre." + flowId + "." + i);
			retval = f.preProcess(state, command, first);
			mon.stop();
			
			if (retval.equals("PASSTHROUGH")) { // passthrough action
				int limit = 10, counter = 0;
				do {
					counter++;
					i++;
					f = actions.get(i);
					retval = f.preProcess(state, command, first);
				} while ((retval.equals("PASSTHROUGH")) && (counter < limit));
			}
			state.setCurMilis(System.currentTimeMillis());
			state.setState(i);
			states.put(msisdn, state);
//			q = "INSERT INTO c_state (msisdn, flow_id, state, expired_ts, session_data, current_page) VALUES (?,?,?,ADDDATE(NOW(), INTERVAL 15 SECOND),?,?)" +
//					"ON DUPLICATE KEY UPDATE state=?, expired_ts=ADDDATE(NOW(), INTERVAL 15 SECOND), session_data=?, current_page=?, flow_id=?";
//			curMilis = System.currentTimeMillis();
//			qr.update(conn, q, new Object[] {msisdn, actualFlowId, i, state.getSessionBytes(), state.getCurrentPage(),i,state.getSessionBytes(), state.getCurrentPage(), actualFlowId});
		} catch (Exception e) {
			if (!(e instanceof NullPointerException)) {
				log.error("Error in exec() : " + e.getMessage(), e);
			}
			throw e;
		} finally {
//			DbUtils.closeQuietly(conn);
		}
		String retval2 = retval.replaceAll(System.getProperty("line.separator"), "");
		log.info(msisdn + "|" + flowId + "|" + step + "|" + retval2);
		return retval;
	}
	
}

class CleanupThread extends Thread {
	public void run() {
		for (;;) {
			if (Flow.states != null) {
				Flow.states.clear();
				Flow.states = null;
				Flow.states = new Hashtable<String, CState>();
			}
			try {
				Thread.sleep(86400000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
