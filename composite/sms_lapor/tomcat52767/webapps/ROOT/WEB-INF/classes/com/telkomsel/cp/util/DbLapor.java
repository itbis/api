package com.telkomsel.cp.util;

import javax.naming.*;
import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public class DbLapor
{
	private Context ctx;
	private DataSource ds;
    private Connection dbConn;
    private Statement stmt;
    private ResultSet rs;
	private String query;
	private PreparedStatement pstmt;
	private boolean b;
	private Vector v;
	private String smsid;
	private int id;
	private String trxid;
	private String keyword;

	java.util.Date skr;
	SimpleDateFormat formatter;
	String tdate;

    public DbLapor()
    {
		try
    	{
      		ctx = new InitialContext();
      		if(ctx == null)
          		throw new Exception("Boom - No Context");

      		ds = (DataSource)ctx.lookup("java:comp/env/jdbc/lapor");
	    }
	    catch(Exception e)
	    {
      		e.printStackTrace();
    	}
    }

    public Connection createDBConnection() throws IOException, SQLException
    {
		if(ds != null)
		{
	   		dbConn = ds.getConnection();
        }
		else
		{
			System.out.println(" ds null");
		}
        return dbConn;
    }

    public void closeDBConnection()
    {
        try
        {
            if(dbConn != null)
			{
				dbConn.close();
				dbConn = null;
				System.out.println(" Close CP DbLapor Connection");
            }
        }
        catch(Exception exception)
        {
            System.out.println("!!!!!! [ ERROR ] Close CP DbLapor Connection Exception: " + exception.getMessage());
        }
    }

    public ResultSet doQuery(String s) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DbLapor Connection Insert : "+dbConn);
        stmt = dbConn.createStatement();
        rs = stmt.executeQuery(s);

        return rs;
    }

    public void doUpdate(String s) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DbLapor Connection Update : "+dbConn);
        stmt = dbConn.createStatement();
        stmt.executeUpdate(s);
        stmt.close();
    }

	public void insertCPReport(String charge_type, String cp_name, String msisdn, String smsid, String contentid, String oa, String trid, String status)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			if(charge_type.equals("MT"))
				query = "INSERT INTO cp_report VALUES(?,?,?,?,?,now(),'N','',?,?)";
			else
				query = "INSERT INTO cp_reportMO VALUES(?,?,?,?,?,now(),'N','',?,?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, msisdn);
			pstmt.setString(3, smsid);
			pstmt.setString(4, contentid);
			pstmt.setString(5, oa);
			pstmt.setString(6, trid);
			pstmt.setString(7, status);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }


	public String selectKeywordFromContentReq(String cp_name, String adn, String k)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT keyword FROM content_req WHERE cp_name = ? AND adn = ? and keyword = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, adn);
			pstmt.setString(3, k);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				keyword = rs.getString("keyword");
			}
			else
			{
				keyword = "dodol";
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			keyword = "dodol";
		}

		return keyword;
    }

}
