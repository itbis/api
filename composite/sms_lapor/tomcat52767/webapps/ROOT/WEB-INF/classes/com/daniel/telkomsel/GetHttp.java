package com.daniel.telkomsel;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import java.io.*;
import java.net.*;
import java.util.*;


public class GetHttp {

 public static String [] get(String surl, int timeout) {
       String ret [] = new String[2];
       String content = "";
        ret[0] = "NOK";

org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
  client.setConnectionTimeout(timeout);
  client.setTimeout(timeout);
    GetMethod method = new GetMethod(surl);
method.setHttp11(false);

    DefaultMethodRetryHandler retryhandler = new DefaultMethodRetryHandler();
    retryhandler.setRequestSentRetryEnabled(false);
    //retryhandler.setRetryCount(3);
    method.setMethodRetryHandler(retryhandler);

    try {
      int statusCode = client.executeMethod(method);

      if (statusCode != HttpStatus.SC_OK) {
        System.out.println("Method failed: " + method.getStatusLine()+" "+surl);
        ret[0] = "NOK";
        ret[1] = method.getStatusLine().toString()+" "+method.getResponseBodyAsString();
      } else { 

      // Read the response body.

      ret[1] = method.getResponseBodyAsString();
      ret[0] = "OK";
        System.out.println(surl+" content:"+ret[1]);
	}

    } catch (Exception e) {
      System.out.println("common Http get :"+e.getMessage()+" "+surl);
      e.printStackTrace(System.out);
        ret[1] = e.toString();

    } finally {
      // Release the connection.
      method.releaseConnection();
    }


        return ret;
}


}
