package com.telkomsel.itvas.ws;

import org.apache.axis.encoding.DeserializerImpl;
import  org.apache.axis.Constants;
import org.apache.axis.encoding.DeserializationContext;
import  org.apache.axis.encoding.Deserializer;
import org.apache.axis.encoding.FieldTarget;
import  org.apache.axis.message.SOAPHandler;
import org.xml.sax.Attributes;
import  org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import  java.util.Hashtable;

/**
 *
 *
 */
public  class UMBServiceResponseDeserializer extends DeserializerImpl {
         public  static final String STATUS = "Status";
         public  static final String ERRORCODE = "ErrorCode";
         public  static final String ERRORMESSAGE = "ErrorMessage";
         public  static final QName myTypeQName = new QName("http://www.openuri.org/", "UMBServiceResponse");

         private  Hashtable typesByMemberName = new Hashtable();  

         public  UMBServiceResponseDeserializer()
         {
                  typesByMemberName.put(STATUS,  Constants.XSD_STRING);
                  typesByMemberName.put(ERRORCODE,  Constants.XSD_STRING);
                  typesByMemberName.put(ERRORMESSAGE,  Constants.XSD_STRING);
                  value = new  UMBServiceResponse("","","");
         }

         /** DESERIALIZER  - event handlers
          */

         /**
          * This  method is invoked when an element start tag is encountered.
          * @param  namespace is the namespace of the element
          * @param  localName is the name of the element
          * @param  prefix is the element's prefix
          * @param  attributes on the element...used to get the type
          * @param  context is the DeserializationContext
          */
         public  SOAPHandler onStartChild(String namespace,
                                                     String  localName,
                                                      String  prefix,
                                                       Attributes  attributes,
                                                      DeserializationContext  context)
                  throws  SAXException
         {
         QName  typeQName = (QName)typesByMemberName.get(localName);
         if  (typeQName == null)
          throw  new SAXException("Invalid element in UMBServiceResponse struct - " + localName);
         
                  // These  can come in either order.
                  Deserializer  dSer = context.getDeserializerForType(typeQName);
                  try {
                  dSer.registerValueTarget(new  FieldTarget(value, localName));
                  } catch  (NoSuchFieldException e) {
                	  System.out.println("haiya : " + value + ", debug : " + localName);
                           throw  new SAXException(e);
                  }
     
                  if (dSer == null)
                           throw  new SAXException("No deserializer for a " + typeQName + "???");
        
                  return  (SOAPHandler)dSer;
         }
}