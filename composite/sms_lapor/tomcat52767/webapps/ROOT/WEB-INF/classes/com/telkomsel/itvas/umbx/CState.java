package com.telkomsel.itvas.umbx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;

public class CState {
	private String msisdn;
	private int state;
	private int currentPage;
	private String appId;
	private String tmpPreMessage;
	private Hashtable<String, String> sessionHash;
	private long curMilis;

	public CState() {

	}

	public CState(String msisdn, String appId) {
		this.msisdn = msisdn;
		this.appId = appId;
		this.state = -1;
		this.currentPage = -1;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public void pushSessionData(String var, String data) {
		if (sessionHash == null) {
			sessionHash = new Hashtable<String, String>(6);
		}
		System.out.println();
		sessionHash.put(var, data);
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void popSessionData(String var) {
		if (sessionHash != null && var != null) {
			sessionHash.remove(var);
		}
	}

	public byte[] getSessionBytes() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream(baos);
		oout.writeObject(sessionHash);
		oout.close();
		return baos.toByteArray();
	}

	public void setSessionBytes(byte[] data) throws IOException,
			ClassNotFoundException {
		if (data != null) {
			ObjectInputStream objectIn = new ObjectInputStream(
					new ByteArrayInputStream(data));
			sessionHash = (Hashtable<String, String>) objectIn.readObject();
		}
	}

	public String getTmpPreMessage() {
		return tmpPreMessage;
	}

	public void setTmpPreMessage(String tmpPreMessage) {
		this.tmpPreMessage = tmpPreMessage;
	}

	public Hashtable<String, String> getSessionHash() {
		return sessionHash;
	}
	
	public String getSession(String key) {
		return sessionHash.get(key);
	}
	
	public void setSession(String key, String value) {
		sessionHash.put(key, value);
	}

	public long getCurMilis() {
		return curMilis;
	}

	public void setCurMilis(long curMilis) {
		this.curMilis = curMilis;
	}
}
