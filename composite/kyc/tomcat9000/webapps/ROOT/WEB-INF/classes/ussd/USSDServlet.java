package ussd;

import javax.servlet.http.*;  
import javax.servlet.*;  
import java.io.*; 
import java.net.URLEncoder;
import com.telkomsel.cp.util.PushLog; 
import com.telkomsel.cp.util.HTTPGet; 
public class USSDServlet extends HttpServlet{  

//GET Method
public void doGet(HttpServletRequest req,HttpServletResponse res)
throws ServletException,IOException
{
        res.setContentType("text/html");//setting the content type
        PrintWriter out=res.getWriter();//get the stream to write the data
	out.println("SMS Apps");
}

//POST Method
public void doPost(HttpServletRequest req,HttpServletResponse res)  
throws ServletException,IOException  
{
	messageReader _msgReader = new messageReader();
	message _msg = new message();
	String response;
	String JResponse;
	String url = "http://10.2.204.198:8083/UssdBroadcastService?";
	PrintWriter out=res.getWriter();//get the stream to write the data  

	try {
		String Result = getBody(req);
		_msg = _msgReader.readJson(Result);
		if (_msg.getMsisdn() != null) {
			String text = URLEncoder.encode(_msg.getMsg(), "UTF-8");
			String opType = "NI";

			if (_msg.getType().equals("1")) opType = "IN";

			url = url + "mms="+_msg.getType()+"&msisdn="+_msg.getMsisdn()+"&operationType="+opType+"&password=adp123&pushReferenceId="+_msg.getTrxid()+"&username=adp&ussdContent="+text+"&ussdShortCode=";
			String encodedUrl = URLEncoder.encode(url, "UTF-8");	
			response = HTTPGet.get(url,0,5000);
			response = response.replaceAll("\n","");
			PushLog.record("USSD:" + url + "|" + response + "|" + _msg.getMsisdn() + "|" + _msg.getType() + "|" + _msg.getMsg() + "|" + _msg.getTrxid());
		
			JResponse = "{\"result\":\"" + response + "\"}";
			out.println();
		}
		else {
			JResponse = "{\"result\":\"Invalid Request (ERR:01)\"}";
			PushLog.record("USSD:Invalid request(ERR:01)" );
		}

		out.println(JResponse);
	}
	catch(Exception e) {
		out.println(e.getMessage());
	}  
}

//READ BODY
public static String getBody(HttpServletRequest request) throws IOException {

    String body = null;
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader bufferedReader = null;

    try {
        InputStream inputStream = request.getInputStream();
        if (inputStream != null) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            char[] charBuffer = new char[128];
            int bytesRead = -1;
            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } else {
            stringBuilder.append("");
        }
    } catch (IOException ex) {
        throw ex;
    } finally {
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                throw ex;
            }
        }
    }

    body = stringBuilder.toString();
    return body;
}
}
