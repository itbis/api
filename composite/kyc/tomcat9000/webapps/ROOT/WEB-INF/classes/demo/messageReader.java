package demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;


public class messageReader {

	public static final String JSON_FILE="/apps/service_canvas/api/monolithic/sms/fe/tomcat8080/webapps/ROOT/WEB-INF/classes/demo/message.txt";
	
	private static ReadFile rF;
	public static void main(String[] args) throws IOException {
		

		rF = new ReadFile();
		String JsonString = rF.readFile(JSON_FILE);

		//JsonString = JsonString.replaceAll("\r\n", "");

		JsonReader jsonReader = Json.createReader(new StringReader(JsonString));
		JsonObject jsonObject = jsonReader.readObject();
		jsonReader.close();

		//Retrieve data from JsonObject and create Employee bean
		message _msg = new message();

		_msg.setMsisdn(jsonObject.getString("msisdn"));
		_msg.setSender(jsonObject.getString("sender"));
		_msg.setMsg(jsonObject.getString("msg"));
		_msg.setTrxid(jsonObject.getString("trxid"));
		
		//print message bean information
		System.out.println(_msg);
	}

}
