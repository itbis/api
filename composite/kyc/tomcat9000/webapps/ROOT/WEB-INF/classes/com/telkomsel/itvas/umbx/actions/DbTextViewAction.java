package com.telkomsel.itvas.umbx.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import com.telkomsel.itvas.database.MysqlFacade;
import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;

import net.n3.nanoxml.IXMLElement;

public  class DbTextViewAction extends Action {
	private String sql;
	private String key;
	private String text;
	private Logger log = Logger.getLogger(this.getClass());
	
	public DbTextViewAction(IXMLElement element) throws UmbAutoException {
		text = element.getContent();
		key = element.getAttribute("key", null);
		sql = element.getAttribute("sql", null);
		if (sql == null) {
			throw new UmbAutoException("[DbTextView] attr not complete : sql");
		}
		if (key == null) {
			throw new UmbAutoException("[DbTextView] attr not complete : key");
		}
		if (text == null) {
			throw new UmbAutoException("[DbTextView] attr not complete : text");
		}
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		return -1;
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		String response = null;
		String originalSQL = formatState(sql, state).toLowerCase();
		String message = formatState(text, state);
		System.out.println("debug : " + message);
		Connection conn = null;
		try {
			StringBuffer sb = new StringBuffer();
			conn = MysqlFacade.getConnection();
			PreparedStatement ps = conn.prepareStatement(originalSQL);
			ResultSet rs = ps.executeQuery();
			boolean first2 = true;
			while (rs.next()) {
				if (first2) {
					first2 = false;
				} else {
					sb.append(",");
				}
				sb.append(rs.getString(key));
			}
			
			rs.next();
			ps.close();
			response = message.replaceAll("\\[" + key + "\\]", sb.toString());
		} catch (Exception e) {
			log.error("Err in preProcess() : " + e.getMessage(), e);
		} finally {
			DbUtils.closeQuietly(conn);
		}
		String strFirst = (first) ? "Yes" : "No";
		return formatResult("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
				+ "<data>"+response+"</data>").trim();
	}
}
