package com.telkomsel.itvas.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

import com.telkomsel.itvas.webstarter.WebStarterProperties;

/*
 * - A connection facade to MySQL Database
 * - Utilize apache tomcat datasource connection, or it also can be used as standalone
 * - Use dbutil package (from apache commons) to simplify query execution
 */
public class MysqlFacade {
	private InitialContext ctx;
	private DataSource ds;
	private static Hashtable<String, MysqlFacade> instances = new Hashtable<String, MysqlFacade>();
	private static String DEFAULT_JNDI = WebStarterProperties.getInstance().getProperty("default.jndi");

	private MysqlFacade() {
		this(DEFAULT_JNDI);
	}
	
	private MysqlFacade(String jdni) {
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(jdni);
		} catch (Exception e) {
			Logger
					.getLogger(MysqlFacade.class)
					.error(
							"Cannot initalize Context, try to using standalone DBCP",
							e);
		}
	}

//	private MysqlFacade(String url, String username, String password) {
//		connectionPool = new GenericObjectPool(null);
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//		} catch (ClassNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		connectionPool.setMinIdle(2);
//		connectionPool.setMaxActive(10);
//
//		connectionFactory = new DriverManagerConnectionFactory(
//				url, username, password);
//
//		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(
//				connectionFactory, connectionPool, null, null, false, true);
//		this.ds = new PoolingDataSource(connectionPool);
//	}

//	public static void initStandalone(String url, String username,
//			String password) {
//		if (instance == null) {
//			instance = new MysqlFacade(url, username, password);
//		}
//	}

	public static Connection getConnection() throws SQLException {
		return getConnection(DEFAULT_JNDI);
	}
	
	public static Connection getConnection(String jndi) throws SQLException {
		MysqlFacade instance = instances.get("java:comp/env/" + jndi);
		
		if (instance == null) {
			instance = new MysqlFacade("java:comp/env/" + jndi);
			instances.put(jndi, instance);
		}
		if (instance.ds == null) {
			return null;
		}
		return instance.ds.getConnection();
	}
	
}
