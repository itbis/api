package com.telkomsel.itvas.umbx.actions;

import java.beans.FeatureDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Enumeration;
import net.n3.nanoxml.IXMLElement;

import com.sun.org.apache.xpath.internal.FoundIndex;
import com.telkomsel.itvas.umbx.ActionFactory;
import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;

public class SwitchAction extends Action {
	private ArrayList<SwitchCase> cases = new ArrayList<SwitchCase>();
	private SwitchCase defaultCase = new SwitchCase();
	private String switchId;
	
	public SwitchAction(IXMLElement x) throws UmbAutoException {
		switchId = x.getAttribute("id", null);
		if (switchId == null) {
			throw new UmbAutoException("[SwitchFlow] attr not complete : od");
		}
		Enumeration enumeration = x.enumerateChildren();
		boolean foundDefault = false;
		while (enumeration.hasMoreElements()) {
			IXMLElement element = (IXMLElement) enumeration.nextElement();
			if (element.getFullName().equalsIgnoreCase("case")) {
				SwitchCase mycase = new SwitchCase();
				mycase.setVar(element.getAttribute("var", null));
				mycase.setEqualization(element.getAttribute("equal", null));
				Enumeration caseenumeration = element.enumerateChildren();
				while (caseenumeration.hasMoreElements()) {
					IXMLElement flowelement = (IXMLElement) caseenumeration.nextElement();
					if (flowelement.getFullName().equalsIgnoreCase("flow") || flowelement.getFullName().equalsIgnoreCase("action")) {
						try {
							mycase.addFlow(ActionFactory.getAction(flowelement));
						} catch (Exception e) {
							e.printStackTrace();
						}
					} 
				}
				cases.add(mycase);
			} else if (element.getFullName().equalsIgnoreCase("default")) {
				foundDefault = true;
				Enumeration caseenumeration = element.enumerateChildren();
				while (caseenumeration.hasMoreElements()) {
					IXMLElement flowelement = (IXMLElement) caseenumeration.nextElement();
					if (flowelement.getFullName().equalsIgnoreCase("flow") || flowelement.getFullName().equalsIgnoreCase("action")) {
						try {
							defaultCase.addFlow(ActionFactory.getAction(flowelement));
						} catch (Exception e) {
							e.printStackTrace();
						}
					} 
				}
			}	
		}
		
		if (!foundDefault) {
			throw new UmbAutoException("[SwitchFlow] Default case not provided");
		}
			
	}


	@Override
	public int postProcess(CState state, String command, boolean first) throws Exception {
		String switchno = state.getSession(switchId + "switchno");
		int foundid = -1;
		if (switchno == null) {
			int i=0;
			for (SwitchCase mycase : cases) {
				String currentData = state.getSession(mycase.getVar());
				if (currentData != null && currentData.equals(mycase.getEqualization())) {
					foundid = i;
					state.pushSessionData(switchId + "switchno", String.valueOf(foundid));
					break;
				}
				i++;
			}
		} else {
			foundid = Integer.parseInt(switchno);
		}
		SwitchCase mycase = null;
		if (foundid == -1) {
			mycase = defaultCase;
		} else {
			mycase = cases.get(foundid);
		}
		
		
		if (mycase == null) {
			mycase = defaultCase;
		}
		
		String strflowno = state.getSession(switchId + "flowno");
		if (strflowno == null) {
			strflowno = "0";
		}
		int flowno = Integer.parseInt(strflowno);
		
		
		Action f = mycase.getFlows().get(flowno);
		
		int retval = f.postProcess(state, command, false);
		flowno += retval;
		if (flowno == -1) {
			state.getSessionHash().remove(switchId + "flowno");
			state.getSessionHash().remove(switchId + "switchno");
			return -1;
		} else if (flowno >= mycase.getFlows().size()) {
			return 1;
		} else {
			state.pushSessionData(switchId + "flowno", String.valueOf(flowno));
			return 0;
		}
	}

	@Override
	public String preProcess(CState state, String command, boolean first) throws Exception {
		String switchno = state.getSession(switchId + "switchno");
		int foundid = -1;
		if (switchno == null) {
			int i=0;
			for (SwitchCase mycase : cases) {
				String currentData = state.getSession(mycase.getVar());
				if (currentData != null && currentData.equals(mycase.getEqualization())) {
					foundid = i;
					state.pushSessionData(switchId + "switchno", String.valueOf(foundid));
					break;
				}
				i++;
			}
		} else {
			foundid = Integer.parseInt(switchno);
		}
		
		SwitchCase mycase = null;
		if (foundid == -1) {
			mycase = defaultCase;
		} else {
			mycase = cases.get(foundid);
		}
		
		if (mycase == null) {
			mycase = defaultCase;
		}
		
		
		String strflowno = state.getSession(switchId + "flowno");
		if (strflowno == null) {
			strflowno = "0";
		}
		int flowno = Integer.parseInt(strflowno);
		
		Action f = mycase.getFlows().get(flowno);
		String retval = f.preProcess(state, command, false);
		return retval;
	}
}

class SwitchCase {
	private String var;
	private String equalization;
	private ArrayList<Action> flows = new ArrayList<Action>();

	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	public String getEqualization() {
		return equalization;
	}
	public void setEqualization(String equalization) {
		this.equalization = equalization;
	}
	public ArrayList<Action> getFlows() {
		return flows;
	}
	public void setFlows(ArrayList<Action> flows) {
		this.flows = flows;
	}
	public void addFlow(Action flow) {
		flows.add(flow);
	}
}
