package com.telkomsel.itvas.jamon;

import com.jamonapi.MonitorFactory;

public class JamonHelper {
	public static String queryAll() {
		String strresponse = "";
		Object[][] data=MonitorFactory.getData();
		if (data != null) {
			StringBuffer sb = new StringBuffer();
			for (int i=0;i<data.length;i++) {
				boolean first = true;
				for (int j=0;j<data[i].length;j++) {
					if (first) {
						first = false;
					} else {
						sb.append("|");
					}
					sb.append(data[i][j]);
				}
				sb.append(System.getProperty("line.separator"));
			}
			strresponse = sb.toString();
		} else {
			strresponse = "DATA_NOT_AVAILABLE";
		}
		return strresponse;
	}
	
	public static String queryAllAndReset() {
		String strresponse = "";
		Object[][] data=MonitorFactory.getData();
		if (data != null) {
			StringBuffer sb = new StringBuffer();
			for (int i=0;i<data.length;i++) {
				boolean first = true;
				for (int j=0;j<data[i].length;j++) {
					if (first) {
						first = false;
					} else {
						sb.append("|");
					}
					sb.append(data[i][j]);
				}
				sb.append(System.getProperty("line.separator"));
			}
			strresponse = sb.toString();
		} else {
			strresponse = "DATA_NOT_AVAILABLE";
		}
		MonitorFactory.reset();
		return strresponse;
	}
}
