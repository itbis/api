package com.telkomsel.cp.stwap;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.*;
import java.net.*;
import com.telkomsel.cp.util.*;

public class STWAPSubmitSM
{
	private DbCP db;

    public STWAPSubmitSM()
    {
    	db = new DbCP();
    }

    public String submitSM(String from, String oa, String msisdn, String msg, String status, String trid, String smsid, String contentid) throws IOException
    {
		String a = "";
        try
        {
			//String dlrURL = URLEncoder.encode("http://10.1.89.197:7000/cp/1stwap/dlr.jsp?smsid=" + smsid + "&trx_id=" + trid, "ISO-8859-1");
			//String url = "http://10.1.81.19:15077/cgi-bin/sendsms?username=aqsreg&password=telkomsel&from=" + from + "&to=" + msisdn + "&text=" + msg + "&dlrmask=1&dlrurl=" + dlrURL;
			String url = "http://10.1.81.19:15077/cgi-bin/sendsms?username=aqsreg&password=telkomsel&from=" + from + "&to=" + msisdn + "&text=" + msg;
   			String s = send(url);
   			if(s.equals("Sent."))
   			{
   				System.out.println(new java.util.Date()+" : INFO : SEND 1STWAP FROM " + from + " TO " + msisdn + " CONTENT: " + msg);
   				db.doUpdate("INSERT INTO cp_reportMO VALUES('1STWAP','" + msisdn + "','" + smsid + "','" + contentid + "'," + oa + ",now(),'D','','" + trid + "','" + status + "')");
   				a = "OK";
   				db.closeDBConnection();
			}
			else
			{
				System.out.println(new java.util.Date()+" : ERROR : CANNOT SEND 1STWAP FROM " + from + " TO " + msisdn + " CONTENT: " + msg + " BECAUSE SMSC LINK IS DOWN");
				a = "NOK";
			}
		}
        catch(SQLException e)
        {
            System.out.println(new java.util.Date()+" : 1STWAP ERROR : " + e.getMessage());
            a = "NOK";
        }
        //finally
        //{
			//db.closeDBConnection();
		//}
        return a;
    }

    public String submitNotif(String from, String msisdn, String msg) throws IOException
    {
		String a = "";
		String url = "http://10.1.81.19:15077/cgi-bin/sendsms?username=aqsreg&password=telkomsel&from=" + from + "&to=" + msisdn + "&text=" + msg;
		String s = send(url);
		if(s.equals("Sent."))
		{
			System.out.println(new java.util.Date()+" : INFO : SEND 1STWAP FROM "+from+" TO "+msisdn+" CONTENT: "+msg);
			a = "OK";
		}
		else
		{
			System.out.println(new java.util.Date()+" : ERROR : CANNOT SEND 1STWAP FROM "+from+" TO "+msisdn+" CONTENT: "+msg+" BECAUSE SMSC LINK IS DOWN");
			a = "NOK";
		}
        return a;
    }

    public static String send(String address) throws MalformedURLException, IOException, ClassCastException
    {
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in1 = url.openStream();
			BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
			String line1 = "";
			while((line1 = br1.readLine()) != null)
			{
				res = res.concat(line1);
			}
			br1.close();
		}
		catch(Exception e)
		{
			res = "dodol";
		}

        return res;
    }
}