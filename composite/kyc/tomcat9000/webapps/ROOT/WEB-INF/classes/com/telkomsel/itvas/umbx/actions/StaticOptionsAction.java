package com.telkomsel.itvas.umbx.actions;

import java.util.ArrayList;
import java.util.Enumeration;

import com.telkomsel.itvas.umbx.ActionFactory;
import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;

import net.n3.nanoxml.IXMLElement;

public class StaticOptionsAction extends Action {
	private String var;
	private ArrayList<String> options = new ArrayList<String>();
	private String text;
	
	public StaticOptionsAction(IXMLElement x) throws Exception {
		var = x.getAttribute("var", null);
		text = x.getAttribute("text", null);
		if (var == null) {
			throw new UmbAutoException("[StaticOptionsFlow] attr not complete : var");
		}
		Enumeration enumeration = x.enumerateChildren();
		while (enumeration.hasMoreElements()) {
			IXMLElement element = (IXMLElement) enumeration.nextElement();
			if (element.getFullName().equalsIgnoreCase("option")) {
				options.add(element.getContent());
			}
		}
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		StringBuffer sb = new StringBuffer();
		int i = 1;
		for (String s : options) {
			sb.append(i + ". " + s + NEWLINE);
			i++;
		}
		String strFirst = (first) ? "Yes" : "No";
		String response = "<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE +"<data>" + NEWLINE + text + NEWLINE  
		+ sb.toString() 
		+ "</data>";
		return formatResult(formatState(response, state));
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		if (command.equals("9")) {
			return -1;
		}
		int i = -1;
		try {
			i = Integer.parseInt(command);
		} catch (Exception e) {
		}
		if (i >= 0 && i <= options.size()) {
			state.pushSessionData(var, command);
			return 1;
		} else {
			return 0;
		}
	}
}
