package kyc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;


public class messageReader {

	public static message readJson(String JsonString) throws IOException {
	
		message _msg = new message();
		
		try {
			JsonReader jsonReader = Json.createReader(new StringReader(JsonString));
			JsonObject jsonObject = jsonReader.readObject();
			jsonReader.close();

			_msg.setMsisdn(jsonObject.getString("msisdn"));
			_msg.setType(jsonObject.getString("type"));
			_msg.setMsg(jsonObject.getString("msg"));
			_msg.setTrxid(jsonObject.getString("trxid"));
		}
		catch(Exception e) {
			_msg.setMsisdn(null);		
			_msg.setType(null);		
			_msg.setMsg(null);		
			_msg.setTrxid(null);		
		}
		
		return _msg; 
	}

}
