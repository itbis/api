<%@ page import="javax.rmi.*,javax.naming.*, java.util.*, java.sql.*, com.daniel.custcare.*" %>

<%
try {

	Properties props = new Properties();
    props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
    props.put(Context.PROVIDER_URL, "10.1.89.210:1099");
	props.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");



	Context jndiContext =  new InitialContext(props);
         Object obj = jndiContext.lookup("LogHomeRemote");
         LogHomeRemote home = (LogHomeRemote) PortableRemoteObject.narrow(obj, LogHomeRemote.class);
         LogRemote l = home.create();
         l.logSend("1", "2", "3", "4", "5");

} catch (Exception e) { out.println(e); e.printStackTrace(System.out); }

%>
done