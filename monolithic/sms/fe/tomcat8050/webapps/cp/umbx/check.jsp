<%@page import="java.sql.*,com.telkomsel.itvas.database.*,org.apache.commons.dbutils.*"%><%
Connection conn = null;
String ret = "";
try {
	conn = MysqlFacade.getConnection("jdbc/umbx4");
	if (conn != null) {
		ret = "OK";
	} else {
		ret = "FAILED";
	}
} catch (Exception e) {
	ret = "FAILED";
	e.printStackTrace();
} finally {
	DbUtils.closeQuietly(conn);
}
out.print(ret);
%>