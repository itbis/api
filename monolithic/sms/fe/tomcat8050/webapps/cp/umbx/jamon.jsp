<%@page import="com.telkomsel.itvas.jamon.JamonHelper"%><%
String type = request.getParameter("type");
if (type == null) {
	type = "";
}
String resp = "";
if (type.equalsIgnoreCase("queryall")) {
	resp = JamonHelper.queryAll();
} else if (type.equalsIgnoreCase("queryallandreset")) {
	resp = JamonHelper.queryAllAndReset();
} else {
	resp = "TYPE_NOT_DEFINED";
}
out.print(resp);
%>