package com.telkomsel.itvas.umbx.actions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;
import com.telkomsel.itvas.util.Util;

import net.n3.nanoxml.IXMLElement;

public  class HttpGetAction extends Action {
	private String[] uri;
	private HttpResponse response;
	
	public HttpGetAction(IXMLElement element) throws UmbAutoException {
		String uri = element.getAttribute("uri", null);
		if (uri == null) {
			throw new UmbAutoException("[HttpGetFlow] attr not complete : uri");
		}
		this.uri = uri.split("\\^_\\^");
		
		Enumeration<IXMLElement> eParams = element.enumerateChildren();
		while (eParams.hasMoreElements()) {
			IXMLElement param = eParams.nextElement();
			if (param.getName().equalsIgnoreCase("response")) {
				response = new HttpResponse(param);
			}
		}
		
		if (response == null) {
			throw new UmbAutoException("[HttpGetFlow] response not complete : uri");
		}
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		StringBuffer sb = new StringBuffer();
		for (int i=0;i<uri.length;i++) {
			String myUri = uri[0];
			myUri = formatState(myUri, state);
			log.debug("HTTP-GET Request-" + i + "|" + myUri);
			String strresponse = Util.HttpGet(myUri).trim();
			log.debug("HTTP-GET Response-" + i + "|" + strresponse);
			if (i > 0) {
				sb.append("^_^");
			}
			sb.append(strresponse);
		}
		log.debug("ALL RETVAL : " + sb.toString());
		String retval = response.findMatchingResponse(sb.toString(), state);
		log.debug("RESPONSE : " + retval);
		String output = formatState(retval, state);
		String strFirst = (first) ? "Yes" : "No";
		return formatResult("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
			+ "<data>"+output+"</data>").trim();
	}
}

class HttpResponse {
	private ArrayList<HttpResponseCase> cases;
	private HttpResponseCase elseCase;
	
	public HttpResponse(IXMLElement element) throws UmbAutoException {
		Enumeration<IXMLElement> eParams = element.enumerateChildren();
		cases = new ArrayList<HttpResponseCase>();
		while (eParams.hasMoreElements()) {
			IXMLElement param = eParams.nextElement();
			if (param.getName().equalsIgnoreCase("case")) {
				cases.add(new HttpResponseCase(param));
			} else if (param.getName().equalsIgnoreCase("else")) {
				elseCase = new HttpResponseCase(param);
			}
		}
		if (elseCase == null) {
			throw new UmbAutoException("[HttpResponse] elseCase is null");
		}
	}
	
	public String findMatchingResponse(String s, CState state) {
		for (HttpResponseCase c : cases) {
			Action.log.info("TEST REGEX : " + c.getRegex());
			if (c.matched(s, state)) {
				return c.getResponse();
			}
		}
		return elseCase.getResponse();
	}

	public ArrayList<HttpResponseCase> getCases() {
		return cases;
	}

	public void setCases(ArrayList<HttpResponseCase> cases) {
		this.cases = cases;
	}

	public HttpResponseCase getElseCase() {
		return elseCase;
	}

	public void setElseCase(HttpResponseCase elseCase) {
		this.elseCase = elseCase;
	}
}

class HttpResponseCase {
	private Pattern pattern;
	private String regex;
	private String response;
	
	public HttpResponseCase(IXMLElement param) throws UmbAutoException {
		regex = param.getAttribute("regex", null);
		response = param.getContent();
		if (!param.getName().equalsIgnoreCase("else") && regex == null) {
			throw new UmbAutoException("[HttpResponseCase] value is null");
		}
		if (regex != null) {
			pattern = Pattern.compile(regex);
		}
		if (response == null) {
			throw new UmbAutoException("[HttpResponseCase] content is null");
		}
	}

	public boolean matched(String message, CState state) {
		Matcher m = pattern.matcher(message);
		if (m.find()) {
			for (int i=1;i<=m.groupCount();i++) {
				state.pushSessionData("r" + i, m.group(i));
			}
			return true;
		} else {
			return false;
		}
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
