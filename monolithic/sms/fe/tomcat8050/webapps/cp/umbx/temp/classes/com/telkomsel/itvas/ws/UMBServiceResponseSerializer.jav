package com.telkomsel.itvas.ws;

import java.io.IOException;

import  javax.xml.namespace.QName;

import org.apache.axis.encoding.SerializationContext;
import  org.apache.axis.encoding.Serializer;
import org.apache.axis.wsdl.fromJava.Types;
import  org.w3c.dom.Element;
import org.xml.sax.Attributes;
import  org.apache.axis.Constants;

/**
*/
public  class UMBServiceResponseSerializer implements Serializer {
         public  static final String STATUS = "Status";
         public  static final String ERRORCODE = "ErrorCode";
         public  static final String ERRORMESSAGE = "ErrorMessage";
         public  static final QName myTypeQName = new QName("nonBeanTypes", "UMBServiceResponse");

         /** SERIALIZER 
          */
         /**
          * Serialize  an element named name, with the indicated attributes
          * and  value.
          * @param  name is the element name
          * @param  attributes are the attributes...serialize is free to add more.
          * @param  value is the value
          * @param  context is the SerializationContext
          */
         public  void serialize(
                  QName  name,
                  Attributes  attributes,
                  Object  value,
                  SerializationContext  context)
                  throws  IOException {
                  if  (!(value instanceof UMBServiceResponse))
                           throw  new IOException(
                                    "Can't  serialize a "
                                             + value.getClass().getName()
                                             + "  with a BookSerializer.");
                  UMBServiceResponse  data = (UMBServiceResponse) value;

                  context.startElement(name,  attributes);
                  context.serialize(new  QName("", STATUS), null, data.Status);
                  context.serialize(new  QName("", ERRORCODE), null, data.ErrorCode);
                  context.serialize(new  QName("", ERRORMESSAGE), null, data.ErrorMessage);
                  context.endElement();
         }
         public  String getMechanismType() {
                  return  Constants.AXIS_SAX;
         }

         /* (non-Javadoc)
          * @see org.apache.axis.encoding.Serializer#writeSchema(java.lang.Class, org.apache.axis.wsdl.fromJava.Types)
          */
         public  Element writeSchema(Class arg0, Types types) throws Exception {
                  //   Auto-generated method stub
                  Element  complexType = types.createElement("complexType");
                  types.writeSchemaElement(myTypeQName,  complexType);
                  complexType.setAttribute("name", myTypeQName.getLocalPart());
                  Element  seq = types.createElement("sequence");
                  complexType.appendChild(seq);

                  Element  element = types.createElement("element");
                  element.setAttribute("name",  "name");
                  element.setAttribute("type",  "xsd:string");
                  seq.appendChild(element);
                  Element  element2 = types.createElement("element");
                  element2.setAttribute("name",  "author");
                  element2.setAttribute("type",  "xsd:string");
                  seq.appendChild(element2);
                  Element  element3 = types.createElement("element");
                  element2.setAttribute("name",  "author");
                  element2.setAttribute("type",  "xsd:string");
                  seq.appendChild(element2);

                  return  complexType;
         }
}
