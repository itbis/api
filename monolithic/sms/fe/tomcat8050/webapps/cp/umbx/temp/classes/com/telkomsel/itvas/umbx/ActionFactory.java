package com.telkomsel.itvas.umbx;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.telkomsel.itvas.umbx.actions.Action;
import com.telkomsel.itvas.umbx.actions.DbBrowseAction;
import com.telkomsel.itvas.umbx.actions.HttpGetAction;
import com.telkomsel.itvas.umbx.actions.InputAction;

import net.n3.nanoxml.IXMLElement;

public class ActionFactory {

	public static Action getAction(IXMLElement element) throws UmbAutoException, ClassNotFoundException, InstantiationException, IllegalAccessException, SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		String fClass = element.getAttribute("class", null);
		if (fClass == null || fClass.trim().equals("")) {
			throw new UmbAutoException("type attribute in element flow is empty");
		}
		
		Class c = Class.forName("com.telkomsel.itvas.umbx.actions." + fClass + "Action");
		Constructor constructor = c.getConstructor(IXMLElement.class);
		Action action;
		action = (Action) constructor.newInstance(element);
		return action;
	}

}
