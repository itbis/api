package com.telkomsel.itvas.umbx;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;

public class WsClient {
	public static void main(String [] args) {
       try {
    	   System.out.println("start!!!");
         String endpoint =
             "http://10.2.248.66:7007/ARSWebServices/ArsSubsQuery.jws";
  
        Service  service = new Service();
        Call     call    = (Call) service.createCall();
  
        call.setTargetEndpointAddress( new java.net.URL(endpoint) );
        call.setOperationName(new QName("http://www.telkomsel.com/soa/2006/07/ars/ws","getTypeByMsisdn"));
        call.addParameter(new QName("http://www.telkomsel.com/soa/2006/07/ars/ws","msisdn"), XMLType.XSD_STRING, ParameterMode.IN);
        call.setReturnType(XMLType.XSD_STRING);
  
        String ret = (String) call.invoke(new Object[]{"62811917427"});
  
        System.out.println("Sent 'Hello!', got '" + ret + "'");
      } catch (Exception e) {
        System.err.println(e.toString());
      }
    }
}