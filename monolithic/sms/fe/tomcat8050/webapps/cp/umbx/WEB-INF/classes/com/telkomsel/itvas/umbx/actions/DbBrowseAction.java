package com.telkomsel.itvas.umbx.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import com.telkomsel.itvas.database.MysqlFacade;
import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;

import net.n3.nanoxml.IXMLElement;

public  class DbBrowseAction extends Action {
	private String sql;
	private String text;
	private String key;
	private String value;
	private String nextPage;
	private String jndi;
	private String var;
	private String[] vars;
	private String[] keys;
	private int nbItem;
	private QueryRunner qr = new QueryRunner();;
	
	public DbBrowseAction(IXMLElement element) throws UmbAutoException {
		text = element.getContent();
		if (text == null) {
			text = "";
		}
		key = element.getAttribute("key", null);
		jndi = element.getAttribute("jndi", null);
		sql = element.getAttribute("sql", null);
		value = element.getAttribute("value", null);
		var = element.getAttribute("var", null);
		
		nextPage = element.getAttribute("next_page", null);
		nbItem = Integer.parseInt(element.getAttribute("nb_item", null));
		if (sql == null) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : sql");
		}
		if (key == null) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : key");
		}
		keys = key.split(",");
		if (value == null) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : value");
		}
		if (var == null) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : var");
		}
		vars = var.split(",");
		
		if (nextPage == null) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : nextPage");
		}
		if (vars.length != keys.length) {
			throw new UmbAutoException("[DbBrowseFlow] Number variable between var and key do not match, var : " + var + ", key : " + key);			
		}
		if (nbItem == -1) {
			throw new UmbAutoException("[DbBrowseFlow] attr not complete : nbItem");
		}
		
	}

	@Override
	public int postProcess(CState state, String command, boolean first) throws Exception {
		try {
			int intCommand = 9;
			try {
				intCommand = Integer.parseInt(command);
			} catch (Exception e1) {
			}
			if (intCommand == nbItem + 1) {
				state.setCurrentPage(state.getCurrentPage() + 1);
				return 0;
			} else if (intCommand == 9) {
				state.setCurrentPage(0);
				for (int i=0;i<vars.length;i++) {
					state.popSessionData(vars[i]);
				}
				return 0;
			} else {
				String modifiedSQL = formatState(sql, state).toLowerCase();
				int offset = (nbItem * state.getCurrentPage()) - 1 + intCommand;
				String qSql = modifiedSQL + " LIMIT " + offset + ", " + 1;
				log.debug("SQL : " + qSql);
				Connection conn = null;
				try {
					if (jndi == null || jndi.equals("")) {
						conn = MysqlFacade.getConnection();
					} else {
						conn = MysqlFacade.getConnection(jndi);
					}
					PreparedStatement ps = conn.prepareStatement(qSql);
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						for (int i=0;i<vars.length;i++) {
							state.pushSessionData(vars[i], rs.getString(keys[i]));
						}
						rs.close();
						ps.close();
						DbUtils.closeQuietly(conn);
						state.setCurrentPage(0);
						return 1;
					} else {
						rs.close();
						ps.close();
						DbUtils.closeQuietly(conn);
						state.setCurrentPage(0);
						return 0;
					}
				} catch (Exception e) {
					log.error("XMLUMB Err in postProcess() : " + e.getMessage(), e);
					throw e;
				} finally {
					DbUtils.closeQuietly(conn);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
		
	}

	@Override
	public String preProcess(CState state, String command, boolean first) throws Exception {
		String response = null;
		String originalSQL = formatState(sql, state).toLowerCase();
		String modifiedSQL = originalSQL;
		int beginning = modifiedSQL.indexOf("select") + 6;
		int end = modifiedSQL.indexOf("from");
		modifiedSQL = modifiedSQL.substring(0, beginning) + " count(*) as jumlah " + modifiedSQL.substring(end);
		Connection conn = null;
		try {
			StringBuffer sb = new StringBuffer();
			if (!text.equals("")) {
				sb.append(text + System.getProperty("line.separator"));
			}
			if (jndi == null || jndi.equals("")) {
				conn = MysqlFacade.getConnection();
			} else {
				conn = MysqlFacade.getConnection(jndi);
			}
			Object res = qr.query(conn, modifiedSQL, new ScalarHandler("jumlah"));
			long nbRecord = (Long) res;
			long nbPage = nbRecord / nbItem;
			if (state.getCurrentPage() == -1) {
				state.setCurrentPage(0);
			}
			if ((nbRecord % nbItem) == 0) {
				nbPage--;
			}
			int offset = state.getCurrentPage() * nbItem;
			String qSql = originalSQL + " LIMIT " + offset + ", " + nbItem;
			PreparedStatement ps = conn.prepareStatement(qSql);
			ResultSet rs = ps.executeQuery();
			int num = 1;
			while (rs.next()) {
				sb.append("<data code=\""+num+"\">"+rs.getString(value)+"</data>" + NEWLINE);
				num++;
			}
			if (state.getCurrentPage() < nbPage) {
				sb.append("<data code=\""+num+"\">"+nextPage+"</data>" + NEWLINE);
				num++;
			}
			
			rs.next();
			ps.close();
			String strFirst = (first) ? "Yes" : "No";
			response = "<type>Menu</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE +"<menu totalmenu=\""+(num-1)+"\">" + NEWLINE  
						+ sb.toString() 
						+ "</menu>";
		} catch (Exception e) {
			log.error("Err in preProcess() : " + e.getMessage(), e);
			throw e;
		} finally {
			DbUtils.closeQuietly(conn);
		}
		return formatResult(response).trim();
	}
}
