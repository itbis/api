<%@page import="java.sql.*"%><%@page import="com.telkomsel.itvas.database.MysqlFacade"%><%
String app = request.getParameter("app");
String msisdn = request.getParameter("msisdn");
Connection conn  = MysqlFacade.getConnection();
String q = "INSERT INTO hit_counter (app, msisdn, dt, counter) VALUES (?,?,CURDATE(),1) ON DUPLICATE KEY UPDATE counter=IF(CURDATE()<>dt, 1, counter+1), dt=CURDATE()";
PreparedStatement ps = null;
ResultSet rs = null;
String resp = "OK";
try {
	ps = conn.prepareStatement(q);
	ps.setString(1, app);
	ps.setString(2, msisdn);
	ps.executeUpdate();
} catch (Exception e) {
	resp = "NOK";
	e.printStackTrace();
} finally {
	if (rs != null) {
		try {
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	if (ps != null) {
		try {
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	if (conn != null) {
		try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
out.print(resp);%>