<%@page import="java.sql.*"%><%@page import="com.telkomsel.itvas.database.MysqlFacade"%><%
String app = request.getParameter("app");
String msisdn = request.getParameter("msisdn");
String limit = request.getParameter("limit");
Connection conn  = MysqlFacade.getConnection();
String q = "SELECT counter FROM hit_counter WHERE app=? AND msisdn=? AND dt=CURDATE()";
PreparedStatement ps = null;
ResultSet rs = null;
String resp = "OK";
try {
	ps = conn.prepareStatement(q);
	ps.setString(1, app);
	ps.setString(2, msisdn);
	rs = ps.executeQuery();
	if (rs.next()) {
		int iLimit = Integer.parseInt(limit);
		if (rs.getInt("counter") >= iLimit) {
			resp = "NOK";
		}
	}
} catch (Exception e) {
	resp = "NOK";
	e.printStackTrace();
} finally {
	if (rs != null) {
		try {
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	if (ps != null) {
		try {
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	if (conn != null) {
		try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
out.print(resp);%>