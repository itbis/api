package com.telkomsel.cp.sms;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.*;
import java.net.*;
import com.telkomsel.cp.util.*;

public class SubmitSMDirect3
{
	private DbCP2 db;
	private HttpGet HTTP;

    public SubmitSMDirect3()
    {
    	db = new DbCP2();
		HTTP = new HttpGet();
    }

    public String submitSM(String cp_name, String charge_type, String from, String oa, String msisdn, String msg, String status, String trid, String smsid, String contentid, String ip_smsgw, String port_smsgw, String id_smsgw, String pwd_smsgw)
    {
		String a = "";
		String m = msg.replaceAll("%40", "%7C");
		m = m.replaceAll("_", "%0B");
        try
        {
			String dlrURL = URLEncoder.encode("http://10.1.89.197:7000/cp/dlr.jsp?smsid=" + smsid + "&trxid=" + trid + "&cp_name=" + cp_name + "&ctype=" + charge_type, "ISO-8859-1");
			//String url = "http://" + ip_smsgw + ":" + port_smsgw + "/cgi-bin/sendsms?username=" + id_smsgw + "&password=" + pwd_smsgw + "&from=" + from + "&to=" + msisdn + "&text=" + m + "&dlrmask=1&dlrurl=" + dlrURL;
			String url = "http://" + ip_smsgw + ":" + port_smsgw;
			String files = "/cgi-bin/sendsms?username=" + id_smsgw + "&password=" + pwd_smsgw + "&from=" + from + "&to=" + msisdn + "&text=" + m + "&dlrmask=1&dlrurl=" + dlrURL;
			String s = HTTP.Get(url, files, 10000);
   			if(s.equals("Sent."))
   			{
   				System.out.println(new java.util.Date() + " [INFO] com.telkomsel.cp.sms.SubmitSMDirect2.submitSM(" + cp_name + ", " + msisdn + ", " + msg + ")=Success");
				db.insertCPReport(charge_type, cp_name, msisdn, smsid, contentid, oa, trid, status);
   				a = "OK";
			}
			else
			{
				System.out.println(new java.util.Date() + " [ERROR] com.telkomsel.cp.sms.SubmitSMDirect2.submitSM(" + cp_name + ", " + msisdn + ", " + msg + ")=Failed");
				a = "NOK";
			}
		}
        catch(Exception e)
        {
            System.out.println(new java.util.Date() + " [FATAL] com.telkomsel.cp.sms.SubmitSMDirect2.submitSM(" + cp_name + ", " + msisdn + ", " + msg + ")=" + e.getMessage());
            a = "NOK";
        }
        return a;
    }

	public String submitNotif(String cp_name, String from, String msisdn, String msg) throws IOException
    {
		String a = "";
		String url = "http://10.1.89.197:19002/cgi-bin/sendsms?username=tester&password=foobar&from=" + from + "&to=" + msisdn + "&text=" + msg;
		String s = send(url);
		if(s.equals("Sent."))
		{
			System.out.println(new java.util.Date() + " [INFO] com.telkomsel.cp.sms.SubmitSMDirect2.submitNotif(" + cp_name + ", " + msisdn + ", " + msg + ")=Success");
			a = "OK";
		}
		else
		{
			System.out.println(new java.util.Date() + " [ERROR] com.telkomsel.cp.sms.SubmitSMDirect2.submitNotif(" + cp_name + ", " + msisdn + ", " + msg + ")=Failed");
			a = "NOK";
		}
        return a;
    }

    public static String send(String address) throws MalformedURLException, IOException, ClassCastException
    {
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in = url.openStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while((line = br.readLine()) != null)
			{
				res = res.concat(line);
			}
			br.close();
			in.close();
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
			res = "dodol";
		}

        return res;
    }
}