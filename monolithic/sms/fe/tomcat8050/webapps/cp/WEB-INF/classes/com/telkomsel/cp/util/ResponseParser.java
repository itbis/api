package com.telkomsel.cp.util;

import java.io.*;
import java.util.*;
import java.net.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.xml.sax.*;
import org.apache.log4j.Logger;

public class ResponseParser
{
	public ResponseParser()
	{
	}

	public String Parse(String content)
    	{
		SAXBuilder builder = null;
		Document myDocument = null;
		Element root = null;
		Element typeChild = null;
		Element firstChild = null;
		Element bcChild = null;
		Element menuChild = null;
		Element nextChild = null;
		Element dataChild = null;

		List allChild = null;
		Iterator it = null;
		List rtp = null;
		Iterator i = null;

		String _type = null;
		String _first = null;
		String _fr = null;
		String _bc = null;
		String _next = null;
		String menu = null;
		String respo = "dodol";

		
		try
		{
			builder = new SAXBuilder();
			myDocument = builder.build(new StringReader(content));

			root = myDocument.getRootElement();

			allChild = root.getChildren();
			it = allChild.iterator();

			typeChild = root.getChild("type");
			_type = typeChild.getText();
			if(_type == null || _type.equals(""))
				_type = "kampret";

			firstChild = root.getChild("first");
			_first = firstChild.getText();
			if(_first == null || _first.equals(""))
				_first = "kampret";

			if(_first.equalsIgnoreCase("Yes"))
				_fr = "Yes";
			else if(_first.equalsIgnoreCase("No") || _first.equalsIgnoreCase("N"))
				_fr = "No";
			else
				_fr = "Yes";

			bcChild = root.getChild("back_code");
			_bc = bcChild.getText();
			if(_bc == null || _bc.equals(""))
				_bc = "99";

			if(_type.equalsIgnoreCase("Menu"))
			{
				menu = "";
				String keyword = null;
				String kyNy = null;
				String adn_push = null;
				String cp_name = null;
				menuChild = root.getChild("menu");
				rtp = menuChild.getChildren();
				i = rtp.iterator();
				int ii = 0;
				while(i.hasNext())
				{
					ii++;
					dataChild = (Element)i.next();
					String _code = dataChild.getAttributeValue("code");
					String _data = dataChild.getText();
					
					if (_code.equals("0"))
						menu = menu.concat(_data + "\n");
					else {
						if(ii == 1) keyword = _data;
						else if (ii == 2) {
							adn_push = _data;
							kyNy = _data;
						}
						else if (ii == 3) cp_name = _data;
						
						if (_data.startsWith("Ya|")) _data = "Ya";

						menu = menu.concat(_code + "." + _data + "\n");
					}
				}

				nextChild = root.getChild("next");
				_next = nextChild.getText();
                                if(_next == null || _next.equals(""))
                                        _next = "N";
				if(_next.equalsIgnoreCase("R") || _next.equalsIgnoreCase("U") || _next.equalsIgnoreCase("DR") || _next.equalsIgnoreCase("DU"))
					_next = _next + "|" + keyword + "|" + adn_push + "|" + cp_name;
				/*if (_next.equalsIgnoreCase("L")) {
					if (kyNy.startsWith("Ya")) {
						StringTokenizer token = new StringTokenizer(kyNy, "|", false);
                				Vector v = new Vector();
                				while(token.hasMoreTokens())
                				{
                    					v.add(token.nextToken());
                				}
					
                				String ad = (String)v.get(1);
                				kyNy = (String)v.get(2);
						_next = "L1|" + ad + "|" + kyNy;
					}
				} */
				if (_next.equalsIgnoreCase("DC")) {
					String ad = keyword;
					String ky = adn_push;
					_next = _next + "|" + ad + "|"+ ky;
				}
			}
			else if(_type.equalsIgnoreCase("Content"))
			{
				dataChild = root.getChild("data");
				menu = dataChild.getText() + "\n";
				if(menu == null || menu.equals(""))
					menu = " \n";
				_next = "N";
			}
	
			if (_fr.equalsIgnoreCase("no") || _fr.equalsIgnoreCase("n"))
				menu = menu + "\n9.back\n0.main";
			
			respo = menu +"|"+_next+"|"+_fr;
		}
		catch(org.jdom.JDOMException e)
		{
			respo = e.getMessage();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			respo = e.getMessage();
		}

		return respo;
	}	
}
