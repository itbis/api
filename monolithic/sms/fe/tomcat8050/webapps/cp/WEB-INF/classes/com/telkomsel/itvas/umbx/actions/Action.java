package com.telkomsel.itvas.umbx.actions;

import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.log4j.Logger;

import com.telkomsel.itvas.umbx.CState;

public abstract class Action {
	protected static String NEWLINE = System.getProperty("line.separator");
	private static String XML_HEADER = "<?xml version=\"1.0\"?>" + NEWLINE + "<umb>" + NEWLINE;
	private static String XML_FOOTER = NEWLINE + "<back_code>9</back_code>" + NEWLINE + "</umb>";
	protected boolean hidePrevious = false;
	
	public static Logger log = Logger.getLogger(com.telkomsel.itvas.umbx.Flow.class);
	/*
	 * @param state
	 * @param command
	 * @return The string to be displayed in USSD Menu Browser state
	 */
	public abstract String preProcess(CState state, String command, boolean first) throws Exception;
	
	/**
	 * @param state
	 * @param command
	 * @return -1 = Back State
	 * 			0 = Current State
	 * 			1 = Next state
	 * @throws Exception 
	 */
	public abstract int postProcess(CState state, String command, boolean first) throws Exception;
	
	protected static String formatState(String mystr, CState state) {
		if ((mystr.indexOf("[") == -1) || (mystr.indexOf("]") == -1)) {
			return mystr;
		}
		Hashtable<String, String> sessionHash = state.getSessionHash();
		String str = mystr;
		if (sessionHash != null) {
			Enumeration<String> enumeration = sessionHash.keys();
			
			while (enumeration.hasMoreElements()) {
				String key = enumeration.nextElement();
				str = str.replaceAll("\\[" + key + "\\]", sessionHash.get(key));
			}
		}
		str = str.replaceAll("\\[msisdn\\]", state.getMsisdn());
		return str;
	}
	
	protected String formatResult(String content) {
		return XML_HEADER + content + XML_FOOTER;
	}
}
