package com.telkomsel.itvas.ws;

import java.util.Iterator;
import  java.util.Vector;
import org.apache.axis.Constants;

import  org.apache.axis.encoding.SerializerFactory;

/**
*/
public  class UMBServiceResponseSerFactory implements SerializerFactory {
         private  Vector mechanisms;

         public  UMBServiceResponseSerFactory() {
         }
         public  javax.xml.rpc.encoding.Serializer getSerializerAs(String mechanismType) {
                  return  new UMBServiceResponseSerializer();
         }
         public  Iterator getSupportedMechanismTypes() {
                  if  (mechanisms == null) {
                           mechanisms = new  Vector();
                           mechanisms.add(Constants.AXIS_SAX);
                  }
                  return  mechanisms.iterator();
         }
}
