package com.telkomsel.cp.util;

import java.io.*;
import java.util.*;
import java.net.*;
//import javax.servlet.*;
//import javax.servlet.http.*;

public class HTTPGet {

        public static void main ( String[] args ) {
                String temp = get(args[0], 0, 0);
                System.out.println("result: " + temp + "\r");
        }

/**
 * @param String url
 * @param int first line to return
 * @param int last line to return or 0 for actual last line
 */
        public static String get( String url, int start, int end ){
                StringBuffer r = new StringBuffer();
                try {
                        URL u = new URL( url );
                        URLConnection uc = u.openConnection();

                        InputStreamReader isr = null;
                        BufferedReader br = null;
                        try {
                                isr = new InputStreamReader( uc.getInputStream() );
                                br = new BufferedReader(isr);
                                int chk = 0;
                                while (( url = br.readLine()) != null) {
                                        if ( chk >= start && (chk < end || chk==0)) r.append( url ).append( '\n');
                                }
                        } catch (java.net.ConnectException cex) {
                                r.append( cex.getMessage() );
                        } catch (Exception ex) {
                                System.out.println("Error: (HTTPG:get:41) " + ex + "\r");
                        } finally {
                                try {
                                        br.close();
                                } catch (Exception ex) {}
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                        //printError( e.toString() );
                }
                return r.toString();
        }

        public static boolean isOk( String url ){
                try {
                        URL u = new URL( url );
                        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                        if ( huc.getResponseCode() != HttpURLConnection.HTTP_OK ) {
                                System.out.println( "CODE: " + huc.getResponseCode() + " " + url);
                        }
                        return huc.getResponseCode() ==  HttpURLConnection.HTTP_OK;
                } catch (Exception e) {
                        e.printStackTrace();
                        //printError( e.toString() );
                }
                return false;
        }

        public int getCode( String url ){
                try {
                        URL u = new URL( url );
                        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                        return huc.getResponseCode();
                } catch (Exception e) {
                        e.printStackTrace();
                        //printError( e.toString() );
                }
                return -10000;
        }
}

