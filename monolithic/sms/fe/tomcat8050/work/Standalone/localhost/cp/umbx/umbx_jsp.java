package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import com.telkomsel.itvas.umbx.Flow;
import com.telkomsel.itvas.umbx.FlowContainer;

public class umbx_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;


	FlowContainer container = FlowContainer.getInstance();
	String flowId = request.getParameter("x");
	String msisdn = request.getParameter("msisdn");
	String command = request.getParameter("command");
	String refresh = request.getParameter("refresh");

	if (flowId == null || msisdn == null || command == null) {
		System.out.println("debug1");
		out.println("PARAM_NOT_COMPLETE");
	} else {
		Flow flow = null;
		try {
			if (refresh != null && refresh.equals("1")) {
				flow = container.get(flowId, true);
			} else {
				flow = container.get(flowId);
			}
		
			if (flow == null) {
				out.println("Flow not found : " + flowId);
			} else {
				String myResponse = flow.exec(msisdn, command, request);
				out.println(myResponse);
			}
		} catch (Exception e) {
			//e.printStackTrace(out);
			throw e;
		}
	}

      out.write("\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
