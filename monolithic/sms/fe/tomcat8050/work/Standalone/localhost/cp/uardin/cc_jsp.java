package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.telkomsel.cp.util.*;

public class cc_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      com.telkomsel.cp.util.Session Ses = null;
      synchronized (application) {
        Ses = (com.telkomsel.cp.util.Session) pageContext.getAttribute("Ses", PageContext.APPLICATION_SCOPE);
        if (Ses == null){
          try {
            Ses = (com.telkomsel.cp.util.Session) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Session");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Session", exc);
          }
          pageContext.setAttribute("Ses", Ses, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.Seq Seq = null;
      synchronized (application) {
        Seq = (com.telkomsel.cp.util.Seq) pageContext.getAttribute("Seq", PageContext.APPLICATION_SCOPE);
        if (Seq == null){
          try {
            Seq = (com.telkomsel.cp.util.Seq) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Seq");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Seq", exc);
          }
          pageContext.setAttribute("Seq", Seq, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.ResponseParser rp = null;
      synchronized (application) {
        rp = (com.telkomsel.cp.util.ResponseParser) pageContext.getAttribute("rp", PageContext.APPLICATION_SCOPE);
        if (rp == null){
          try {
            rp = (com.telkomsel.cp.util.ResponseParser) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.ResponseParser");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.ResponseParser", exc);
          }
          pageContext.setAttribute("rp", rp, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.Enc mye = null;
      synchronized (application) {
        mye = (com.telkomsel.cp.util.Enc) pageContext.getAttribute("mye", PageContext.APPLICATION_SCOPE);
        if (mye == null){
          try {
            mye = (com.telkomsel.cp.util.Enc) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Enc");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Enc", exc);
          }
          pageContext.setAttribute("mye", mye, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.DbCP2 Db = null;
      synchronized (pageContext) {
        Db = (com.telkomsel.cp.util.DbCP2) pageContext.getAttribute("Db", PageContext.PAGE_SCOPE);
        if (Db == null){
          try {
            Db = (com.telkomsel.cp.util.DbCP2) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.DbCP2");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.DbCP2", exc);
          }
          pageContext.setAttribute("Db", Db, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.DB Db2 = null;
      synchronized (pageContext) {
        Db2 = (com.telkomsel.cp.util.DB) pageContext.getAttribute("Db2", PageContext.PAGE_SCOPE);
        if (Db2 == null){
          try {
            Db2 = (com.telkomsel.cp.util.DB) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.DB");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.DB", exc);
          }
          pageContext.setAttribute("Db2", Db2, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.HTTPGet HTTP = null;
      synchronized (pageContext) {
        HTTP = (com.telkomsel.cp.util.HTTPGet) pageContext.getAttribute("HTTP", PageContext.PAGE_SCOPE);
        if (HTTP == null){
          try {
            HTTP = (com.telkomsel.cp.util.HTTPGet) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.HTTPGet");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.HTTPGet", exc);
          }
          pageContext.setAttribute("HTTP", HTTP, PageContext.PAGE_SCOPE);
        }
      }

		
		String msisdn = request.getParameter("msisdn");
	
		String keyword = request.getParameter("keyword");
                String service = request.getParameter("service");
                String alias = request.getParameter("alias");
                String tarif = request.getParameter("tarif");
                String adn = request.getParameter("adn");
                String umbcombo = request.getParameter("umbcombo");
		String name = request.getParameter("name");
		String cs = request.getParameter("cs");
		
		String command = request.getParameter("command");
		String trx_id = null;
		String u = null;
		String tgl = null;
		String ul = null;
		String api = null;
		int tp = 0;
		int dpn = 0;
		int a = 0;
		StringTokenizer token1;
        	Vector v1;
		String getSession = null;
		int flagbnum = 0;
		int flagptid = 0;
		String SISI = "xardin";

		try
		{
			//jika command adalah 0
			if (command.equals("0")) {
				Ses.DestroySubscriberMenuSession(msisdn);
			}
			else {
				command = URLEncoder.encode(command, "UTF-8");
				getSession = Ses.GetSubscriberMenuSession(msisdn);

				if (getSession.equals("dodol")) {}
				else {
			   		if (command.equals("1")) {
                        			token1 = new StringTokenizer(getSession, "|", false);
                        			v1 = new Vector();
                        			while(token1.hasMoreTokens())
                        			{
                        				v1.add(token1.nextToken());
                        			}
						a = Integer.parseInt((String)v1.get(0));
						if (command.equals("9")) {
							a = a - 1;
						} 
						//create trxid
						SimpleDateFormat format = new SimpleDateFormat("HHmmssyyMMdd");
                                		trx_id = "u"+""+format.format(new java.util.Date())+""+Seq.get();
						if ( a == 2 )
							Ses.DestroySubscriberMenuSession(msisdn);

			    		}
				}
			
				//get api
                                String url = "http://10.2.114.59:9000";
                                String path = "/cp/ussd_general/combo8.jsp?";

				if ( a == 1 ) {
					Ses.UpdateSubscriberMenuSession(msisdn, 2,1,"1",1,1,"1","1");
                                        SISI = "Konfirmasi berlangganan layanan dari "+name+". Tekan 1 untuk melanjutkan.\n1.Ya\n9.back";
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>"+SISI+"</data><back_code>9</back_code></umb>";
				}
				//untuk PUSH REG		
				else if ( a == 2 ) {
					keyword = "REG " + keyword;
					alias = URLEncoder.encode(alias, "UTF-8");
                                        umbcombo = URLEncoder.encode(umbcombo, "UTF-8");
                                        keyword = URLEncoder.encode(keyword, "UTF-8");
                                        path = path +"msisdn="+msisdn+"&keyword="+keyword+"&alias="+alias+"&umbcombo="+umbcombo+"&adn="+adn;
                                        api = url + path;

					ul = HTTP.get(url+path,0,10000);
                        		if(ul == null || ul.equals("dodol"))
                        		{
                                		u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>9</back_code></umb>";
                        		}

				
					//if (ul.startsWith("1")){
					if (!ul.equals("dodol")){
						u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>TerimaKasih.Permintaan Anda Sedang kami proses.</data><back_code>9</back_code></umb>";
					}else {
						u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>Mohon Maaf Permintaan Registrasi Anda Gagal. Silahkan coba kembali</data><back_code>9</back_code></umb>";
					}

					Ses.DestroySubscriberMenuSession(msisdn);
				}
				else {
					Ses.CreateSubscriberMenuSession(msisdn,1,1,"1",1,1,"1","1");
                                        SISI = "Anda akan berlangganan layanan "+service+" dgn tarif "+tarif+". Anda bersedia?\n1.Setuju";
                               		u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>"+SISI+"</data><back_code>9</back_code></umb>";
				}

				//create trx id tuk d record d log
				PushLog.record("USSD|"+trx_id+"|"+msisdn+"|"+adn+"|"+api);
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace(System.out);
			out.print(ee.getMessage());
			u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>Internal error</data><back_code>9</back_code></umb>";
		}
		
		PushLog.record("USSD|command : "+command);
		
		out.print(u);
		

      out.write("\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
