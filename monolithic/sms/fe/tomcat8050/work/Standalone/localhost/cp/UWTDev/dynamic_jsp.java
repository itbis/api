package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.telkomsel.cp.util.*;

public class dynamic_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      com.telkomsel.cp.util.Session Ses = null;
      synchronized (application) {
        Ses = (com.telkomsel.cp.util.Session) pageContext.getAttribute("Ses", PageContext.APPLICATION_SCOPE);
        if (Ses == null){
          try {
            Ses = (com.telkomsel.cp.util.Session) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Session");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Session", exc);
          }
          pageContext.setAttribute("Ses", Ses, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.Seq Seq = null;
      synchronized (application) {
        Seq = (com.telkomsel.cp.util.Seq) pageContext.getAttribute("Seq", PageContext.APPLICATION_SCOPE);
        if (Seq == null){
          try {
            Seq = (com.telkomsel.cp.util.Seq) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Seq");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Seq", exc);
          }
          pageContext.setAttribute("Seq", Seq, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.ResponseParser rp = null;
      synchronized (pageContext) {
        rp = (com.telkomsel.cp.util.ResponseParser) pageContext.getAttribute("rp", PageContext.PAGE_SCOPE);
        if (rp == null){
          try {
            rp = (com.telkomsel.cp.util.ResponseParser) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.ResponseParser");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.ResponseParser", exc);
          }
          pageContext.setAttribute("rp", rp, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.Enc mye = null;
      synchronized (application) {
        mye = (com.telkomsel.cp.util.Enc) pageContext.getAttribute("mye", PageContext.APPLICATION_SCOPE);
        if (mye == null){
          try {
            mye = (com.telkomsel.cp.util.Enc) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Enc");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Enc", exc);
          }
          pageContext.setAttribute("mye", mye, PageContext.APPLICATION_SCOPE);
        }
      }
      com.telkomsel.cp.util.DbCP2 Db = null;
      synchronized (pageContext) {
        Db = (com.telkomsel.cp.util.DbCP2) pageContext.getAttribute("Db", PageContext.PAGE_SCOPE);
        if (Db == null){
          try {
            Db = (com.telkomsel.cp.util.DbCP2) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.DbCP2");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.DbCP2", exc);
          }
          pageContext.setAttribute("Db", Db, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.DB Db2 = null;
      synchronized (pageContext) {
        Db2 = (com.telkomsel.cp.util.DB) pageContext.getAttribute("Db2", PageContext.PAGE_SCOPE);
        if (Db2 == null){
          try {
            Db2 = (com.telkomsel.cp.util.DB) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.DB");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.DB", exc);
          }
          pageContext.setAttribute("Db2", Db2, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.HTTPGet HTTP = null;
      synchronized (pageContext) {
        HTTP = (com.telkomsel.cp.util.HTTPGet) pageContext.getAttribute("HTTP", PageContext.PAGE_SCOPE);
        if (HTTP == null){
          try {
            HTTP = (com.telkomsel.cp.util.HTTPGet) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.HTTPGet");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.HTTPGet", exc);
          }
          pageContext.setAttribute("HTTP", HTTP, PageContext.PAGE_SCOPE);
        }
      }

		String msisdn = request.getParameter("msisdn");
   		String adn = request.getParameter("adn");
   		//String adn = "900";
		String command = request.getParameter("msg");
		String trx_id = null;
		String url_cp = request.getParameter("url_cp");
		String path_cp = request.getParameter("path_cp");
		path_cp = path_cp + "&";
		String u = null;
		String tgl = null;
		String ul = null;
		String api = null;
		int tp = 0;
		int dpn = 0;
		String keyword = null;
		String adn_push = null;
		String adnNy = null;
		String keyNy = null;
		String cp_name = null;
		int a = 0;
		StringTokenizer token1;
        	Vector v1;
		String getSession = null;
		int flagbnum = 0;
		int flagptid = 0;
		String cek_adn = "*" + adn;

		try
		{
			out.print("cek adn : " + cek_adn);
			//jika command adalah 0
			if (command.equals("0") || command.equals("9") || command.startsWith(cek_adn)) {
				Ses.DestroySubscriberMenuSession(msisdn);
			}
			command = URLEncoder.encode(command, "UTF-8");

			//dibalikin k menu sebelumny
			getSession = Ses.GetSubscriberMenuSession(msisdn);

			out.print("Isi session : " + getSession);

			if (getSession.equals("dodol")) {}
			else {
			   if (command.equals("1")) {
                        	token1 = new StringTokenizer(getSession, "|", false);
                        	v1 = new Vector();
                        	while(token1.hasMoreTokens())
                        	{
                        		v1.add(token1.nextToken());
                        	}

				
				keyword = (String)v1.get(2);
				tp = Integer.parseInt((String)v1.get(3)); 
				if (tp == 1) {
					a = 1;
					keyword = "REG+" + keyword;
				}
				else if (tp == 2) 
				{
					a = 1;
					keyword = "UNREG+" + keyword;
				}
				else if (tp == 3) a = 2;
				else if (tp == 4) {
					a = 3;
					adnNy = (String)v1.get(2);
					keyNy = (String)v1.get(5);
				}
				adn_push = (String)v1.get(5);
				cp_name = (String)v1.get(6);

				SimpleDateFormat format = new SimpleDateFormat("HHmmssyyMMdd");
                                trx_id = "u"+""+format.format(new java.util.Date())+""+Seq.get();
				Ses.DestroySubscriberMenuSession(msisdn);
			
				//command = "9";

			    }
			}
			
			//get api

			String url_push = "http://10.1.89.132:8300";
			String path_push = "/cp/mo_sms.jsp?";
			String url_pull = "http://10.1.89.132:8300";
			String path_pull = "/cp/ussd_req.jsp?";

			//untuk PUSH REG dan UNREG		
			if (a == 1) {
				path_push = path_push +"msisdn="+msisdn+"&adn="+adn_push+"&msg="+keyword;
				//url_push = URLEncoder.encode(url_push, "UTF-8");
				//path_push = URLEncoder.encode(path_push, "UTF-8");
				api = url_push + path_push;
				
				//ul = HTTP.Get(url_push,path_push,10000);
				ul = HTTP.get(url_push+path_push,0,10000);
                        	if(ul == null || ul.equals("dodol"))
                        	{
                                	u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>0</back_code></umb>";
                        	}

				
				//if (ul.startsWith("1")){
				if (!ul.equals("dodol")){
					u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Terima Kasih.Permintaan Anda Sedang kami proses(Push)</data><back_code>0</back_code></umb>";
				}else {
					u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Mohon Maaf Permintaan Registrasi Anda Gagal. Silahkan coba kembali</data><back_code>0</back_code></umb>";
				}
			}
			//untuk PULL
			else if (a == 2) {
				String cp_url = url_cp+path_cp+"adn="+adn+"&command="+command+"&msisdn="+msisdn+"&";
                                cp_url = URLEncoder.encode(cp_url, "UTF-8");
                                path_pull = path_pull + "cpurl="+cp_url+"&msisdn="+msisdn+"&trxid="+trx_id+"&msg="+command;
                                api = url_pull + path_pull;

                                //ul = HTTP.Get(url_pull,path_pull,10000);
                                ul = HTTP.get(url_pull+path_pull,0,10000);
                                if(ul == null || ul.equals("dodol"))
                                {
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>0</back_code></umb>";
                                }

                                if (ul.startsWith("1")){
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Terima Kasih.Permintaan Anda Sedang kami proses(Pull)</data><back_code>0</back_code></umb>";
                                }else {
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Mohon Maaf Permintaan Anda Gagal. Silahkan coba kembali</data><back_code>0</back_code></umb>";
                                }
                        }
			//untuk pull special
			else if (a == 3) {
				keyNy = URLEncoder.encode(keyNy, "UTF-8");
                                String cp_url = "http://10.1.89.132:8300/cp/mo_sms.jsp?adn="+adnNy+"&msg="+keyNy+"&msisdn="+msisdn+"&";
                                cp_url = URLEncoder.encode(cp_url, "UTF-8");
                                path_pull = path_pull + "cpurl="+cp_url+"&msisdn="+msisdn+"&trxid="+trx_id+"&msg="+command;
                                api = url_pull + path_pull;


                                //ul = HTTP.Get(url_pull,path_pull,10000);
                                ul = HTTP.get(url_pull+path_pull,0,10000);
                                if(ul == null || ul.equals("dodol"))
                                {
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>0</back_code></umb>";
                                }

                                if (ul.startsWith("1")){
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Terima Kasih.Permintaan Anda Sedang kami proses(Pull)</data><back_code>0</back_code></umb>";
                                }else {
                                        u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Mohon Maaf Permintaan Anda Gagal. Silahkan coba kembali</data><back_code>0</back_code></umb>";
                                }
                        }

			else {
				path_cp = path_cp + "msisdn="+msisdn+"&command="+command;
				//url_cp = URLEncoder.encode(url_cp, "UTF-8");
                                //path_cp = URLEncoder.encode(path_cp, "UTF-8");
				api = url_cp + path_cp;
				//out.print(api);

                                //ul = HTTP.Get(url_cp,path_cp,15000);
                                ul=HTTP.get(url_cp+path_cp,0,5000);
				//out.print(ul);
                                //ul = HTTP.ConventionalGet(api);
                        	if(ul == null || ul.equals("dodol"))
                        	{
                                	u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi ke content provider.</data><back_code>0</back_code></umb>";
                        	}else {
					u = ul;
				}
			}

			//create trx id tuk d record d log
			PushLog.record("USSD|"+trx_id+"|"+msisdn+"|"+adn+"|"+api);
		}
		catch(Exception ee)
		{
			ee.printStackTrace(System.out);
			out.print(ee.getMessage());
			u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Internal error</data><back_code>0</back_code></umb>";
		}
		finally {
		}

		String url_pull = "http://10.1.89.132:8300";
                String path_pull = "/cp/ussd_req.jsp?";
		
		//Parsing xml
		out.print(u);
		String jans = u;

		u = rp.Parse(u);  

		StringTokenizer token4 = new StringTokenizer(u, "|", false);
		Vector v4 = new Vector();
                while(token4.hasMoreTokens())
                {
                    v4.add(token4.nextToken());
                }

                u = (String)v4.get(0);
		String ff = (String)v4.get(1); 
		//out.print(ff);
		String ky = "a", ad = "b", cn = "c";
		if (ff.equalsIgnoreCase("L") || ff.equalsIgnoreCase("N") ) {}
		else if (ff.equalsIgnoreCase("L1") || ff.equalsIgnoreCase("DC")) {
			ad = (String)v4.get(2);
                        ky = (String)v4.get(3);

			//out.println(ky);

                        ky = ky.toUpperCase();
                        if (ky.startsWith("REG")) ff = "DR";
		}
		else {
			ky = (String)v4.get(2);
                	ad = (String)v4.get(3);
                	cn = (String)v4.get(4);

		}

		//penanda untuk pull
		if (ff.equalsIgnoreCase("L")){
			try {
				//inisialisasi
                                //msisdn, pos_prev, pos_curr, teks, tipe, next, l_command, hasil
                                Ses.CreateSubscriberMenuSession(msisdn,1,0," ",3,0," "," ");
			}
			catch(Exception e) {
				e.printStackTrace(System.out);
			}
 		}
		else if (ff.equalsIgnoreCase("DC")){
                        try {
				SimpleDateFormat format = new SimpleDateFormat("HHmmssyyMMdd");
                                trx_id = "u"+""+format.format(new java.util.Date())+""+Seq.get();
				String cp_url = "http://10.1.89.132:8300/cp/mo_sms.jsp?adn="+ad+"&msg="+ky+"&msisdn="+msisdn+"&";
                                cp_url = URLEncoder.encode(cp_url, "UTF-8");
                                path_pull = path_pull + "cpurl="+cp_url+"&msisdn="+msisdn+"&trxid="+trx_id+"&msg="+command;
                                api = url_pull + path_pull;
				//out.print(api);

                                //ul = HTTP.Get(url_pull,path_pull,10000);
                                ul = HTTP.get(url_pull+path_pull,0,10000);
                                if(ul == null || ul.equals("dodol"))
                                {
                                        jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>0</back_code></umb>";
                                }

                                if (ul.startsWith("1")){
                                        jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Terima Kasih.Permintaan Anda Sedang kami proses(Pull)</data><back_code>0</back_code></umb>";
                                }else {
                                        jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Mohon Maaf Permintaan Anda Gagal. Silahkan coba kembali</data><back_code>0</back_code></umb>";
                                }
                        }
                        catch(Exception e) {
                                e.printStackTrace(System.out);
                        }
                }
		//penanda pull special
		else if (ff.equalsIgnoreCase("L1")){
                        try {
                                //inisialisasi
                                //msisdn, pos_prev, pos_curr, teks, tipe, next, l_command, hasil
                                Ses.CreateSubscriberMenuSession(msisdn,1,0,ad,4,0,ky," ");
				 u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>Apakah Anda Akan membeli Content ini?\n1.Ya</data><back_code>9</back_code></umb>";
                        }
                        catch(Exception e) {
                                e.printStackTrace(System.out);
                        }
                }

		//penanda untuk REG Push
		else if (ff.equalsIgnoreCase("R")){

                        try {
                                //inisialisasi
                                //msisdn, pos_prev, pos_curr, teks, tipe, next, l_command, hasil
                                Ses.CreateSubscriberMenuSession(msisdn,1,0,ky,1,0,ad,cn);
								//u = "Anda akan dikenakan Tarif Rp."+cn+"? 1.Ya";
                        	u = cn+" 1.Ya";
				jans = u;
				jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>"+jans+"</data><back_code>9</back_code></umb>";
			}
                        catch(Exception e) {
                                e.printStackTrace(System.out);
                        }
                }
		//penanda untuk UNREG Push
		else if (ff.equalsIgnoreCase("U")){

                        try {
                                //inisialisasi
                                //msisdn, pos_prev, pos_curr, teks, tipe, next, l_command, hasil
                                Ses.CreateSubscriberMenuSession(msisdn,1,0,ky,2,0,ad,cn);
				u = "Apakah Anda yakin utk berhenti berlangganan dgn Tarif Rp."+cn+"? Tekan 1 utk Ya";
                        	jans = u;
				jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>No</first><data>"+jans+"</data><back_code>9</back_code></umb>";
			}
                        catch(Exception e) {
                                e.printStackTrace(System.out);
                        }
                }
		//direct REG and direct UNREG
		else if (ff.equalsIgnoreCase("DR") || ff.equalsIgnoreCase("DU")) {
			try {
				//boolean bo = false;
				String reply = "1";
				//cek valid keyword adn dan cpname
				//bo = Db2.isValid(ky,ad,cn); 
				reply = Db2.isValidResp(ky,ad,cn);
		
				//tembak langsung push
				if (!reply.equals("0")) {
					String url_push = "http://10.1.89.132:8300";
                        		String path_push = "/cp/mo_sms.jsp?";

					keyword = ky;

					if (ff.equalsIgnoreCase("DR")) keyword = "REG " + ky;
					if (ff.equalsIgnoreCase("DU")) keyword = "UNREG " + ky;
			
                        		//untuk PUSH REG dan UNREG
					keyword = URLEncoder.encode(keyword, "UTF-8");
                                	path_push = path_push +"msisdn="+msisdn+"&adn="+ad+"&msg="+keyword;
                                	//url_push = URLEncoder.encode(url_push, "UTF-8");
                                	//path_push = URLEncoder.encode(path_push, "UTF-8");
                                	api = url_push + path_push;
					//out.println(api);

                                	//ul = HTTP.Get(url_push,path_push,10000);
                                	ul = HTTP.get(url_push+path_push,0,10000);
					//out.print(ul);
                                	if(ul == null || ul.equals("dodol"))
                                	{
                                        	u = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Maaf kami sedang mengalami gangguan koneksi.</data><back_code>0</back_code></umb>";
						jans = u;
                                	}

                                	//if (ul.startsWith("1")){
                                	if (!ul.equals("dodol")){
                                        	//jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Terima Kasih.Permintaan Anda Sedang kami proses(Push)</data><back_code>0</back_code></umb>";
                                        	jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>"+reply+"</data><next>L</next><back_code>0</back_code></umb>";
                                	}else {
                                        	jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Mohon Maaf Permintaan Registrasi Anda Gagal. Silahkan coba kembali</data><back_code>0</back_code></umb>";
                                	}
				}
				else {
					jans = "<?xml version=\"1.0\"?><umb><type>Content</type><first>Yes</first><data>Program ini belum berlaku, Maaf Anda belum bisa melakukan registrasi</data><back_code>0</back_code></umb>";
				}
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}					

		//out.print(u);
		jans = rp.Parse(jans);
		StringTokenizer token5 = new StringTokenizer(jans, "|", false);
                Vector v5 = new Vector();
                while(token5.hasMoreTokens())
                {
                    v5.add(token5.nextToken());
                }
		jans = (String)v5.get(0);
		
		out.print(jans);
		

      out.write("\n\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
