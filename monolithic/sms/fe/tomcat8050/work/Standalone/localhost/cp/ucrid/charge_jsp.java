package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import com.telkomsel.cp.util.*;

public class charge_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      com.telkomsel.cp.util.HttpGet HTTP = null;
      synchronized (pageContext) {
        HTTP = (com.telkomsel.cp.util.HttpGet) pageContext.getAttribute("HTTP", PageContext.PAGE_SCOPE);
        if (HTTP == null){
          try {
            HTTP = (com.telkomsel.cp.util.HttpGet) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.HttpGet");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.HttpGet", exc);
          }
          pageContext.setAttribute("HTTP", HTTP, PageContext.PAGE_SCOPE);
        }
      }


    	String msisdn = request.getParameter("msisdn");
	//msisdn = URLEncoder.encode(msisdn, "UTF-8");
	String urlCharge = "http://10.2.114.222:8060";
	String pathCharge = "/ROOT/Charge/charge.jsp?";
	String appsid = "ussd50";
	String password = "ussd321";
	String cpname = "cp836";
	String orderid = "836";
	String bnumber = "836";

	
	String msg = request.getParameter("msg");
	String url808 = "http://localhost:6001";
	String path808 = "/callme?key=CM&smsc=USSD&adn=808";
	
	try
	{
	  	//out.print(Db.GetArsProfile(msisdn));
	  	//out.print("ARS Profile : "+msisdn +" -  " + Db.GetArsProfile(msisdn) + "<br>");
	  	//if(Db.GetArsProfile(msisdn).equals("1") || Db.GetArsProfile(msisdn).equals("2") || Db.GetArsProfile(msisdn).equals("3")) {
		
		SimpleDateFormat format = new SimpleDateFormat("HHmmssyyMMdd");
                String trxid = "u"+format.format(new Date())+""+msisdn;

		path808 = path808 + "&msg="+msg+"&msisdn="+ msisdn +"&trxid="+ trxid;
                String _hit808 = HTTP.Get(url808, path808, 10000);
                //out.print(_hit808 + "<br>");
		if (_hit808.equals("dodol")) {
			out.print("Maaf Sistem Sedang Sibuk");	
		}
		else {
			out.print(_hit808);
			pathCharge = pathCharge + "msisdn=" + msisdn + "&appsid=" + appsid + "&password=" + password + "&cpname=" + cpname + "&orderid=" + orderid + "&bnumber=" + bnumber + "&trxid=" + trxid + "&ctype=0&sepid=0" ;
                	String _getCharge = HTTP.Get(urlCharge, pathCharge, 5000);
			//out.print(_getCharge + "<br>");
		}

	     	PushLog.record("ucrid|"+trxid+"|"+msisdn);	

	}
	catch(Exception ee)
	{
		ee.printStackTrace(System.out);
		out.println(ee.getMessage());
	}

		

      out.write("\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
