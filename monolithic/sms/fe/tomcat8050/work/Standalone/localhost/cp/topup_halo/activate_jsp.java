package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import com.telkomsel.cp.util.*;

public class activate_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      com.telkomsel.cp.util.Db_topup Db = null;
      synchronized (pageContext) {
        Db = (com.telkomsel.cp.util.Db_topup) pageContext.getAttribute("Db", PageContext.PAGE_SCOPE);
        if (Db == null){
          try {
            Db = (com.telkomsel.cp.util.Db_topup) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.Db_topup");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.Db_topup", exc);
          }
          pageContext.setAttribute("Db", Db, PageContext.PAGE_SCOPE);
        }
      }
      com.telkomsel.cp.util.HttpGet HTTP = null;
      synchronized (pageContext) {
        HTTP = (com.telkomsel.cp.util.HttpGet) pageContext.getAttribute("HTTP", PageContext.PAGE_SCOPE);
        if (HTTP == null){
          try {
            HTTP = (com.telkomsel.cp.util.HttpGet) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "com.telkomsel.cp.util.HttpGet");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "com.telkomsel.cp.util.HttpGet", exc);
          }
          pageContext.setAttribute("HTTP", HTTP, PageContext.PAGE_SCOPE);
        }
      }


    	String msisdn = request.getParameter("msisdn");
	//msisdn = URLEncoder.encode(msisdn, "UTF-8");
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	String msg = request.getParameter("msg");
	String cekuser = "0";
	String u = null;
	String trxid = null;
	
	try
	{
		if (msisdn.startsWith("+")) {
			msisdn = msisdn.replaceFirst("+","");
		}
		//trxid
		SimpleDateFormat format = new SimpleDateFormat("HHmmssyyMMdd");
                trxid = format.format(new Date())+""+msisdn;
		if (msg.equals("1") || msg.equals("2")) {
			//cek username & password
			cekuser = Db.cek_user(username,password,msisdn,msg);
			if (cekuser.equals("1")) 
			{
				u = "SA";
				//Success activate
				String spin = HTTP.Get("http://10.1.89.196:8080","/topup/sxpin.jsp?msisdn="+msisdn, 7000);
				//u = u+"|"+spin;	
			}
			else if (cekuser.equals("2")) {
				u = "Err:03";
				//msisdn has been registered
			}
			else if (cekuser.equals("3")) {
                                u = "SD";
                                //Success Deactivate
                        }
			else if (cekuser.equals("4")) {
                                u = "Err:04";
                                //msisdn not registered cannot be deactivate
                        }
			else {
				out.print(cekuser);
				u = "Err:01";
				//wrong username & password
			}
		}
		else {
			u = "Err:02";
			//message invalid outside of 1 or 2
		}
	
	     	PushLog.record("Activation TopupHalo|"+trxid+"|"+msisdn+"|"+msg+"|"+u);	

	}
	catch(Exception ee)
	{
		ee.printStackTrace(System.out);
		out.println(ee.getMessage());
	}

	out.print(u);	
		

      out.write("\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
