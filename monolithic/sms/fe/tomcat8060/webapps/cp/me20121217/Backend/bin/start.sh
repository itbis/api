#!/bin/bash

cd /apps/tomcat8060/webapps/cp/me/Backend/

if [ -f u.pid ]; then
PID=`cat u.pid`
fi

if [ "$PID" = "" ]
then
   java -Xms128m -Xmn64m -Xmx1024m -verbose:gc -cp lib/commons-collections-3.1.jar:lib/mysql-connector-java-3.1.7-bin.jar:lib/commons-pool-1.2.jar:lib/commons-dbutils-1.1.jar:lib/commons-dbcp-1.2.1.jar:lib/commons-httpclient-3.0.1.jar:lib/jdom.jar:lib/log4j-1.2.9.jar:lib/log4j-1.2.9.jar:. src.DbDaemon /apps/tomcat8060/webapps/cp/me/Backend/conf/cweeta.conf > log/out.txt 2>&1 & echo $! > u.pid
else
    echo "udah jalan : $PID"
fi
