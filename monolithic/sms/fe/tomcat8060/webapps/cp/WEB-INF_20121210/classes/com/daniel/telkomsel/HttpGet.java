package com.daniel.telkomsel;

import HTTPClient.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class HttpGet {


public static String get(String url, String remFile, int timeout)  {
	try {
		URL u = new URL(url);
		HTTPConnection con = new HTTPConnection(u);
		CookieModule.setCookiePolicyHandler(null);
		con.setTimeout(timeout);
		//NVPair[] def_hdrs = { new NVPair("Connection", "close") };
		NVPair[] def_hdrs = { new NVPair("User-Agent", "xxx-agent") };
    		con.setDefaultHeaders(def_hdrs);
    		//con.setDefaultHeaders(def_hdrs);

		HTTPResponse rsp = con.Get(remFile);

		if (rsp.getStatusCode() >= 300) {
			//return "error "+rsp.getText();
			System.err.println(new Date()+" "+rsp.getText());
			return "timeout / error";
		} else {
			return rsp.getText();
		}
	} catch (Exception i) {
		System.err.println(new Date()+" exception in "+url+" "+remFile);
		i.printStackTrace(System.err);	
		return "timeout / error";
	}

}

public static String [] get2(String addr) {
	String [] ret = new String[2];
	ret[1] = "";
	 try
                {
                       URL url = new URL(addr);
                       InputStream in1 = url.openStream();
                       BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
                        String l = "";
                        while((l = br1.readLine()) != null)
                        {
                                ret[1] = ret[1].concat(l);
                        }
                        br1.close();
			ret[0] = "OK";
			return ret;
                }
                catch(Exception e)
                {
                        System.out.println("error :"+addr+" : "+e);
			ret[0] = "NOK";
			ret[1] = String.valueOf(e);
			return ret;
                }


}

public static void main(String [] args) {
	//String msisdn = args[0];
	String status = HttpGet.get("http://202.53.227.147","/tsel_mq/pull.asp?msisdn=62811107177&sms=Mq",120000);
	//if (status.equals("error\n")) status = "prepaid";
	//else status = "postpaid";
	System.out.println(status);
}

}
