package cweeta.telkomsel.util;

import java.util.Hashtable;
import org.apache.log4j.Logger;

public class Function
{
	public static Logger logger = Logger.getLogger(Function.class);

	public Function()
	{
	}

	public static String stringToHex(String base)
    	{
     		StringBuffer buffer = new StringBuffer();
     		int intValue;
     		for(int x = 0; x < base.length(); x++)
         	{
         		int cursor = 0;
         		intValue = base.charAt(x);
         		String binaryChar = new String(Integer.toBinaryString(base.charAt(x)));
         		for(int i = 0; i < binaryChar.length(); i++)
             		{
             			if(binaryChar.charAt(i) == '1')
                 		{
                 			cursor += 1;
             			}
         		}
         		if((cursor % 2) > 0)
             		{
             			intValue += 128;
         		}
         		buffer.append(Integer.toHexString(intValue) + " ");
     		}
     		return buffer.toString();
	}

	public boolean CheckLength(String _msg)
	{
		boolean ret = false;
		try {
			if (_msg.length() > 130) ret = false;
			else ret = true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public String CheckQueue(String _getSess) {
		String ret = null;
		try {
			String [] _split = _getSess.split(";");
			ret = _split[7];
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public int CheckDepthMenu(String _getSess) {
		int ret = 0;
		try {
			String [] _split = _getSess.split(";");
                        ret = Integer.parseInt(_split[2]);
		}	
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public int GetLcc(String _getSess) {
                int ret = 0;
                try {
                        String [] _split = _getSess.split(";");
                        ret = Integer.parseInt(_split[4]);
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }

	public int GetCorres(String _getSess) {
                int ret = 0;
                try {
                        String [] _split = _getSess.split(";");
                        ret = Integer.parseInt(_split[6]);
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }

	public String [] parseState(String state) {
		String [] ret = new String[10];
		try {
			ret = state.split(":");

		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public String GetProfileSess(String _getSess) {
                String ret = "0000000000";
                try {
                        String [] _split = _getSess.split(";");
                        ret = _split[7];
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
}
