package me;

import java.util.Hashtable;
import org.apache.log4j.Logger;

public class Session
{
	public static Logger logger = Logger.getLogger(Session.class);

	Hashtable hSession;
	Hashtable hContentSession;
	
	public Session()
	{
		hSession = new Hashtable();
	}

	public void CreateSession(String msisdn, String isi)
	{
		try
		{
			hSession.put(msisdn, new String(isi));
			logger.info(msisdn + "|CreateSession: " + isi);
		}
		catch(Exception e)
		{
			logger.fatal("CreateSession: " + e.getMessage(), e);
		}
	}
	
	public String GetSession(String msisdn)
	{
		try
		{
			if(hSession.containsKey(msisdn))
			{
				logger.info(msisdn + "|GetSession: " + (String)hSession.get(msisdn));
				return (String)hSession.get(msisdn);
			}
			else
			{
				logger.info(msisdn + "|GetSession: dodol");
				return "dodol";
			}
		}
		catch(Exception e)
		{
			logger.fatal("GetSession: " + e.getMessage(), e);
			return "dodol";
		}
	}
	
	public void DestroySession(String msisdn)
	{
		try
		{
			hSession.remove(msisdn);
			logger.info(msisdn + "|DestroySession: " + (String)hSession.remove(msisdn));
		}
		catch(Exception e)
		{
			logger.fatal("DestroySession: " + e.getMessage(), e);
		}
	}
	
	public void UpdateSession(String msisdn, String isi)
	{
		try
		{
			hSession.remove(msisdn);
			hSession.put(msisdn, new String(isi));
			logger.info(msisdn + "|UpdateSession: " + isi);
		}
		catch(Exception e)
		{
			logger.fatal("UpdateSession: " + e.getMessage(), e);
		}
	}

	public void ResetSession()
	{
		try
		{
			hSession.clear();
		}
		catch(Exception e)
		{
			logger.fatal("ResetSession: " + e.getMessage(), e);
		}
	}

	public int GetSizeSession()
	{
		logger.info("GetSizeSession: " + hSession.size());
		return hSession.size();
	}

}
