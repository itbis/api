package com.daniel.telkomsel;
import java.io.*;
import java.util.*;

public class CGProp{
	public CGProp(){}
	
	public String getProperty(String name,String path){
		String result="";
		InputStream fin = CGProp.class.getResourceAsStream(path);
		Properties p = new Properties();
		try
		{
			p.load(fin);
			result = p.getProperty(name);
			fin.close();
		}
		catch(IOException iox)
		{
			iox.printStackTrace();
		}
		return result;
	}
	
	public static void main(String[] args){
		CGProp prop = new CGProp();
		System.out.println(prop.getProperty(args[0],args[1]));
	}
}
