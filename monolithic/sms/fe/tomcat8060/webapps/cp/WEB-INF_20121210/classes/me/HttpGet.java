package me;

import HTTPClient.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class HttpGet
{
	public static String Get(String url, String remFile, int timeout)
	{
		try
		{
			URL u = new URL(url);
			HTTPConnection con = new HTTPConnection(u);
			CookieModule.setCookiePolicyHandler(null);
			con.setTimeout(timeout);
			NVPair[] def_hdrs = {new NVPair("User-Agent", "TSEL-Agent")};
    		con.setDefaultHeaders(def_hdrs);

			HTTPResponse rsp = con.Get(remFile);
			if(rsp.getStatusCode() >= 300)
			{
				System.out.println(new Date() + " HTTP GET: " + rsp.getText());
				return "dodol";
			}
			else
			{
				return rsp.getText();
			}
		}
		catch(Exception i)
		{
			i.printStackTrace(System.out);	
			return "dodol";
		}
	}

	public static String ConventionalGet(String address)
	{
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in = url.openStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while((line = br.readLine()) != null)
			{
				res = res.concat(line);
			}
			br.close();
			in.close();
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
			res = "dodol";
		}
        return res;
	}
}
