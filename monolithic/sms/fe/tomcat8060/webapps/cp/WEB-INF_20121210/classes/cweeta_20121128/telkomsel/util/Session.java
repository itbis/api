package cweeta.telkomsel.util;

import java.util.Hashtable;
import org.apache.log4j.Logger;

public class Session
{
	public static Logger logger = Logger.getLogger(Session.class);

	Hashtable hMenuSession;
	Hashtable hContentSession;
	
	public Session()
	{
		hMenuSession = new Hashtable();
		hContentSession = new Hashtable();
	}

	public void CreateSubscriberMenuSession(String msisdn, int rootid, int cumid, int cmid, int getlcumcode, int lcumcode, int input, int corres, String backcode)
	{
		try
		{
			hMenuSession.put(msisdn, new String(rootid + ";" + cumid + ";" + cmid + ";" + getlcumcode + ";" + lcumcode + ";" + input + ";" + corres + ";" + backcode));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String GetSubscriberMenuSession(String msisdn)
	{
		try
		{
			if(hMenuSession.containsKey(msisdn))
			{
				return (String)hMenuSession.get(msisdn);
			}
			else
			{
				return "dodol";
			}
		}
		catch(Exception e)
		{
			return "dodol";
		}
	}
	
	public void DestroySubscriberMenuSession(String msisdn)
	{
		try
		{
			hMenuSession.remove(msisdn);
			logger.info(msisdn + "|DestroySubscriberMenuSession: " + (String)hMenuSession.remove(msisdn));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void UpdateSubscriberMenuSession(String msisdn, int rootid, int cumid, int cmid, int getlcumcode, int lcumcode, int input, int corres, String backcode)
	{
		try
		{
			hMenuSession.remove(msisdn);
			hMenuSession.put(msisdn, new String(rootid + ";" + cumid + ";" + cmid + ";" + getlcumcode + ";" + lcumcode + ";" + input + ";" + corres + ";" + backcode));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void ResetMenuSession()
	{
		try
		{
			hMenuSession.clear();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public int GetSizeMenuSession()
	{
		return hMenuSession.size();
	}

}
