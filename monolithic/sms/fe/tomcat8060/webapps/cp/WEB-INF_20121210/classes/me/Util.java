package me;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.log4j.Logger;

public class Util
{
        public static Logger logger = Logger.getLogger(Util.class);
	public Util() {}

	public String createXmlMenu(String isFirst, String header, String isi, String isNext) {
		String u = "";
		String [] isiXml = isi.split(";");
		int pIsiXml = isiXml.length;
                String data = "";

		if (!header.equals("")) data = data + "<data code=\"" + 0 + "\">"+header+"</data>";
		
		int j = 0;
                for (int i = 0; i < pIsiXml ; i++ ) {
			j = i + 1;
			data = data + "<data code=\"" + j + "\">"+isiXml[i]+"</data>";
		}

		u = "<?xml version=\"1.0\"?>"
                    + "<umb>"
		    + "<type>Menu</type>"
		    + "<first>"+isFirst+"</first>" 
                    + "<menu totalmenu=\"" + pIsiXml  +"\">"
		    + data
                    + "</menu>"
		    + "<next>"+isNext+"</next>"	
		    + "<back_code>9</back_code>"
		    + "</umb>";

		logger.info("XMl structure : " + u);

		return u;
	}

	public String createXmlContent(String isFirst, String isi, String isNext) {
		String u = "";

		u = "<?xml version=\"1.0\"?>"
                    + "<umb>"
                    + "<type>Content</type>"
                    + "<first>"+isFirst+"</first>"
                    + "<data>"
                    + isi
                    + "</data>"
                    //+ "<next>"+isNext+"</next>"
                    + "<back_code>9</back_code>"
                    + "</umb>";

                logger.info("XMl structure : " + u);

		return u;
	}

        public int modNumber(String msisdn, int pembagi) {
		int nilai;
		String getAkhir = msisdn.substring(msisdn.length()-1,msisdn.length());
		nilai = Integer.parseInt(getAkhir) % pembagi;
		
		return nilai;
	}
}
