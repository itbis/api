package cweeta.telkomsel.util;

import javax.naming.*;
import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public class Db
{
	private Context ctx;
	private DataSource ds;
    	private Connection dbConn;
    	private Statement stmt;
    	private ResultSet rs;
	private String query;
	private PreparedStatement pstmt;
	private boolean b;
	private Vector v;
	private String smsid;
	private int id;
	private String trxid;
	private String keyword;

	private Statement stmt1;
    private ResultSet rs1;
	private PreparedStatement pstmt1;
	
	private Statement stmt2;
    private ResultSet rs2;
	private PreparedStatement pstmt2;
	
	java.util.Date skr;
	SimpleDateFormat formatter;
	String tdate;

    	public Db()
    	{
		try
    		{
      			ctx = new InitialContext();
      			if(ctx == null)
          			throw new Exception("Boom - No Context");

      			ds = (DataSource)ctx.lookup("java:comp/env/jdbc/cweeta");
	    	}
	    	catch(Exception e)
	    	{
      			e.printStackTrace();
    		}
    	}

    	public Connection createDbConnection() throws IOException, SQLException
    	{
		if(ds != null)
		{
	   		dbConn = ds.getConnection();
        	}
		else
		{
			System.out.println(" ds null");
		}
        	return dbConn;
    	}

    	public void closeDbConnection()
    	{
        	try
        	{
            		if(dbConn != null)
			{
				dbConn.close();
				dbConn = null;
				System.out.println(" Close CP Db Connection");
            		}
        	}
        	catch(Exception exception)
        	{
            		System.out.println("!!!!!! [ ERROR ] Close CP Db Connection Exception: " + exception.getMessage());
        	}
    	}

    	public ResultSet doQuery(String s) throws IOException, SQLException
    	{
    		if(dbConn == null || dbConn.isClosed())
    	   		createDbConnection();

       		System.out.println("CP Db Connection Insert : "+dbConn);
        	stmt = dbConn.createStatement();
        	rs = stmt.executeQuery(s);

        	return rs;
    	}

    	public void doUpdate(String s) throws IOException, SQLException
    	{
    		if(dbConn == null || dbConn.isClosed())
    	   		createDbConnection();

       		System.out.println("CP Db Connection Update : "+dbConn);
        	stmt = dbConn.createStatement();
        	stmt.executeUpdate(s);
        	stmt.close();
    	}

    	public boolean CheckWL(String msisdn) {
		boolean bo = false;
		String a = "";
		try {
			if (dbConn == null || dbConn.isClosed()) 
				createDbConnection();
			a = "Select * from whitelist Where msisdn = ?";
			PreparedStatement ps = dbConn.prepareStatement(a);
			ps.setString(1,msisdn);

			ResultSet rs = ps.executeQuery();

			if(rs.next()){
				bo = true;
			}
		
			rs.close();
			ps.close();
			closeDbConnection();

		}
		catch(Exception e) {
			e.printStackTrace(System.out);
			closeDbConnection();
		}

		return bo;
    	}

	public void QueueTrxid(String trxid, String msg) {
                String a = "";
                try {
                        if (dbConn == null || dbConn.isClosed())
                                createDbConnection();
                        a = "INSERT INTO queue_trxid VALUES (?, ?)";
                        PreparedStatement ps = dbConn.prepareStatement(a);
                        ps.setString(1,trxid);
                        ps.setString(2,msg);
                        ps.executeUpdate();
                        ps.close();
                        closeDbConnection();

                }
                catch(Exception e) {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }
	
	public String CheckQueueComment(String msisdn) {
                String ret = "";
                String a = "";
		String bnumber = "";
                try {
                        if (dbConn == null || dbConn.isClosed())
                                createDbConnection();
                        a = "Select * from queue_comment Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(a);
                        ps.setString(1,msisdn);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
				bnumber = rs.getString("bnumber");
                                ret = rs.getString("bnumber") +";" + Integer.toString(rs.getInt("id_status"));
                        } else ret = "-99";

                        rs.close();
                        ps.close();

			if(!ret.equals("-99")) {

				//get anumber and anumber_name
				a = "Select * from member Where msisdn = ?";
                        	ps = dbConn.prepareStatement(a);
                        	ps.setString(1,msisdn);
                        	rs = ps.executeQuery();

                        	if(rs.next()){
                                	ret = ret + ";" + rs.getString("direct_code") +";" + rs.getString("nama");
                        	}

                        	rs.close();
                        	ps.close();

				//bnumber_msisdn
				//get anumber and anumber_name
                                a = "Select * from member Where direct_code = ?";
                                ps = dbConn.prepareStatement(a);
                                ps.setString(1,bnumber);
                                rs = ps.executeQuery();

                                if(rs.next()){
                                        ret = ret + ";" + rs.getString("msisdn");
                                }
				

			}

                        closeDbConnection();

                }
                catch(Exception e) {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

                return ret;
        }
	
	  public boolean CheckFollower(String anumber,String bnumber) {
                boolean bo = false;
                String a = "";
                try {
                        if (dbConn == null || dbConn.isClosed())
                                createDbConnection();
                        a = "Select * from follower Where anumber = ? and bnumber = ?";
                        PreparedStatement ps = dbConn.prepareStatement(a);
                        ps.setString(1,anumber);
                        ps.setString(2,bnumber);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
                                bo = true;
                        }

                        rs.close();
                        ps.close();
                        closeDbConnection();

                }
                catch(Exception e) {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

                return bo;
        }

	public boolean CheckMember(String msisdn) {
                boolean bo = false;
                String a = "";
                try {
                        if (dbConn == null || dbConn.isClosed())
                                createDbConnection();
                        a = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(a);
                        ps.setString(1,msisdn);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
                                bo = true;
                        }

                        rs.close();
                        ps.close();
                        closeDbConnection();

                }
                catch(Exception e) {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

                return bo;
        }
	
	public String GetMember(String msisdn) {
		String res="";
		String a="";
                try {
                        if (dbConn == null || dbConn.isClosed())
                                createDbConnection();
                        a = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(a);
                        ps.setString(1,msisdn);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
			   res=rs.getString("direct_code");
                        }

                        rs.close();
                        ps.close();
                        closeDbConnection();

                }
                catch(Exception e) {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

                return res;
        }

	public void AddQueueComment(String msisdn, int topic, String bnumber)
        {
                String q = "";
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "INSERT INTO queue_comment VALUES('', ?, ?, ?)";
                        pstmt = dbConn.prepareStatement(q);
                        pstmt.clearParameters();
                        pstmt.setString(1, msisdn);
                        pstmt.setInt(2, topic);
			pstmt.setString(3, bnumber);
                        pstmt.executeUpdate();
                        pstmt.close();

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }

		public String AddQueueGroupComment(int idgroup, String anumber, String nama, String msg)
        {
                String q = "";
				String res="-1";
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "INSERT INTO `group_komentar` VALUES('', ?, ?, ?, ?, NOW())";
						
						//pstmt = dbConn.prepareStatement(q,Statement.RETURN_GENERATED_KEYS);
						pstmt = dbConn.prepareStatement(q);
						pstmt.clearParameters();
                        pstmt.setInt(1, idgroup);
                        pstmt.setString(2, anumber);
						pstmt.setString(3, nama);
						pstmt.setString(4, msg);
                        pstmt.executeUpdate();
                        pstmt.close();
						
						res="1";
						q = "Select * from group_member Where id_group = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setInt(1,idgroup);
                        ResultSet rs = ps.executeQuery();
						
						
                        while(rs.next()){
								
								String q1 = "INSERT INTO queue_comment_group VALUES('', ?, 0, ?, NOW())";
								pstmt = dbConn.prepareStatement(q1);
								pstmt.clearParameters();
								pstmt.setString(1, rs.getString("msisdn"));
								pstmt.setString(2, msg);
								pstmt.executeUpdate();
								pstmt.close();
								
								
							
                        }
						rs.close();
                        ps.close();
						
                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
				return res;
        }
		
		
		
	public void UpdateStatus(String msisdn, String msg)
        {
                String q = "";
		int no_member = 0;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

						q = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,msisdn);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
                                no_member = rs.getInt("no");
                        }

                        rs.close();
                        ps.close();

                        q = "INSERT INTO status VALUES('', ?, ?, now())";
                        pstmt = dbConn.prepareStatement(q);
                        pstmt.clearParameters();
                        pstmt.setInt(1, no_member);
                        pstmt.setString(2, msg);
                        pstmt.executeUpdate();
                        pstmt.close();

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }

	public void UpdateComment(String msisdn, int _idStat,String _aNum,String _aNumName,String _bNum,String msg)
        {
                String q = "";
                int no_member = 0;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "INSERT INTO komentar VALUES('', ?,?,?,?,?, now()) ";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setInt(1,_idStat);
                        ps.setString(2,_aNum);
                        ps.setString(3,_aNumName);
                        ps.setString(4,_bNum);
                        ps.setString(5,msg);
                        ps.executeUpdate();
                        ps.close();

			q = "DELETE FROM queue_comment WHERE msisdn = ?";
                        ps = dbConn.prepareStatement(q);
                        ps.setString(1,msisdn);
			ps.executeUpdate();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }

	public void UpdateComment(int _idStat,String _aNum,String _aNumName,String _bNum,String msg)
        {
                String q = "";
                int no_member = 0;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "INSERT INTO komentar VALUES('', ?,?,?,?,?, now()) ";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setInt(1,_idStat);
                        ps.setString(2,_aNum);
                        ps.setString(3,_aNumName);
                        ps.setString(4,_bNum);
                        ps.setString(5,msg);
                        ps.executeUpdate();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }

	public void InsertUndang(String msisdn)
    	{
		String q = "";
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDbConnection();
			
			q = "INSERT INTO whitelist VALUES('', ?)";
			pstmt = dbConn.prepareStatement(q);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.executeUpdate();
			pstmt.close();
			closeDbConnection();
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
			closeDbConnection();
		}
    	}
	public void UpdateFollower(String anumber,String bnumber,int stat)
        {
                String q = "";
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();
			if(stat==1){
                        q = "REPLACE INTO follower VALUES('', ?, ?,NOW())";
                        pstmt = dbConn.prepareStatement(q);
                        pstmt.clearParameters();
                        pstmt.setString(1, anumber);
                        pstmt.setString(2, bnumber);
                        pstmt.executeUpdate();
						pstmt.close();

                       }else{
						q = "DELETE from follower where anumber=? and bnumber= ?";
						pstmt = dbConn.prepareStatement(q);
                        pstmt.clearParameters();
                        pstmt.setString(1, anumber);
                        pstmt.setString(2, bnumber);
                        pstmt.executeUpdate();
                        pstmt.close();
	
			} 
			
                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }
        }
		
	public String AddGroup(String msisdn, String anumber, String nama)
        {
		String ret = "-1";
        String q = "";
		boolean bo = true;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();
							//get group name
							q = "Select nama from `group` Where nama = ?";
                        
							PreparedStatement ps = dbConn.prepareStatement(q);
							ps.setString(1,nama);
                        	rs = ps.executeQuery();
                        	if(rs.next()){
                                	bo= false;
								
                        	}

                        	rs.close();
                        	ps.close();

							if(bo){
								
								q = "INSERT INTO `group` VALUES('', ?,?,?,NOW())";
								pstmt = dbConn.prepareStatement(q,Statement.RETURN_GENERATED_KEYS);
								pstmt.clearParameters();
								pstmt.setString(1, anumber);
								pstmt.setString(2, msisdn);
								pstmt.setString(3, nama);
								pstmt.executeUpdate();
								
								rs = pstmt.getGeneratedKeys();
								
								if (rs.next()){
								
									q = "REPLACE INTO `group_member` VALUES('', ?,?,?,NOW())";
									pstmt = dbConn.prepareStatement(q);
									pstmt.clearParameters();
									pstmt.setInt(1, rs.getInt(1));
									pstmt.setString(2, msisdn);
									pstmt.setString(3, anumber);
									pstmt.executeUpdate();
									pstmt.close();
									
									ret="1";
								
								}
							}

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

		return ret;
        }

		
	public String Registration(String msisdn, String _nama, String _tglLahir, String _JK)
        {
		String ret = null;
                String q = "";
		String aksesQ = "";
		int no = 0;
		boolean bo = false;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

			//check msisdn has been exist or not
			q = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,msisdn);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
                                bo = true;
				aksesQ = rs.getString("direct_code");
                        }

                        rs.close();
                        ps.close();

			if(bo) {
				ret = "Anda telah terdaftar sebelumnya dengan aksesQ : " + aksesQ;
			}
			else {
                        	q = "INSERT INTO member VALUES('', ?,'',?,?,?,'*')";
                        	pstmt = dbConn.prepareStatement(q);
                        	pstmt.clearParameters();
                        	pstmt.setString(1, msisdn);
                        	pstmt.setString(2, _nama);
                        	pstmt.setString(3, _tglLahir);
                        	pstmt.setString(4, _JK);
                        	pstmt.executeUpdate();
                        	pstmt.close();

				//get directcode
				q = "Select no from member Where msisdn = ?";
                        	ps = dbConn.prepareStatement(q);
                        	ps.setString(1,msisdn);
                        	rs = ps.executeQuery();
                        	if(rs.next()){
                                	no = rs.getInt("no");
                        	}

                        	rs.close();
                        	ps.close();

				//cek availability
				aksesQ = "*807*" + no + "#";
				q = "Select direct_code from member Where direct_code = ?";
                                ps = dbConn.prepareStatement(q);
                                ps.setString(1,aksesQ);
                                rs = ps.executeQuery();
                                if(rs.next()){
                                        aksesQ = "*807*0" + no + "#";
                                }

                                rs.close();
                                ps.close();
				
				q = "UPDATE member SET direct_code = ? WHERE msisdn = ?";
                                pstmt = dbConn.prepareStatement(q);
                                pstmt.clearParameters();
                                pstmt.setString(1, aksesQ);
                                pstmt.setString(2, msisdn);
                                pstmt.executeUpdate();
                                pstmt.close();

				//set status default
				q = "INSERT INTO status VALUES('', ?,?,now())";
                                pstmt = dbConn.prepareStatement(q);
                                pstmt.clearParameters();
                                pstmt.setInt(1, no);
                                //pstmt.setString(2, "belum ada status");
                                pstmt.setString(2, "blm ada status-utk update status ketik status [isi_status] sms ke 1374");
                                pstmt.executeUpdate();
                                pstmt.close();
				
				ret = "Terimakasih Anda telah terdaftar dgn aksesQ : " + aksesQ + ", informasikan aksesQ Anda kpd tmn2, dan dapatkan Rp.1 Tcash setiap pelihat berbeda yg melihat aksesQ Anda.";
			}

                        closeDbConnection();
                }
                catch(Exception e)
                {
                        e.printStackTrace(System.out);
                        closeDbConnection();
                }

		return ret;
        }

	public String GetInfoABnumber(String msisdn, String bnumber)
        {
                String q = "";
                String ret = "-99";
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,msisdn);
                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
				ret = rs.getString("direct_code") + ";" + rs.getString("nama");
                        }

                        rs.close();
                        ps.close();

			q = "Select * from member Where direct_code = ?";
                        ps = dbConn.prepareStatement(q);
                        ps.setString(1,bnumber);
                        rs = ps.executeQuery();

                        if(rs.next()){
                                ret = ret + ";" + rs.getString("msisdn") + ";" + rs.getString("nama");
                        }

                        rs.close();
                        ps.close();

			closeDbConnection();

		} catch(Exception e) {
			e.printStackTrace(System.out);
			closeDbConnection();
		}

		return ret;
	}


	public String GetInfoMember(String dc)
        {
		String q = "";
                String ret = null;
                int no_member = 0;
                String nama = "";
		String isi_status = "";
		String predikat = "";
                boolean bo = false;
                try
                {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from member Where direct_code = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,dc);

                        ResultSet rs = ps.executeQuery();

                        if(rs.next()){
                                bo = true;
				no_member = rs.getInt("no");
				nama = rs.getString("nama");
				predikat = rs.getString("predikat");
                        }

                        rs.close();
                        ps.close();

			if (bo) {
				q = "Select * from status Where id_member = ? order by tgl_update desc";
                        	ps = dbConn.prepareStatement(q);
                        	ps.setInt(1,no_member);

				rs = ps.executeQuery();

				if(rs.next()) {
					isi_status = rs.getString("isi_status");
					ret = predikat + nama + ":\"" + isi_status + "\"" ;
				} else {
					//ret = predikat + nama + ": belum ada status" ;
					ret = predikat + nama + ": blm ada status-utk update status ketik status [isi_status] sms ke 1374" ;
				}

				rs.close();
                        	ps.close();

			}
			else {
				ret = "member belum terdaftar\n"
					+"Silahkan daftar ke *807# pilih 2.Registrasi";
			}

			closeDbConnection();
		}
		catch(Exception e) {
			e.printStackTrace();
			closeDbConnection();
		}

		return ret;
	}

        public String [] GetHome(String msisdn) {
                String [] ret = new String[6];
				int id_member = 0;
                String q = "";
                try{
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from member Where msisdn = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,msisdn);
                        rs = ps.executeQuery();

                        if(rs.next()) {
								id_member = rs.getInt("no");
                                ret[0] = rs.getString("direct_code");
                                ret[1] = rs.getString("nama");
                                ret[2] = rs.getString("tgl_lahir");
                                ret[3] = rs.getString("jenis_kelamin");
                                ret[4] = rs.getString("predikat");
                        }

                        rs.close();
                        ps.close();

			q = "Select * from status Where id_member = ? ORDER BY tgl_update DESC LIMIT 0,1";
                        ps = dbConn.prepareStatement(q);
                        ps.setInt(1,id_member);
                        rs = ps.executeQuery();

			if(rs.next()) {
				ret[5] = rs.getString("isi_status");
			}

			rs.close();
			ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                        closeDbConnection();
                }
		
		return ret;
	}

	public String [] GetProfil(String idMember) {
		String [] ret = new String[3];
		String q = "";
		try{
			if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

			q = "Select * from member Where direct_code = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1,idMember);
                        rs = ps.executeQuery();

                        if(rs.next()) {
				ret[0] = rs.getString("nama");
				ret[1] = rs.getString("tgl_lahir");
				ret[2] = rs.getString("jenis_kelamin");
			}

                        rs.close();
                        ps.close();	

			closeDbConnection();
		}
		catch(Exception e) {
			e.printStackTrace();
			closeDbConnection();
		}
		return ret;
	}

	public String [] GetComment(int id_status, int _pAwal) {
		String [] ret = new String[3];
		ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";

		String q = "";
		try {
			if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

			q = "Select * from komentar Where id_status = ? order by tgl_kirim desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setInt(1, id_status);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

			int i = 0;
                        while(rs.next()) {
				ret[i] = rs.getInt("no") + ":" + rs.getString("anumber") + ":" + rs.getString("nama_anumber") + ":" + rs.getString("isi_komentar") + ":" + rs.getString("tgl_kirim");
				i++;	
                        }

                        rs.close();
                        ps.close();

                        closeDbConnection();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public String [] GetFollower(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
				 String q1 = "";
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from follower Where anumber = ? order by tgl_update desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								    q1 = "Select * from member Where direct_code = ?";
									PreparedStatement ps1 = dbConn.prepareStatement(q1);
									ps1.setString(1, rs.getString("bnumber"));
									rs1 = ps1.executeQuery();
									String nama="";
									if(rs1.next()) {
										nama=rs1.getString("nama");
									}
									ret[i] = rs.getInt("no") + ":" + rs.getString("anumber") + ":" + rs.getString("bnumber") + ":" + nama; 
									
									rs1.close();
									ps1.close();
						
								i++;
                        }
						
						
						
                        rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
		
		public String [] GetFollowerStatus(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
			    String q1 = "";
				String q2 = "";
				String nama="-99";
				String status="-99";
				String idfollower="-99";
				String no="-99";
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from follower Where anumber = ? order by tgl_update desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								    q1 = "Select * from member Where direct_code = ?";
									PreparedStatement ps1 = dbConn.prepareStatement(q1);
									ps1.setString(1, rs.getString("bnumber"));
									rs1 = ps1.executeQuery();
									
									if(rs1.next()) {
										nama=rs1.getString("nama");
										idfollower=rs1.getString("direct_code");
										no=rs1.getString("no");
										
										q2 = "Select * from status Where id_member = ? order by tgl_update desc";
										PreparedStatement ps2 = dbConn.prepareStatement(q2);
										ps2.setInt(1, rs1.getInt("no"));
										rs2= ps2.executeQuery();
										if(rs2.next()) {
											status=rs2.getString("isi_status");
											
										}
											
									}
									
									
									ret[i] = no +":"+ idfollower +":"+ status; 
									
									rs1.close();
									ps1.close();
						
								i++;
                        }
						
						
						
                        rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
		
	public String [] GetChatHistory(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
			    String q1 = "";
				String q2 = "";
				String nama="-99";
				String status="-99";
				String idfollower="-99";
				String no="-99";
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from follower Where anumber = ? order by tgl_update desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								    q1 = "Select * from member Where direct_code = ?";
									PreparedStatement ps1 = dbConn.prepareStatement(q1);
									ps1.setString(1, rs.getString("bnumber"));
									rs1 = ps1.executeQuery();
									
									if(rs1.next()) {
										nama=rs1.getString("nama");
										idfollower=rs1.getString("direct_code");
										no=rs1.getString("no");
										
										q2 = "Select * from status Where id_member = ? order by tgl_update desc";
										PreparedStatement ps2 = dbConn.prepareStatement(q2);
										ps2.setInt(1, rs1.getInt("no"));
										rs2= ps2.executeQuery();
										if(rs2.next()) {
											status=rs2.getString("isi_status");
											
										}
											
									}
									
									
									ret[i] = no +":"+ idfollower +":"+ status; 
									
									rs1.close();
									ps1.close();
						
								i++;
                        }
						
						
						
                        rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
		
		public String [] GetInboxGroup(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
			    String q1 = "";
				String q2 = "";
				String nama="-99";
				String status="-99";
				String idfollower="-99";
				String no="-99";
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from follower Where anumber = ? order by tgl_update desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								    q1 = "Select * from member Where direct_code = ?";
									PreparedStatement ps1 = dbConn.prepareStatement(q1);
									ps1.setString(1, rs.getString("bnumber"));
									rs1 = ps1.executeQuery();
									
									if(rs1.next()) {
										nama=rs1.getString("nama");
										idfollower=rs1.getString("direct_code");
										no=rs1.getString("no");
										
										q2 = "Select * from status Where id_member = ? order by tgl_update desc";
										PreparedStatement ps2 = dbConn.prepareStatement(q2);
										ps2.setInt(1, rs1.getInt("no"));
										rs2= ps2.executeQuery();
										if(rs2.next()) {
											status=rs2.getString("isi_status");
											
										}
											
									}
									
									
									ret[i] = no +":"+ idfollower +":"+ status; 
									
									rs1.close();
									ps1.close();
						
								i++;
                        }
						
						
						
                        rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
	public String [] GetGroup(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
				 String q1 = "";
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from `group_member` Where direct_code = ? order by tgl_update desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								    q1 = "Select * from `group` Where no = ?";
									PreparedStatement ps1 = dbConn.prepareStatement(q1);
									ps1.setString(1, rs.getString("id_group"));
									rs1 = ps1.executeQuery();
									String nama="";
									String direct_code="";
									if(rs1.next()) {
										nama=rs1.getString("nama");
										direct_code=rs1.getString("direct_code");
									}
									ret[i] = rs1.getInt("no") + ":" + nama  + ":" + direct_code + ":" + rs.getString("tgl_update") ;
									
									rs1.close();
									ps1.close();
						
								i++;
                        }
						
						
						
                        rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }	
	public String [] GetSearch(String anumber, int _pAwal) {
                String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";


                String q = "";
				         
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from member Where nama like ? order by nama desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, "%"+anumber+"%");
                        ps.setInt(2, _pAwal);
                        rs = ps.executeQuery();

                        int i = 0;
                        while(rs.next()) {
								ret[i] = rs.getInt("no") + ":" + rs.getString("msisdn") + ":" + rs.getString("direct_code") + ":" + rs.getString("nama"); 
                                i++;
                        }
						 rs.close();
                        ps.close();

                        closeDbConnection();
                }
                catch(Exception e) {
                        e.printStackTrace();
                }
                return ret;
        }
	
	
	public int GetTopTopic(String id_member){
		int ret = 0;
		String q = "";
		int no = 0;
		try {
			if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

					q = "Select no from member Where direct_code = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, id_member);
                        rs = ps.executeQuery();

                        if(rs.next()) {
                                no = rs.getInt("no");
                        }

                        rs.close();
                        ps.close();

                        q = "Select * from status Where id_member = ? order by tgl_update desc";
                        ps = dbConn.prepareStatement(q);
                        ps.setInt(1, no);
                        rs = ps.executeQuery();

                        if(rs.next()) {
                        	ret = rs.getInt("no");
			}

                        rs.close();
                        ps.close();

                        closeDbConnection();
		}
		catch(Exception e){
			e.printStackTrace();
			closeDbConnection();
		}
		return ret;
	}
	public String [] GetTopTopicComment(String id_member, int _pAwal){
		String q = "";
		String q1="";
		int id_status = 0;
		String [] ret = new String[4];
                ret[0] = "-99";
                ret[1] = "-99";
                ret[2] = "-99";
				ret[3] = "-99";

				
		try {
			if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

					q = "Select count(*) as c , id_status from komentar Where bnumber = ? group by id_status order by c desc limit ?,3";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, id_member);
						ps.setInt(2, _pAwal);
                       
                        rs = ps.executeQuery();
						int i=0;
                        while(rs.next()) {
                                id_status = rs.getInt("id_status");
								q1 = "Select * from `status` where no = ?";
								PreparedStatement ps1 = dbConn.prepareStatement(q1);
								ps1.setInt(1, id_status);
								
								rs1 = ps1.executeQuery();
								//ret[1]=q1;
								if(rs1.next()){
									//ret[i]=Integer.toString(rs1.getInt("no"));
									ret[i] = rs1.getString("no") + ":" + rs1.getString("isi_status") + ":" + rs1.getString("tgl_update"); 
									
								}
								
								i++;
								rs1.close();
								ps1.close();
								
				        }

						//ret[0]=q;
                        rs.close();
                        ps.close();
					
                        closeDbConnection();
		}
		catch(Exception e){
			e.printStackTrace();
			closeDbConnection();
		}
		return ret;
	}
	
	public String GetTrxidChat(String anumber, String bnumber, String bnum_id, String bnum_name){
                String ret = "1000000000000000";
                String q = "";
		boolean b = false;
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDbConnection();

                        q = "Select * from queue_chat Where anumber = ? and bnumber = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setString(2, bnumber);
                        ResultSet rs = ps.executeQuery();

                        if(rs.next()) {
                                ret = rs.getString("trxid");
                        } else {
				b = true;
			}

                        rs.close();
                        ps.close();

			if(b) {
				q = "Insert into queue_chat VALUES ('','00',?,?,?,?,now(),'')";
                        	ps = dbConn.prepareStatement(q);
                        	ps.setString(1, anumber);
                        	ps.setString(2, bnumber);
                        	ps.setString(3, bnum_id);
                        	ps.setString(4, bnum_name);
                        	ps.executeUpdate();

				ps.close();
			
				String no = "";
				q = "Select * from queue_chat Where anumber = ? and bnumber = ?";
                        	ps = dbConn.prepareStatement(q);
                        	ps.setString(1, anumber);
                        	ps.setString(2, bnumber);
                        	rs = ps.executeQuery();
				if (rs.next()) {
					no = Integer.toString(rs.getInt("no"));
				}

				rs.close();
				ps.close();

				int len = no.length();
				String start = "1";

                                for (int i=0; i<(15-len) ;i++) {
                                        start = start + "0";
                                }

                                no = start + no;

				q = "UPDATE queue_chat set trxid = ? WHERE anumber = ? and bnumber = ?";
                                ps = dbConn.prepareStatement(q);
                                ps.setString(1, no);
                                ps.setString(2, anumber);
                                ps.setString(3, bnumber);
                                ps.executeUpdate();

                                ps.close();
				ret = no;

			}

                        closeDbConnection();
                }
                catch(Exception e){
                        e.printStackTrace();
			closeDbConnection();
                }
                return ret;
        }

}

