package cweeta.telkomsel.util;

import org.apache.log4j.Logger;

public class PushLog
{
        public static Logger logger = Logger.getLogger(PushLog.class);

        public static void record(String msg)
        {
                logger.info(msg);
        }
}

