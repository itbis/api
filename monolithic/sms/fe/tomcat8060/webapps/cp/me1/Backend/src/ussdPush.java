package src;

import javax.naming.*;
import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.net.*;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.protocol.Protocol;

public class ussdPush {
	
	public static String sendUssdPushMenu(String msisdn, String msg, String tipe, String mtype, String ip, int port, String trxid){
                String ret = null;
                Socket so = null;
                try {

                //InetSocketAddress isa = new InetSocketAddress("10.1.89.223", 50000);
                InetSocketAddress isa = new InetSocketAddress(ip, port);
                so = new Socket();
                so.setSoTimeout(5000);
                so.connect(isa, 5000);

                String push_msg = "p|"+tipe+"|"+msisdn+"|"+msg+"|"+mtype+"|"+trxid;

                BufferedReader br = new BufferedReader(new InputStreamReader(so.getInputStream()));
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(so.getOutputStream()));
                bw.write(push_msg+"\n");
                bw.flush();
                String push_resp = br.readLine();
                ret = push_resp;

                }catch(Exception e1){

                        e1.printStackTrace(System.out);

                } finally{
                        if(so != null) try { so.close(); } catch(Exception e2){}
                }

                return ret;


        }
	public static String httpXMLRequestPost(String url, String xmlData, int timeout) {		
		return httpXMLRequestPost(url, xmlData, timeout, null, null, null, 0, null);
	}
	
	public static String httpXMLRequestPost(String url, String xmlData, int timeout, String username, String password, String authHost, int authPort, String authRealm) {		
		long start = System.currentTimeMillis();
		String response = null;
		HttpClient http = new HttpClient();
		http.getParams().setSoTimeout(timeout);
		if (username != null && password != null) {
			http.getState().setCredentials(
		            new AuthScope(authHost, authPort, authRealm),
		            new UsernamePasswordCredentials(username, password)
		        );

		}
		PostMethod method = new PostMethod(url);
		method.setRequestBody(xmlData);
		method.getParams().setParameter("http.socket.timeout", timeout);
		method.getParams().setParameter("http.connection.timeout", timeout);
		try {
			int statusCode = http.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK && statusCode != 202) {
				response = "HTTP_FAILED|" + statusCode;
			} else {
				response = method.getResponseBodyAsString().trim();
				//response="HTTP_FAILED|";
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = "FAILED|" + e.getMessage();
		} finally {
			method.releaseConnection();
		}
		return response;
	}
		
}
