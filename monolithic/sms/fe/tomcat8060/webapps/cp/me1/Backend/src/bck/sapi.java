package src;

import java.io.*;
import java.net.*;
public class sapi{
	Socket requestSocket;
	ObjectOutputStream out;
 	ObjectInputStream in;
 	String message;
	sapi(){}
	void run()
	{
		try{
			//1. creating a socket to connect to the server
			requestSocket = new Socket("10.2.188.183", 10021);
			System.out.println("Connected to 10.2.188.183 in port 10021");
			//2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			//3: Communicating with the server
			String msg = "POST /vas_soap HTTP/1.0\r\n"+
			"Host: 10.2.188.183:10021\r\n"+
			"Authorization: Basic MjAwNDoxMjM0\r\n"+
			"User-Agent: MM7submit/1.0\r\n"+
			"Content-Type: multipart/related; boundary=\"--_=_NextPart_B1C_1C8_B8D.EEC\"; type=\"text/xml\";\r\n";
			do{
				try{
					sendMessage(msg);
					message = (String)in.readObject();
                                        System.out.println("server>" + message);
				}
				catch(ClassNotFoundException classNot){
					System.err.println("data received in unknown format");
				}
			}while(!message.equals("bye"));
		}
		catch(UnknownHostException unknownHost){
			System.err.println("You are trying to connect to an unknown host!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		finally{
			//4: Closing connection
			try{
				in.close();
				out.close();
				requestSocket.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}
	void sendMessage(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
			System.out.println("client>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public static void main(String args[])
	{
		sapi client = new sapi();
		client.run();
	}
}


