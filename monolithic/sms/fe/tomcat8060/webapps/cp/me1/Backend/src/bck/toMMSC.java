package tsel;

import java.io.*;
import java.util.Properties;
import org.apache.log4j.*;

import tsel.util.*;

public class toMMSC {

        static Logger log = Logger.getLogger(toMMSC.class.getName());

        public String getXml() {
		String result = getContent("/apps/aplikasi8090/FA/WEB-INF/classes/out.txt");
		String res = "Authorization: Basic MjAwNDoxMjM0\n" +
		"Content-Type: multipart/related; boundary=\"\"; type=\"text/xml\";" +
		"start=\"<mm7submit@localhost>\" \n"+
		"SOAPAction: \"\"\n" +
		"Content-Length: 12345\n" +

		"------=_Part_5328_16881106.1140390445743\n" +
		"Content-Type: application/xml\n" +
		"Content-Id: <mm7submit@localhost>\n" +

		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
		"<soapenv:Envelope xmlns:soapenv=http://schemas.xmlsoap.org/soap/envelope/xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchemainstance\">" +
		"<soapenv:Header>" +
		"<ns1:TransactionID soapenv:mustUnderstand=\"1\" xsi:type=\"xsd:string\" " +
		"xmlns:ns1=\"http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-2\">CE52B2D3EBB5E90A2B7E1A7B35BC0230</ns1:TransactionID>" +
		"</soapenv:Header>" +
		"<soapenv:Body>" +
		"<SubmitReq xmlns=\"http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-2\">" +
		"<MM7Version>5.3.0</MM7Version>" +
		"<SenderIdentification>" +
		"<VASPID>2004</VASPID>" +
		"<SenderAddress>" +
		"<ShortCode displayOnly=\"false\">2004</ShortCode>" +
		"</SenderAddress>" +
		"</SenderIdentification>" +
		"<Recipients>" +
		"<To>" +
		"<Number displayOnly=\"false\">62811916441</Number>" +
		"</To>" +
		"</Recipients>" +
		"<Subject>MMS umlaut text and image</Subject>" +
		"<Content allowAdaptations=\"true\" href=\"cid:1404FA19F5DE7F7F817027646EC9CB77\"/>" +
		"</SubmitReq>" +
		"</soapenv:Body>" +
		"</soapenv:Envelope>\n" +
		"------=_Part_5328_16881106.1140390445743\n" +
		"Content-Type: multipart/mixed;\n" +
		"boundary=\"----=_Part_5327_5454214.1140390445695\"\n " +
		"Content-Transfer-Encoding: binary\n" +
		"Content-Id: <1404FA19F5DE7F7F817027646EC9CB77>\n\n" +

		"------=_Part_5327_5454214.1140390445695\n"+
		"Content-Type: text/plain; name=\"message.txt\"; charset=iso-8859-1\n" +
		"Content-Transfer-Encoding: 8bit\n"+
		"Content-ID: <message.txt>\n"+
		"Content-Location: message.txt\n\n"+

		"Please smile\n\n"+

		"------=_Part_5327_5454214.1140390445695\n"+
		"Content-Type: image/jpeg; name=\"smiley.jpg\"\n "+
		"Content-Transfer-Encoding: base64" +
		"Content-ID: <smiley.jpg>"+
		"Content-Location: smiley.jpg\n\n"+
		result + "\n\n" +

		"------=_Part_5327_5454214.1140390445695--\n\n"+

		"------=_Part_5328_16881106.1140390445743---";

		return res;
        }

	public String getContent(String malio) {
		File file = new File(malio);
        	StringBuffer contents = new StringBuffer();
        	BufferedReader reader = null;
 
        	try {
            		reader = new BufferedReader(new FileReader(file));
            		String text = null;
 
            		// repeat until all lines is read
            		while ((text = reader.readLine()) != null) {
                	contents.append(text)
                    	.append(System.getProperty(
                        	"line.separator"));
            		}
        	} catch (FileNotFoundException e) {
            		e.printStackTrace();
        	} catch (IOException e) {
            		e.printStackTrace();
        	} finally {
            		try {
                		if (reader != null) {
                    			reader.close();
                		}
            		} catch (IOException e) {
                		e.printStackTrace();
            		}
		
        	}
         
        	// show file contents here
        	//System.out.println(contents.toString());
		return contents.toString();
	}

        public String []  submitToMMSC(String s){
		String url = "http://10.2.188.183:10021/vas_soap";
		//String url = "http://10.251.99.99:2000/vas_soap";
		//HttpGet.SSLRegister(8443);

                String ret [] = HttpGet.postDataMMSC(url, 10000, s);
                //ret[3] = url;
                if(ret[0].equals("OK")) {
                }else
                {
                        log.error("error_submit s:"+s+" err:"+ret[1]);
                }
                return ret;
        }

}

