package src;

import java.net.*;
import java.io.*;

public class kacrut {

  public static void main(String[] args) {
		
    try {
	String xmldata = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
	"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-2\">\r\n\t" +
	"<SOAP-ENV:Header>\r\n\t\t" +
	"<TransactionID SOAP-ENV:mustUnderstand=\"1\">vas00001</TransactionID>\r\n\t" +
	"</SOAP-ENV:Header>\r\n\t" +
	"<SOAP-ENV:Body>\r\n\t" +
	"<SubmitReq>\r\n\t\t\t" +
	"<MM7Version>5.3.0</MM7Version>\r\n\t\t\t" +
	"<SenderIdentification>\r\n\t\t\t\t" +
	"<VASPID>2004</VASPID>\r\n\t\t\t\t" +
	"<VASID>2004</VASID>\r\n\t\t\t\t" +
	"<SenderAddress><ShortCode>2004</ShortCode></SenderAddress>\r\n\t\t\t" +
	"</SenderIdentification>\r\n\t\t\t" +
	"<Recipients>\r\n\t\t\t\t" +
	"<To><Number>62811916441</Number></To>\r\n\t\t\t" +
	"</Recipients>\r\n\t\t\t" +
	"<MessageClass>Informational</MessageClass>\r\n\t\t\t" +
	"<DeliveryReport>false</DeliveryReport>\r\n\t\t\t" +
	"<ReadReply>false</ReadReply>\r\n\t\t\t" +
	"<Priority>Normal</Priority>\r\n\t\t\t" +
	"<ChargedParty>Sender</ChargedParty>\r\n\t\t\t" +
	"<Content href=\"cid:content@localhost\" allowAdaptations=\"true\"/>\r\n\t\t" +
	"</SubmitReq>\r\n\t" +
	"</SOAP-ENV:Body>\r\n" +
	"</SOAP-ENV:Envelope>\r\n" +
	"\r\n" + 
        "----_=_NextPart_21B2_1EE0_E97.2593\r\n"+
        "Content-Type: multipart/mixed; boundary=\"----_=_NextPart_220A_232_2517.E38\"\r\n\r\n"+
	"Content-ID: <content@localhost>\r\n\r\n"+
	"------_=_NextPart_220A_232_2517.E38\r\n"+
	"Content-Type: text/plain; charset=\"us-ascii\"; Name=text0.txt\r\n"+
	"Content-ID: <text0.txt>\r\n"+
	"Content-Location: text0.txt\r\n\r\n"+
	"test bobby\r\n\r\n"+
	"------_=_NextPart_220A_232_2517.E38--\r\n"+
	"----_=_NextPart_21B2_1EE0_E97.2593--\r\n";

      //Create socket
      String hostname = "10.2.188.183";
      int port = 10021;
      InetAddress  addr = InetAddress.getByName(hostname);
      Socket sock = new Socket(addr, port);
			
      //Send header
      String path = "/vas_soap";
      //BufferedWriter  wr = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(),"UTF-8"));
      BufferedWriter  wr = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
      // You can use "UTF8" for compatibility with the Microsoft virtual machine.
      wr.write("POST " + path + " HTTP/1.0\r\n");
      wr.write("Host: 10.2.188.183:10021\r\n");
      wr.write("Authorization: Basic MjAwNDoxMjM0\r\n");
      wr.write("User-Agent: MM7submit/1.0\r\n");
      wr.write("Content-Type: multipart/related; boundary=\"--_=_NextPart_21B2_1EE0_E97.2593\"; type=\"text/xml\";\r\n");
      wr.write("\t");
      wr.write("start=\"<mm7submit@localhost>\"\r\n");	
      wr.write("Content-Length: " + xmldata.length() + "\r\n"); 
      wr.write("SOAPAction: \"\"\r\n");	 
      wr.write("Accept: */*\r\n\r\n");	 
      wr.write("----_=_NextPart_21B2_1EE0_E97.2593\r\n");
      wr.write("Content-Type: application/xml\r\n");
      wr.write("Content-ID: <mm7submit@localhost>\r\n");
      wr.write("\r\n");	 	
			
      //Send data
      wr.write(xmldata);

      /*wr.write("----_=_NextPart_21B2_1EE0_E97.2593\r\n");	
      wr.write("Content-Type: multipart/mixed; boundary=\"----_=_NextPart_220A_232_2517.E38\"\r\n");
      wr.write("Content-ID: <content@localhost>\r\n");	
      wr.write("\r\n");
      wr.write("------_=_NextPart_220A_232_2517.E38\r\n");
      wr.write("Content-Type: text/plain; charset=\"us-ascii\"; Name=text0.txt\r\n");	
      wr.write("Content-ID: <text0.txt>\r\n");
      wr.write("Content-Location: text0.txt\r\n");
      wr.write("test bobby\r\n");
      wr.write("------_=_NextPart_220A_232_2517.E38--\r\n");
      wr.write("----_=_NextPart_21B2_1EE0_E97.2593--\r\n");
      */
      wr.flush();
      // Response
      BufferedReader rd = new BufferedReader(new InputStreamReader(sock.getInputStream()));
      String line;
      while((line = rd.readLine()) != null)
	System.out.println(line);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

