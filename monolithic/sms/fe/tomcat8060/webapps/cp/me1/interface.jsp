<%
	//basic parameter
	String _msg = null; //message that will be send as output for USSD
        String u = null; //output
        String _err1 = "char more than 130 (Err:01)";
        String _err2 = "Under maintenance (Err:02)";
        String _err3 = "Wrong Input (Err:03)\n0.Menu Utama";
        String _underCons = "UnderConstruction";
        String _adn = "807";
        String _getSess = null;
	int _topTopic = 0;
	int _nPage = 0;
	String trxid = "0000000000000000";

        //smsgw
        //http://10.2.224.148:5054","/cgi-bin/sendsms?user=tester&pass=foobar&from=Telkomsel&to="+msisdn+"&text="+msg
        String _url_smsgw = "http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=cweeta&";
        String _url_smsgw_1374 = "http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=1374&";

        String _smsCaraP = "1.Pengguna hrs melakukan pendaftaran utk menggunakan layanan ini akses *807# pilih 2.Registrasi\n" 
                        + "2.Anda akan mndptkan CweetaID, infokan cweeta ID Anda ke tmn2, dan jadikan Anda terkenal....\n";
        _smsCaraP = URLEncoder.encode(_smsCaraP, "UTF-8");
        String _biaya = "Saat ini bebas biaya";
        _biaya = URLEncoder.encode(_biaya, "UTF-8");
        String _predicate = "Setiap pengguna akan memiliki predikat yg disimbolkan dgn tanda * , berdasarkan jumlah follower nya:\n"
                + "* utk  newbie,0-100 followers\n"
                + "** utk  startup,101-500 followers\n"
                + "*** utk advance,501-1500 followers\n"
                + "**** utk experince,1501-5000 followers\n"
                + "***** utk Top,5001-.. followers";
        _predicate = URLEncoder.encode(_predicate, "UTF-8");
        String _advantage = "(belum berlaku fitur ini)--Setiap akses oleh pengguna lain yg mengakses profil Anda ( 1 pengguna dlm 1 mgg akan dihitung sebagai 1x) , maka Anda akan mendapatkan Rp.1 per 1x hitungan pelihat";
        _advantage = URLEncoder.encode(_advantage, "UTF-8");
        String _caraDaftar = "Ketik Daftar[spasi]{Nama}[pagar]{Tgl Lahir}[pagar][Jenis Kelamin (L/P)],\n"
                                + "Contoh : Daftar Rian Rahardian#20-10-1973#L\n"
                                + "Kirim ke 1374";
        _caraDaftar = URLEncoder.encode(_caraDaftar, "UTF-8");
        String _respThanks = "Terimakasih permintaan Anda sedang Kami Proses";
        int timeout = 5000;

	//menu define
        //default menu
        String _thanks = "Terimakasih permintaan Anda sedang kami proses\n"
                        + "9.Menu Sebelumnya\n"
                        + "0.Menu Utama";

        String _mainMenu = "--Cweeta--\n"
                                + "(Cuit-Cuit bareng Telkomsel)\n"
                                + "1.Tentang Cweeta\n"
                                + "2.Registrasi\n"
                                + "3.Syarat&Ketentuan\n"
                                + "4.Peluru\n5.Tcash";

        //about cweeta
        String _aboutCweeta = "--Tentang Cweeta-- \n"
                                + "Media Sosial khusus pelanggan Telkomsel menggunakan UMB dan SMS\n"
                                + "1.Cara Penggunaan\n"
                                + "0.Menu Utama";

        //registration
        String _registration = "--Registrasi--\n"
                                + "1. Via UMB\n"
                                + "2. Via SMS\n"
                                + "0.Menu Utama";

        //term & condition
        String _term = "--Syarat&Ketentuan--\n"
                        + "1.Biaya\n"
                        + "2.Keuntungan\n"
                        + "3.Predikat\n"
                        + "0.Menu Utama";

        //bullets
        String _bullet = "--Peluru--\n"
                        + "(belum berlaku)--Alat tukar utk mlakukn aktivitas sprti update status,kirim pesan,dst.1 aktivitas akan mngurangi 1 peluru\n"
                        + "0.Menu Utama";

        //tcash
        String _tcash = "--TCash Cweeta--\n"
                        + "Uang eletronik yg dpt dibelanjakn, dicairkn di merchat Tcash terdekat,& saldo Tcash dpt di Transfer.\n"
                        + "0.Menu Utama";

	//step 1
        String _step1 = "--Step 1--\n"
                        + "Masukkan Nama ?\n"
			+ "Maksimum 20 karakter\n"
                        + "9.Menu Sebelumnya\n"
                        + "0.Menu Utama";

        //step 1
        String _step2 = "--Step 2--\n"
                        + "Tgl Lahir ?\n"
                        + "Contoh:15-09-1983\n"
                        + "9.Menu Sebelumnya\n"
                        + "0.Menu Utama";

        //step 1
        String _step3 = "--Step 3--\n"
                        + "Jenis Kelamin(L/P) ?\n"
                        + "9.Menu Sebelumnya\n"
                        + "0.Menu Utama";

        //end Registration
        String _endReg = "00" + "Terimakasih permintaan Anda sedang Kami proses, silahkan tunggu sms konfirmasi utk informasi ID Anda.";

	//send comment end via umb
        String _endComment = "Terimakasih, komentar Anda telah dikirim\n"
			+ "9.kembali\n"
			+ "0.Menu Utama";

	//view member
	String _vMember = "1.Komen\n"
			+ "2.Profil\n"
			+ "3.Chat\n";

	//send message
	String _sMsg = "Masukkan pesan(waktu isi maks 1 menit, maksimal karakter 100)"
			+ "0.Menu Utama";

	//reject coz not member
	String _nMember = "00" + "Anda belum menjadi member, tidak bisa melakukan aktivitas ini, segera daftar ke *807# OK/Call";

	//message sent.
	String _sent = "00" + "Pesan telah dikirim kepada";

	//ussdpush
        String _ipPush = "10.1.89.223";
        int _portPush = 50005;

	//info underconst+msg
	String _ucMsg = "00" + "UnderConst(05)\n"
			+ "Utk update status ketik status(spasi){isi status} SMS ke 1374, cth: status cweetaQ nich..\n";
%>
