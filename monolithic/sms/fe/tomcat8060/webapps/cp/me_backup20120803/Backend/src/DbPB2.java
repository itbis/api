package src;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class DbPB2
{
	public static Logger logger = Logger.getLogger(DbPB2.class);

	PropsReader Cfg;
	DataSource dataSource;
    	Connection dbConn;
    	Statement stmt;
    	ResultSet rs;
	String query;
	PreparedStatement pstmt;

	public DbPB2(PropsReader Cfg, DataSource dataSource)
    	{
		this.Cfg = Cfg;
		this.dataSource = dataSource;
	}

    	public Connection createDBConnection() throws IOException, SQLException
    	{
		if(dataSource != null)
		{
			dbConn = dataSource.getConnection();
		}
		else
			logger.fatal("Fails to create DB connection --> DataSource null");

		return dbConn;
    	}

    	public void closeDBConnection()
    	{
        	try
        	{
            		if(dbConn != null)
            		{
				dbConn.close();
				dbConn = null;
            		}
        	}
        	catch(Exception e)
        	{
            		logger.fatal("Exception on close DB connection: " + e.getMessage(), e);
        	}
    	}

	public ResultSet doQuery(String s) throws IOException, SQLException
    	{
        	if(dbConn == null || dbConn.isClosed())
                	createDBConnection();

        	//System.out.println("CP DB Connection Insert : "+dbConn);
        	stmt = dbConn.createStatement();
        	rs = stmt.executeQuery(s);

        	return rs;
    	}

    	public void doUpdate(String s) throws IOException, SQLException
    	{
        	if(dbConn == null || dbConn.isClosed())
                	createDBConnection();

        	//System.out.println("CP DB Connection Update : "+dbConn);
        	stmt = dbConn.createStatement();
        	stmt.executeUpdate(s);
        	stmt.close();
    	}

	public String GetInfoQueueChat(String trxid) {
		String ret = "00";
		String q = "";
		String anum = "62811";
		String bnum = "62811";
		String bnum_id = "00";
		String bnum_name = "00";
		try {
			if(dbConn == null || dbConn.isClosed())
                                createDBConnection();
			q = "SELECT * FROM queue_chat WHERE trxid = ?";
			PreparedStatement ps = dbConn.prepareStatement(q);
			ps.setString(1,trxid);
			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				anum = rs.getString("anumber");
				bnum = rs.getString("bnumber");
				bnum_id = rs.getString("bnum_id");
				bnum_name = rs.getString("bnum_name");
			}

			rs.close();
			ps.close();

			q = "SELECT * FROM member WHERE msisdn = ?";
                        ps = dbConn.prepareStatement(q);
                        ps.setString(1,anum);
                        rs = ps.executeQuery();

			if(rs.next()) {
				ret = anum + ";" + bnum + ";" + bnum_id + ";" + bnum_name + ";" + rs.getString("direct_code") + ";" 
					+ rs.getString("nama");
			}
		
			rs.close();
			ps.close();
			closeDBConnection();
		}
		catch(Exception e) {
			e.printStackTrace();
			closeDBConnection();
		}
		return ret;
	}

	public String GetTrxidChat(String anumber, String bnumber, String bnum_id, String bnum_name){
                String ret = "1000000000000000";
                String q = "";
                boolean b = false;
                try {
                        if(dbConn == null || dbConn.isClosed())
                                createDBConnection();

                        q = "Select * from queue_chat Where anumber = ? and bnumber = ?";
                        PreparedStatement ps = dbConn.prepareStatement(q);
                        ps.setString(1, anumber);
                        ps.setString(2, bnumber);
                        ResultSet rs = ps.executeQuery();

                        if(rs.next()) {
                                ret = rs.getString("trxid");
                        } else {
                                b = true;
                        }

                        rs.close();
                        ps.close();

                        if(b) {
                                q = "Insert into queue_chat VALUES ('','00',?,?,?,?,now())";
                                ps = dbConn.prepareStatement(q);
                                ps.setString(1, anumber);
                                ps.setString(2, bnumber);
                                ps.setString(3, bnum_id);
                                ps.setString(4, bnum_name);
                                ps.executeUpdate();

                                ps.close();

                                String no = "";
                                q = "Select * from queue_chat Where anumber = ? and bnumber = ?";
                                ps = dbConn.prepareStatement(q);
                                ps.setString(1, anumber);
                                ps.setString(2, bnumber);
                                rs = ps.executeQuery();
                                if (rs.next()) {
                                        no = Integer.toString(rs.getInt("no"));
                                }

                                rs.close();
                                ps.close();

                                int len = no.length();
				String start = "1";

                                for (int i=0; i<(15-len) ;i++) {
                                        start = start + "0";
                                }

				no = start + no;

                                q = "UPDATE queue_chat set trxid = ? WHERE anumber = ? and bnumber = ?";
                                ps = dbConn.prepareStatement(q);
                                ps.setString(1, no);
                                ps.setString(2, anumber);
                                ps.setString(3, bnumber);
                                ps.executeUpdate();

                                ps.close();
                                ret = no;

                        }

                        closeDBConnection();
                }
                catch(Exception e){
                        e.printStackTrace();
                        closeDBConnection();
                }
                return ret;
        }


}
