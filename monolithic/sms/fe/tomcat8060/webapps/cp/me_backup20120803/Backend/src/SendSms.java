package src;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.Queue;
import java.sql.*;

public class SendSms extends Thread {
   LinkedList<String> list;
   String sms;
   String cp_name ;
   String pwd ;
   String sid ;
   String url; 
   String dlr_cp;
   HttpGet ht;
   String result;
   int MaxThread;
   int timeout;
   int besar;
   DbCB2 DbC;	
   DbPB2 DB;	
   DataSource Ds;
   String ip_push;
   String port_push;
	
   static Logger logger = Logger.getLogger(SendSms.class.getName());

   public SendSms(String file, LinkedList<String> list, DbPB2 DB) throws Exception {
        PropertyConfigurator.configure(file);
	PropsReader Props = new PropsReader(file);
        this.DB = DB;
     	this.list = list;
	this.ip_push = Props.readString("ip_push");
	this.port_push = Props.readString("port_push");
	try { sms = URLEncoder.encode(sms,"UTF-8");} catch (Exception e){}
    	for (int i = 0; i < 10; i++) {
		Thread t = new Thread(this);
		t.start();
	}
	
   }	

   public void run() {
     for(;;) { 
	try { Thread.sleep(1000); } catch(InterruptedException e) {}
	String req = "";
	
	synchronized(list) 
	{
		while(list.isEmpty()) {
			try {
				list.wait();
			}
			catch (InterruptedException e) {}
		}	
		req = list.removeFirst();
		besar = list.size();
	}

	String trxid = req.split(";")[0];
	String msg = req.split(";")[1];
	log.record("queue:" + besar+ "|" + req);

	try {
		if (!req.equals("")) {
			//get info bnumber
			String ret = DB.GetInfoQueueChat(trxid);
			String no_tujuan = ret.split(";")[0];
			String no_pengirim = ret.split(";")[1];
			String bnum_id = ret.split(";")[2];
			String bnum_name = ret.split(";")[3];
			String anum_id = ret.split(";")[4];
			String anum_name = ret.split(";")[5];
			//chat message
			msg = bnum_name + "(" + bnum_id + "):\"" + msg + "\""; 
			//get trxidchat
			String _trxid = DB.GetTrxidChat(no_pengirim,no_tujuan,anum_id,anum_name);
                        //send ussd push
			String push = "NOT OK";
			if (!no_tujuan.equals("") && !no_pengirim.equals("") && !bnum_id.equals("") && !bnum_name.equals("") && !_trxid.equals("")) {
				ussdPush.sendUssdPushMenu(no_tujuan, msg, "2", "2", ip_push, Integer.parseInt(port_push), _trxid);
				push = "OK";
			}
			log.record("cweeta ussdpush:" + no_tujuan + "|" + no_pengirim + "|" + bnum_id + "|" + bnum_name + "|" + _trxid + "|" + push + "|" + ip_push + ":" + port_push);
      		}
	}
	catch (Exception e) {
		log.record(e.getMessage());
		synchronized (this) {DB.closeDBConnection();}
	}
      }	
   }
}

