package src;

import java.net.*;
import java.io.*;


public class soaptest {

  //public final static String DEFAULT_SERVER = "http://www.elharo.com/fibonacci/SOAP";
  public final static String DEFAULT_SERVER = "http://10.2.188.183:10021/vas_soap";
  //public final static String SOAP_ACTION = "http://www.example.com/fibonacci";
  public final static String SOAP_ACTION = "";

  public static void main(String[] args) {
  
    String server = DEFAULT_SERVER;
    
    try {
      URL u = new URL(server);
      URLConnection uc = u.openConnection();
      HttpURLConnection connection = (HttpURLConnection) uc;
      
      connection.setDoOutput(true);
      connection.setDoInput(true);
      connection.setRequestMethod("POST");
      connection.setRequestProperty("Host", "10.2.188.183");
      connection.setRequestProperty("Authorization", "Basic MjAwNDoxMjM0");
      connection.setRequestProperty("Content-Type", "multipart/related; boundary=\"----_=_NextPart_BD0_B70_5DF.14B2\"; type=\"text/xml\";start=\"<mm7submit@localhost>\"");
      connection.setRequestProperty("Content-Length", "1534");
      connection.setRequestProperty("SOAPAction", "\"\"");
      connection.setRequestProperty("Accept", "*/*");
      
      OutputStream out = connection.getOutputStream();
      Writer wout = new OutputStreamWriter(out);
      
      System.out.print("----_=_NextPart_BD0_B70_5DF.14B2\r\n");
      System.out.print("Content-Type: application/xml\r\n");
      System.out.print("Content-ID: <mm7submit@localhost>\r\n");
      System.out.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
      System.out.print("<SOAP-ENV:Envelope ");
      System.out.print("xmlns:SOAP-ENV=");    
      System.out.print("\"http://schemas.xmlsoap.org/soap/envelope/\" " );
      System.out.print("xmlns=\"http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-2\">");
      System.out.print("<SOAP-ENV:Header>");
      		System.out.print("<TransactionID SOAP-ENV:mustUnderstand=\"1\">vas00001</TransactionID>");	
      System.out.print("</SOAP-ENV:Header>");
      System.out.print("<SOAP-ENV:Body>");
      		System.out.print("<SubmitReq>");
      			System.out.print("<MM7Version>5.3.0</MM7Version>");
      			System.out.print("<SenderIdentification>");
      				System.out.print("<VASPID>2004</VASPID>");
      				System.out.print("<VASID>2004</VASID>");
      				System.out.print("<SenderAddress><ShortCode>2004</ShortCode></SenderAddress>");
      			System.out.print("</SenderIdentification>");
      			System.out.print("<Recipients>");
      				System.out.print("<To><Number>62811916441</Number></To>");
      			System.out.print("</Recipients>");
      			System.out.print("<MessageClass>Informational</MessageClass>");
      			System.out.print("<TimeStamp>2011-05-13T20:28:01+07:00</TimeStamp>");
      			System.out.print("<DeliveryReport>false</DeliveryReport>");
      			System.out.print("<ReadReply>false</ReadReply>");
      			System.out.print("<Priority>Normal</Priority>");
      			System.out.print("<Subject>test</Subject>");
      			System.out.print("<ChargedParty>Sender</ChargedParty>");
      			System.out.print("<Content href=\"cid:content@localhost\" allowAdaptations=\"true\"/>");
      		System.out.print("</SubmitReq>");
      System.out.print("</SOAP-ENV:Body>");
      System.out.print("</SOAP-ENV:Envelope>\r\n");
      
      System.out.print("----_=_NextPart_BD0_B70_5DF.14B2");
      System.out.print("Content-Type: multipart/mixed; boundary=\"----_=_NextPart_8B8_16D5_E32.2237\"\r\n ");
      System.out.print("Content-ID: <content@localhost>\r\n");	
      System.out.print("------_=_NextPart_8B8_16D5_E32.2237\r\n");
      System.out.print("Content-Type: text/plain; charset=\"us-ascii\"; Name=text0.txt\r\n");
      System.out.print("Content-ID: <text0.txt>\r\n");
      System.out.print("Content-Location: text0.txt\r\n");
      System.out.print("tetra lia keysha\r\n");
      System.out.print("------_=_NextPart_8B8_16D5_E32.2237--\r\n");
      System.out.print("----_=_NextPart_BD0_B70_5DF.14B2--");

      wout.flush();
      wout.close();
      
      InputStream in = connection.getInputStream();
      int c;
      while ((c = in.read()) != -1) System.out.write(c);
      in.close();

    }
    catch (IOException e) {
      System.err.println(e); 
    }
  
  } // end main

}
