<%
	//back to main menu
	if (msg.equals("0")) {
		_msg = _mainMenu;
		Ses.CreateSubscriberMenuSession(msisdn, 0, 0, 0, 0, 0, 0, 0, "DM" );
		PushLog.record("CreateSubscriberMenuSession " + msisdn + ", 0, 0, 0, 0, 0, 0, 0, DM") ;
	} else {
		int rootid=0, cumid=0, cmid=0, getlcumcode=0, lcumcode=0, input=0, corres=0;
		String info;
		String _MoNa = _MoN + ":" + msg ;

		//check depth of menu
		int _depthM = Func.CheckDepthMenu(_getSess);
		int _Corres = Func.GetCorres(_getSess);
                int _getLcc = Func.GetLcc(_getSess);

		if(_Corres != 0 && _Corres != 21 && _Corres != 211 && _Corres != 2111 && _Corres != 21111) {
			if(!msg.equals("9") && !msg.equals("0") ) _depthM = 0;
		}

		//command == 9 / back
		if(msg.equals("9")) {
                        //corres == 11
                        if (_Corres == 11) {
                                _depthM = 0;
                                msg = "1";
                        } else if (_Corres == 22) {
				_depthM = 0;
				msg = "2";
			} else if (_Corres == 31 || _Corres == 32 || _Corres == 33) {
				_depthM = 0;
                                msg = "3";
			} else if (_Corres == 21) {
				_depthM = 0;
                                msg = "2";
			}
			else if (_Corres == 211) {
				_depthM = 1;
				_getLcc = 2;
                                msg = "1";
                        }
			else if (_Corres == 2111) {
				String [] _parse = Func.parseState(_MoN);
				_MoNa = "";
				for (int i=0; i < _parse.length-1; i++) {
					if (i != _parse.length-2)
						_MoNa = _MoNa + _parse[i] + ":" ;
					else
						_MoNa = _MoNa + _parse[i] ;
				}
				_depthM = -99;
				%><%@include file="step1.jsp" %><%
			}
			else if (_Corres == 21111) {
				String [] _parse = Func.parseState(_MoN);
                                _MoNa = "";
                                for (int i=0; i < _parse.length-1; i++) {
                                        if (i != _parse.length-2)
                                                _MoNa = _MoNa + _parse[i] + ":" ;
                                        else
                                                _MoNa = _MoNa + _parse[i] ;
                                }
                                _depthM = -99;
				%><%@include file="step2.jsp" %><%
			}

                }

		if (_depthM == 0) {
			//if current state root menu
			try {
				int _choose = Integer.parseInt(msg);

				switch(_choose){ 

					case 1 : _msg = _aboutCweeta;
						rootid = 1; cumid = 0; cmid = 1; getlcumcode = 0; lcumcode = 1; input = _choose; corres = 0;
						break;		
					case 2 :
						_msg = _registration;
						rootid = 2; cumid = 0; cmid = 1; getlcumcode = 0; lcumcode = 2; input = _choose; corres = 0;
						break;
					case 3 :
						_msg = _term;
						rootid = 3; cumid = 0; cmid = 1; getlcumcode = 0; lcumcode = 3; input = _choose; corres = 0;
						break;
					case 4 :
						_msg = _bullet;
						rootid = 4; cumid = 0; cmid = 1; getlcumcode = 0; lcumcode = 4; input = _choose; corres = 0;
						break;
					case 5 :
						_msg = _tcash;
						rootid = 5; cumid = 0; cmid = 1; getlcumcode = 0; lcumcode = 5; input = _choose; corres = 0;
						break;
					default:
                				_msg = _mainMenu;
						break;
				}
			} catch(Exception ex) {
				ex.printStackTrace();
				_msg = _mainMenu;
			}

			Ses.UpdateSubscriberMenuSession(msisdn, rootid, cumid, cmid, getlcumcode, lcumcode, input, corres, "DM");
                	PushLog.record("UpdateSubscriberMenuSession " + msisdn + "," + rootid + "," + cumid + "," +cmid+ "," +getlcumcode+ "," +lcumcode+ "," +input+ "," +corres+", DM|" + _depthM) ;
		
		} else if (_depthM == 1) {
			//if current state with depth 1
			if ( _getLcc == 4 || _getLcc == 5) {
				_msg = _mainMenu;
				Ses.UpdateSubscriberMenuSession(msisdn, 0, 0, 0, 0, 0, 0, 0, "DM" );
                                PushLog.record("UpdateSubscriberMenuSession " + msisdn + ", 0, 0, 0, 0, 0, 0, 0, DM |" + _depthM + "|" + _getLcc) ;
			} 
			else {
				//if choose about cweeta
				if (_getLcc == 1) {
					try {
						int _choose = Integer.parseInt(msg);

						switch(_choose) {
							case 1 : _msg = _thanks;
								_url_smsgw = _url_smsgw + "to=" + msisdn + "&text=" + _smsCaraP ;
                                        			String hit = HTTP.get(_url_smsgw, 0 , timeout);
								rootid = 1; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 1; input = _choose; corres = 11;
								PushLog.record(_url_smsgw + "|" + hit );
								break;	
							default : _msg = _mainMenu;
								rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
								break;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						_msg = _mainMenu;
						rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
					}
				}
				//if choose registration
				else if (_getLcc == 2 ) {
					try {
                                                int _choose = Integer.parseInt(msg);

                                                switch(_choose) {
							case 1 : _msg = _step1;
                                                                rootid = 2; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 1; input = _choose; corres = 21;
                                                                break;
                                                        case 2 : _msg = _thanks;
                                                                _url_smsgw = _url_smsgw + "to=" + msisdn + "&text=" + _caraDaftar ;
                                                                String hit = HTTP.get(_url_smsgw, 0 , timeout);
                                                                rootid = 2; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 2; input = _choose; corres = 22;
                                                                PushLog.record(_url_smsgw + "|" + hit );
                                                                break;
                                                        default : _msg = _mainMenu;
                                                                rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
                                                                break;
                                                }
                                        } catch (Exception ex) {
                                                ex.printStackTrace();
                                                _msg = _mainMenu;
                                                rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
                                        }

				}
				// if choose term & condition
				else if(_getLcc == 3) {
					try {
                                                int _choose = Integer.parseInt(msg);
						String hit = null;
                                                switch(_choose) {
                                                        case 1 : _msg = _thanks;
                                                                _url_smsgw = _url_smsgw + "to=" + msisdn + "&text=" + _biaya ;
                                                                hit = HTTP.get(_url_smsgw, 0 , timeout);
                                                                rootid = 3; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 1; input = _choose; corres = 31;
                                                                PushLog.record(_url_smsgw + "|" + hit );
                                                                break;
							case 2 : _msg = _thanks;
                                                                _url_smsgw = _url_smsgw + "to=" + msisdn + "&text=" + _advantage ;
                                                                hit = HTTP.get(_url_smsgw, 0 , timeout);
                                                                rootid = 3; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 2; input = _choose; corres = 32;
                                                                PushLog.record(_url_smsgw + "|" + hit );
                                                                break;
							case 3 : _msg = _thanks;
                                                                _url_smsgw = _url_smsgw + "to=" + msisdn + "&text=" + _predicate ;
                                                                hit = HTTP.get(_url_smsgw, 0 , timeout);
                                                                rootid = 3; cumid = 1; cmid = 2; getlcumcode = 1; lcumcode = 3; input = _choose; corres = 33;
                                                                PushLog.record(_url_smsgw + "|" + hit );
                                                                break;
                                                        default : _msg = _mainMenu;
                                                                rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
                                                                break;
                                                }
                                        } catch (Exception ex) {
                                                ex.printStackTrace();
                                                _msg = _mainMenu;
                                                rootid = 0; cumid = 0; cmid = 0; getlcumcode = 0; lcumcode = 0; input = 0; corres = 0;
				}
                                        }

				
				Ses.UpdateSubscriberMenuSession(msisdn, rootid, cumid, cmid, getlcumcode, lcumcode, input, corres, "DM");
                        	PushLog.record("UpdateSubscriberMenuSession " + msisdn + "," + rootid + "," + cumid + "," +cmid+ "," +getlcumcode+ "," +lcumcode+ "," +input+ "," +corres+", DM|" + _depthM + "|" + _getLcc) ;
			}
		}
		else if (_depthM == 2) {%><%@include file="step1.jsp" %><%}
		else if (_depthM == 3) {%><%@include file="step2.jsp" %><%}
		else if (_depthM == 4) {%><%@include file="step3.jsp" %><%}
		else if (_depthM == 5) {%><%@include file="endReg.jsp" %><%}
	}
		
%>
