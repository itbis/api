<%@ page import = "java.io.*, java.net.*,java.sql.*, java.util.*, java.text.*" %><jsp:useBean id="Ses" scope="application" class="com.telkomsel.cp.util.Session"></jsp:useBean><jsp:useBean id="Db" scope="application" class="com.telkomsel.cp.util.DbJasindo"></jsp:useBean><jsp:useBean id="Http" scope="application" class="com.telkomsel.cp.util.HttpGet"></jsp:useBean><%
	
	String msisdn = request.getParameter("msisdn");
	String msg = request.getParameter("msg");
	StringTokenizer token1;
        Vector v1;
	int threshold = 25000;
	String adn = "807";
	String u = null;
	int value = -1;

	if( msisdn == null || msg == null || msisdn.equals("62812819167777") || msisdn.equals("6282123459774") || msisdn.equals("6282174975222") || msisdn.equals("628111101772") || msisdn.equals("6281281210909") || msisdn.equals("6285216171863") || msisdn.equals("628111111973") || msisdn.equals("628119950543") || msisdn.equals("62811109080") || msisdn.equals("62811186789") || msisdn.equals("628809070") || msisdn.equals("62811916441") || msisdn.equals("62811159446") || msisdn.equals("628111171115") || msisdn.equals("62811917695")|| msisdn.equals("628111987768")||msisdn.equals("6281290201937") || msisdn.equals("6281281916777") || msisdn.equals("6281281916555") ) {
		if (msg.equals("*" + adn + "*1#")) {
			value = Db.getBalance(msisdn);
			if (value > -1) {
				msg = "00Pulsa Anda sebesar Rp." + value;
			}
			else {
				msg = "00Invalid Number";
			}
		}
		else if (msg.equals("*" + adn + "*2#")) {
			value = Db.getTcash(msisdn);
			//value = 0;
                        if (value > -1) {
				msg = "00Saldo T-Cash Anda sebesar Rp." + value;
			}
                        else {
                                msg = "00Invalid Number";
                        }	
		}
		else {
			if (msg.startsWith("*" + adn)) {
				Ses.DestroySubscriberMenuSession(msisdn);
				msg = msg.replaceAll("\\#","");
				String [] payload = msg.split("\\*");
				System.out.println("Panjang msg = +++++++++++++++" + payload.length);
				if(payload.length == 3) {
					int balance = 0;
					String hrn_no = "0000";
					//chek DB HRN
					try {
						hrn_no = payload[2];
						balance = Db.getNominal(hrn_no);
						if (balance > -1) {
							if (balance >= threshold) {
								int balance1 = balance - 3500;
								msg = "Apakah Anda ingin membeli premi asuransi Jasindo seharga Rp.3500 dan pulsa senilai Rp." + Integer.toString(balance1) + "\n" 
								+ "1.Ya\n"
								+ "2.Tidak\n"
								+ "(2.Tidak, Secara otomatis pulsa Anda Rp."+balance+")"; 
								Ses.CreateSubscriberMenuSession(msisdn,0,0,Integer.toString(balance),0,0,Integer.toString(balance1)," ");
							}
							else {
								msg = "00Terimakasih permintaan isi pulsa Anda sebesar Rp." + balance + ", sedang Kami proses.";
		
								Db.updateBalance(msisdn,balance);
								//sms
								String sms_resp = "Terimkasih pulsa Anda telah bertambah sebesar Rp." + balance + ", cek pulsa Anda call *807*1#"  ;
								sms_resp = URLEncoder.encode(sms_resp, "UTF-8");
                                				u = Http.get("http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=Telkomsel&to="+msisdn+"&text="+sms_resp,0,5000);
							}
						}
						else {
							msg = "00HRN number invalid.";
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				else {
					msg = "00Command Invalid(001)";
				}
			} else {
                        	//URLEncoder.encode(command, "UTF-8");
                       		String getSession = Ses.GetSubscriberMenuSession(msisdn);
	
				String b = "0";
				String b1 = "0";
				String stat = "0";
                       		if (getSession.equals("dodol")) {
					msg = "00Transaction Failed.";
				} else {
					token1 = new StringTokenizer(getSession, "|", false);
                               		v1 = new Vector();
                               		while(token1.hasMoreTokens())
                               		{
                                       		v1.add(token1.nextToken());
                               		}

					stat = (String)v1.get(0);
                               		b = (String)v1.get(2);
                               		b1 = (String)v1.get(5);
				}

				if (stat.equals("0")) {
					if(msg.equals("1")) {
						//msg = "00Mohon maaf, proses pengisian pulsa Anda gagal dikarenakan gangguan sistem. Mohon coba beberapa saat lagi.";
						msg = "Terimakasih permintaan isi pulsa Anda sebesar Rp." + b1 + ", dan premi asuransi Jasindo sebesar Rp.3500 akan segera diproses.\n"
						+ "Ketik 1. Untuk Pengisian data diri asuransi";
						Db.updateBalance(msisdn, Integer.parseInt(b1));
						//sms
				        	String sms_resp = "Terimkasih pulsa Anda telah bertambah sebesar Rp." + b1 + ", cek pulsa Anda call *807*1#. dan Anda telah membeli premi asuransi jasindo sebesar Rp.3500." ;
                                        	sms_resp = URLEncoder.encode(sms_resp, "UTF-8");
                                        	u = Http.get("http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=Telkomsel&to="+msisdn+"&text="+sms_resp,0,5000);
						Ses.CreateSubscriberMenuSession(msisdn,1,0,b,0,0,b1," ");
					}
					else {
						msg = "00Terimakasih permintaan isi pulsa Anda sebesar Rp." + b + ", sedang Kami proses. (TIDAK ada pembelian premi Jasindo)";
					
						Db.updateBalance(msisdn,Integer.parseInt(b));	
						//sms
						String sms_resp = "Terimkasih pulsa Anda telah bertambah sebesar Rp." + b + ", cek pulsa Anda call *807*1#"  ;
                                        	sms_resp = URLEncoder.encode(sms_resp, "UTF-8");
                                        	u = Http.get("http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=Telkomsel&to="+msisdn+"&text="+sms_resp,0,5000);
					}
				}
				else {
					String resp = "";
					//u = Http.get("http://119.235.30.5/tetra/?msisdn="+msisdn+"&command="+msg,0,7000);
					u = Http.get("http://10.2.230.218/jasindo/?msisdn="+msisdn+"&command="+msg,0,7000);
					resp = u;
					if (u.startsWith("Terima Kasih.")) {
						resp = "00" + u;
                                                u = URLEncoder.encode(u, "UTF-8");
                                                u = Http.get("http://10.2.224.148:5054/cgi-bin/sendsms?user=tester&pass=foobar&from=Telkomsel&to="+msisdn+"&text="+u,0,5000);
					}
					msg = resp;
				}
			}
		}
	}
	else {
		msg = "00Access Denied!!";
	}

	out.println(msg);
%>
