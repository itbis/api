package src;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

//public class PropsReader implements FileChangeListener
public class PropsReader
{
        public static Logger logger = Logger.getLogger(PropsReader.class);

    	FileInputStream fStream;
    	Properties pFile;
        String fileName;
    	long checkPeriod;

    	public PropsReader(String fileName)
    	{
                this.fileName = fileName;
                reloadProperties();
    	}
    
	/*
    	public synchronized void setCheckPeriod(long period)
        {
        	checkPeriod = period;
                try
                {
                        FileMonitor.getInstance().addFileChangeListener(this, fileName, checkPeriod);
                }
                catch(FileNotFoundException e)
                {
                }
    	}
    	*/

    	private void reloadProperties()
        {
                try
                {
                        fStream = new FileInputStream(fileName);
                        pFile = new Properties();
                        pFile.load(fStream);
                        fStream.close();
                }
                catch(IOException e)
                {
                }

                //setCheckPeriod(Long.parseLong(pFile.getProperty("ConfigCheckPeriod")));
    	}

    	public void fileChanged(String fileName)
        {
        	reloadProperties();
    	}

    	public String readProps(String param)
    	{
        	return pFile.getProperty(param);
    	}

    	public String readString(String param)
    	{
        	return pFile.getProperty(param);
    	}

    	public int readInt(String param)
    	{
        	return Integer.parseInt(pFile.getProperty(param));
    	}

    	public long readLong(String param)
    	{
        	return Long.parseLong(pFile.getProperty(param));
    	}

    	public boolean readBoolean(String param)
    	{
        	return Boolean.parseBoolean(pFile.getProperty(param));
    	}

}

