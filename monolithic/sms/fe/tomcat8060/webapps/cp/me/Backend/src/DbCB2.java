package src;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

public class DbCB2
{
	public static Logger logger = Logger.getLogger(DbCB2.class.getName());

	PropsReader Cfg;

	public DbCB2(PropsReader Cfg)
    {
    	this.Cfg = Cfg;
	}

    public DataSource setupDataSource()
	{
        BasicDataSource ds = new BasicDataSource();
		logger.info("Loading database driver.");
        ds.setDriverClassName(Cfg.readString("DbB2.Driver"));
		logger.info("Database driver loaded successfuly.");
		
		logger.info("Setting up DataSource.");
        ds.setUsername(Cfg.readString("DbB2.User"));
        ds.setPassword(Cfg.readString("DbB2.Pwd"));
        ds.setUrl(Cfg.readString("DbB2.Url"));
		ds.setInitialSize(Cfg.readInt("DbB2.PoolInitialSize"));
		ds.setMaxActive(Cfg.readInt("DbB2.PoolMaxActive"));
		ds.setMaxIdle(Cfg.readInt("DbB2.PoolMaxIdle"));
		ds.setMinIdle(Cfg.readInt("DbB2.PoolMinIdle"));
		ds.setMaxWait(Cfg.readLong("DbB2.PoolMaxWait"));
		ds.setRemoveAbandoned(Cfg.readBoolean("DbB2.PoolRemoveAbandoned"));
		ds.setRemoveAbandonedTimeout(Cfg.readInt("DbB2.PoolRemoveAbandonedTimeout"));
		ds.setLogAbandoned(Cfg.readBoolean("DbB2.PoolLogAbandoned"));
		logger.info("DataSource Setup Ok.");
		
        return ds;
    }
}
