package src;

import java.net.*;
import java.io.*;


public class soap {

  //public final static String DEFAULT_SERVER = "http://www.elharo.com/fibonacci/SOAP";
  public final static String DEFAULT_SERVER = "http://10.2.188.183:10021/vas_soap";
  //public final static String SOAP_ACTION = "http://www.example.com/fibonacci";
  public final static String SOAP_ACTION = "";

  public static void main(String[] args) {
  
    String server = DEFAULT_SERVER;
    
    try {
      URL u = new URL(server);
      URLConnection uc = u.openConnection();
      HttpURLConnection connection = (HttpURLConnection) uc;
      
      connection.setDoOutput(true);
      connection.setDoInput(true);

      connection.setRequestMethod("POST");
      connection.setRequestProperty("Host", "10.2.188.183");
      connection.setRequestProperty("Authorization", "Basic MjAwNDoxMjM0");
      connection.setRequestProperty("User-Agent", "MM7submit/1.0");
      connection.setRequestProperty("Content-Type", "multipart/related; boundary=\"----_=_NextPart_BD0_B70_5DF.14B2\"; type=\"text/xml\";");
      
      OutputStream out = connection.getOutputStream();
      Writer wout = new OutputStreamWriter(out);
    
      wout.write("\t");
      wout.write("start=\"<mm7submit@localhost>\"\r\n");
      //wout.write("Content-Length: 1534");
      wout.write("SOAPAction: \"\"\r\n");
      wout.write("Accept: */*\r\n");
      wout.write("----_=_NextPart_BD0_B70_5DF.14B2\r\n");
      wout.write("Content-Type: application/xml\r\n");
      wout.write("Content-ID: <mm7submit@localhost>\r\n");
      wout.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
      wout.write("<SOAP-ENV:Envelope \r\n");
      wout.write("xmlns:SOAP-ENV=\r\n");    
      wout.write("\"http://schemas.xmlsoap.org/soap/envelope/\" \r\n" );
      wout.write("xmlns=\"http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-2\">\r\n");
      wout.write("<SOAP-ENV:Header>\r\n");
      		wout.write("<TransactionID SOAP-ENV:mustUnderstand=\"1\">vas00001</TransactionID>\r\n");	
      wout.write("</SOAP-ENV:Header>\r\n");
      wout.write("<SOAP-ENV:Body>\r\n");
      		wout.write("<SubmitReq>\r\n");
      			wout.write("<MM7Version>5.3.0</MM7Version>\r\n");
      			wout.write("<SenderIdentification>\r\n");
      				wout.write("<VASPID>2004</VASPID>\r\n");
      				wout.write("<VASID>2004</VASID>\r\n");
      				wout.write("<SenderAddress><ShortCode>2004</ShortCode></SenderAddress>\r\n");
      			wout.write("</SenderIdentification>\r\n");
      			wout.write("<Recipients>\r\n");
      				wout.write("<To><Number>62811916441</Number></To>\r\n");
      			wout.write("</Recipients>\r\n");
      			wout.write("<MessageClass>Informational</MessageClass>\r\n");
      			wout.write("<TimeStamp>2011-05-13T20:28:01+07:00</TimeStamp>\r\n");
      			wout.write("<DeliveryReport>false</DeliveryReport>\r\n");
      			wout.write("<ReadReply>false</ReadReply>\r\n");
      			wout.write("<Priority>Normal</Priority>\r\n");
      			wout.write("<Subject>test</Subject>\r\n");
      			wout.write("<ChargedParty>Sender</ChargedParty>\r\n");
      			wout.write("<Content href=\"cid:content@localhost\" allowAdaptations=\"true\"/>\r\n");
      		wout.write("</SubmitReq>\r\n");
      wout.write("</SOAP-ENV:Body>\r\n");
      wout.write("</SOAP-ENV:Envelope>\r\n");
      
      wout.write("----_=_NextPart_BD0_B70_5DF.14B2\r\n");
      wout.write("Content-Type: multipart/mixed; boundary=\"----_=_NextPart_8B8_16D5_E32.2237\"\r\n ");
      wout.write("Content-ID: <content@localhost>\r\n");	
      wout.write("------_=_NextPart_8B8_16D5_E32.2237\r\n");
      wout.write("Content-Type: text/plain; charset=\"us-ascii\"; Name=text0.txt\r\n");
      wout.write("Content-ID: <text0.txt>\r\n");
      wout.write("Content-Location: text0.txt\r\n");
      wout.write("tetra lia keysha\r\n");
      wout.write("------_=_NextPart_8B8_16D5_E32.2237--\r\n");
      wout.write("----_=_NextPart_BD0_B70_5DF.14B2--\r\n");

      wout.flush();
      wout.close();
     
      InputStream in = connection.getInputStream();
      int c;
      while ((c = in.read()) != -1) System.out.write(c);
      in.close();
     

    }
    catch (IOException e) {
      System.err.println(e); 
    }
  
  } // end main

}
