package src;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import javax.sql.DataSource;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Queue;
import java.sql.*;

public class DbDaemon extends Thread {
   private ServerSocket Server;
   LinkedList<String> list1;
   LinkedList<String> list2;
   static Logger logger = Logger.getLogger(DbDaemon.class.getName());
   String file;
   DbPB1 DB1;
   DataSource Ds1;
   String tbl_processor;

   public static void main(String argv[]) throws Exception {
     	PropertyConfigurator.configure(argv[0]);

	//load configuration from file
        PropsReader props = new PropsReader(argv[0]);
        log.record("Initiation ...." + argv[0]);

	//DEFINE THREAD
     	LinkedList<String> list1 = new LinkedList<String>();
     	LinkedList<String> list2 = new LinkedList<String>();

	PropsReader Props2 = new PropsReader(argv[0]);
	DbCB2 DbC2 = new DbCB2(Props2);
        DataSource Ds2 = DbC2.setupDataSource();
        DbPB2 DB2 = new DbPB2(Props2, Ds2);
	
	DbCB1 DbC1 = new DbCB1(Props2);
        DataSource Ds1 = DbC1.setupDataSource();
        //DbPB1 DB1;
        //DB1 = new DbPB1(Props2, Ds1);

	Thread ss1 = new Thread(new DbDaemon(argv[0],list1,list2,Ds1, "queue_trxid"));
	Thread t1 = new Thread(new SendSms(argv[0],list1,DB2));
	Thread t2 = new Thread(new SendSms(argv[0],list2,DB2));
	ss1.start();	
	t1.start();
	t2.start();

	try {
		ss1.join();
		t1.join();
		t2.join();
	}
	catch(InterruptedException e) {
		logger.fatal(e.getMessage());
	}

   }

   public DbDaemon(String file,LinkedList<String> list1,LinkedList<String> list2, DataSource Ds1, String tbl_processor) throws Exception {
     	this.file = file;
     	this.list1 = list1;
     	this.list2 = list2;
     	PropsReader Props = new PropsReader(file);	
	this.tbl_processor = tbl_processor;
	this.Ds1 = Ds1;
        this.DB1 = new DbPB1(Props, Ds1);
	for(int i=0; i<10; i++) {
                Thread t = new Thread(this);
        }
   }
  
   public void run() {
	int q = 0;
	String req = null;
	for(;;) {
		try { Thread.sleep(1000);}catch(InterruptedException e) {}
		try {
			ResultSet rs = DB1.doQuery("SELECT * FROM "+tbl_processor+" limit 1");
			if(rs.next()) {
				String trxid = rs.getString("trxid");
				String msg = rs.getString("msg");
				msg = msg.replaceAll(";", " ");

				log.record(tbl_processor + "|" + trxid + "|" + msg);
				if(!trxid.equals("")) {	
					req = trxid + ";" + msg;
					q = Integer.parseInt(req.substring(0,1));
				
					log.record("queue|" + trxid + "|" + Integer.toString(q) + "|" + msg);
					if (q % 2 == 0) {
                                   		try {
                                			synchronized(list1) {
                                               			list1.addLast(req);
                                               			list1.notify();
                                        		}
                                   		} catch (Exception e) {
							logger.fatal("queue:"+e.getMessage());
				   		}
					}
					else {
			           		try {
                                        		synchronized(list2) {
                                               			list2.addLast(req);
                                               			list2.notify();
                                        		}
                                   		} catch (Exception e) {
                                        		logger.fatal("queue:"+e.getMessage());
                                   		}		
					}
				}
				
				DB1.doUpdate("DELETE FROM "+tbl_processor+" WHERE trxid='"+trxid+"'");
				
			}
			rs.close();
			synchronized (this) {DB1.closeDBConnection();}
       		}
		catch (Exception e) {
			synchronized (this) {DB1.closeDBConnection();}
			logger.fatal("DbDaemon 3:" + e.getMessage());
		}
   	}
   }	
}




