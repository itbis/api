package src;

import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

public class DbCB1
{
	public static Logger logger = Logger.getLogger(DbCB1.class.getName());

	PropsReader Cfg;

	public DbCB1(PropsReader Cfg)
    {
    	this.Cfg = Cfg;
	}

    public DataSource setupDataSource()
	{
        BasicDataSource ds = new BasicDataSource();
		logger.info("Loading database driver.");
        ds.setDriverClassName(Cfg.readString("DbB1.Driver"));
		logger.info("Database driver loaded successfuly.");
		
		logger.info("Setting up DataSource.");
        ds.setUsername(Cfg.readString("DbB1.User"));
        ds.setPassword(Cfg.readString("DbB1.Pwd"));
        ds.setUrl(Cfg.readString("DbB1.Url"));
		ds.setInitialSize(Cfg.readInt("DbB1.PoolInitialSize"));
		ds.setMaxActive(Cfg.readInt("DbB1.PoolMaxActive"));
		ds.setMaxIdle(Cfg.readInt("DbB1.PoolMaxIdle"));
		ds.setMinIdle(Cfg.readInt("DbB1.PoolMinIdle"));
		ds.setMaxWait(Cfg.readLong("DbB1.PoolMaxWait"));
		ds.setRemoveAbandoned(Cfg.readBoolean("DbB1.PoolRemoveAbandoned"));
		ds.setRemoveAbandonedTimeout(Cfg.readInt("DbB1.PoolRemoveAbandonedTimeout"));
		ds.setLogAbandoned(Cfg.readBoolean("DbB1.PoolLogAbandoned"));
		logger.info("DataSource Setup Ok.");
		
        return ds;
    }
}
