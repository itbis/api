package src;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class DbPB1
{
	public static Logger logger = Logger.getLogger(DbPB1.class);

	PropsReader Cfg;
	DataSource dataSource;
    	Connection dbConn;
    	Statement stmt;
    	ResultSet rs;
	String query;
	PreparedStatement pstmt;

	public DbPB1(PropsReader Cfg, DataSource dataSource)
    {
		this.Cfg = Cfg;
		this.dataSource = dataSource;
	}

    public Connection createDBConnection() throws IOException, SQLException
    {
		if(dataSource != null)
		{
			dbConn = dataSource.getConnection();
		}
		else
			logger.fatal("Fails to create DB connection --> DataSource null");

		return dbConn;
    }

    public void closeDBConnection()
    {
        try
        {
            if(dbConn != null)
            {
		dbConn.close();
		dbConn = null;
            }
        }
        catch(Exception e)
        {
            logger.fatal("Exception on close DB connection: " + e.getMessage(), e);
        }
    }

	public ResultSet doQuery(String s) throws IOException, SQLException
    {
        if(dbConn == null || dbConn.isClosed())
                createDBConnection();

        //System.out.println("CP DB Connection Insert : "+dbConn);
        stmt = dbConn.createStatement();
        rs = stmt.executeQuery(s);
        return rs;
    }

    public void doUpdate(String s) throws IOException, SQLException
    {
        if(dbConn == null || dbConn.isClosed())
                createDBConnection();

        //System.out.println("CP DB Connection Update : "+dbConn);
        stmt = dbConn.createStatement();
        stmt.executeUpdate(s);
        stmt.close();
    }


}
