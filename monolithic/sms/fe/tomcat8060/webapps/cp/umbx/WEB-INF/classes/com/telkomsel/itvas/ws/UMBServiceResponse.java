package com.telkomsel.itvas.ws;

public class UMBServiceResponse {
	public String Status;
	public String ErrorMessage;
	public String ErrorCode;

	/**
	 * Constructor.
	 * @param  name book name
	 * @param author book  author
	 * @throws IllegalArgumentException  name or author is null
	 */
	public UMBServiceResponse(String status, String errorMessage,
			String errorCode) {
		if (status == null) {
			throw new IllegalArgumentException("status is null!");
		}

		if (errorMessage == null) {
			throw new IllegalArgumentException("errorMessage is null!");
		}

		if (errorCode == null) {
			throw new IllegalArgumentException("errorCode is null!");
		}

		this.Status = status;
		this.ErrorMessage = errorMessage;
		this.ErrorCode = errorCode;
	}

	/**
	 * Test for  equality.
	 * @param object any object
	 * @return true if books are equal
	 */
	public boolean equals(Object object) {
		if (!(object instanceof UMBServiceResponse)) {
			return false;
		}

		UMBServiceResponse secondBook = (UMBServiceResponse) object;

		return Status.equals(secondBook.Status) && ErrorMessage.equals(secondBook.ErrorMessage);
	}
	
	public String toString() {
		return Status + "|" + ErrorCode + "|" + ErrorMessage;
	}
}