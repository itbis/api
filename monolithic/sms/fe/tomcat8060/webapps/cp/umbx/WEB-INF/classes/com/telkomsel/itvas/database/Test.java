package com.telkomsel.itvas.database;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.encoding.XMLType;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;

import com.telkomsel.itvas.ws.UMBServiceResponse;
import com.telkomsel.itvas.ws.UMBServiceResponseDeserFactory;
import com.telkomsel.itvas.ws.UMBServiceResponseSerFactory;


public class Test {

	/**
	 * @param args
	 * @throws ServiceException 
	 * @throws MalformedURLException 
	 * @throws RemoteException 
	 */
	public static void main(String[] args) throws ServiceException, MalformedURLException, RemoteException {
		Service service = new Service();
		Call call = (Call) service.createCall();
		
		QName    qn      = new QName( "http://www.openuri.org/", "UMBServiceResponse" );
		
		
		call.registerTypeMapping(UMBServiceResponse.class, qn,
                new UMBServiceResponseSerFactory(),        
                new UMBServiceResponseDeserFactory());        

		call.setTargetEndpointAddress(new java.net.URL("http://10.2.248.66:8668/UMBServiceDev/com/visitek/tsel/soa/umbservice/UMBService.jws"));
		call.setOperationName(new QName(
				"http://www.openuri.org/",
				"queryPUK"));
		
			call.addParameter(new QName(
					"http://www.openuri.org/", "msisdn"),
					XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter(new QName(
					"http://www.openuri.org/", "pin"),
					XMLType.XSD_STRING, ParameterMode.IN);
			call.addParameter(new QName(
					"http://www.openuri.org/", "imsi"),
					XMLType.XSD_STRING, ParameterMode.IN);

		call.setReturnType(qn);
//		call.setReturnType(new QName("http://www.openuri.org/", "UMBServiceResponse"), UMBServiceResponse.class);
//		call.setReturnClass(UMBServiceResponse.class);
		Object ret = call.invoke(new Object[] {"62811917427", "11", "11"});
		UMBServiceResponse response = (UMBServiceResponse) ret;
		System.out.println("retval : " + response);
		String regex = "<content>(Pemakaian sementara.*)</content>";
		String content = "<content>Pemakaian sementara Halo dunia coi </content>";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(content);
		
		while (m.find()) {
			System.out.println("group : " + m.group(1));
			System.out.println("group : " + m.groupCount());
		}
	}

}
