<%@page import="com.telkomsel.itvas.umbx.Flow"%><%@page import="com.telkomsel.itvas.umbx.FlowContainer"%><%
	FlowContainer container = FlowContainer.getInstance();
String flowId = request.getParameter("x");
String msisdn = request.getParameter("msisdn");
String command = request.getParameter("command");
String refresh = request.getParameter("refresh");
String reset = request.getParameter("reset");
if(reset==null)reset="0";
if(reset.equals("1"))refresh="1";

if (flowId == null || msisdn == null || command == null) {
	System.out.println("debug1");
	out.println("PARAM_NOT_COMPLETE");
} else {
	Flow flow = null;
	try {
		if (refresh != null && refresh.equals("1")) {
			flow = container.get(flowId, true);
			String ex=flow.exec(msisdn, "0", request);
		} else {
			flow = container.get(flowId);
		}
		
		if (flow == null) {
			out.println("Flow not found : " + flowId);
		} else {
			String myResponse = flow.exec(msisdn, command, request);
			out.println(myResponse);
		}
	} catch (Exception e) {
		//e.printStackTrace(out);
		throw e;
	}
}%>
