package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import cweeta.telkomsel.util.*;

public class rec_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      cweeta.telkomsel.util.Function Func = null;
      synchronized (pageContext) {
        Func = (cweeta.telkomsel.util.Function) pageContext.getAttribute("Func", PageContext.PAGE_SCOPE);
        if (Func == null){
          try {
            Func = (cweeta.telkomsel.util.Function) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "cweeta.telkomsel.util.Function");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "cweeta.telkomsel.util.Function", exc);
          }
          pageContext.setAttribute("Func", Func, PageContext.PAGE_SCOPE);
        }
      }
      cweeta.telkomsel.util.Db Db = null;
      synchronized (pageContext) {
        Db = (cweeta.telkomsel.util.Db) pageContext.getAttribute("Db", PageContext.PAGE_SCOPE);
        if (Db == null){
          try {
            Db = (cweeta.telkomsel.util.Db) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "cweeta.telkomsel.util.Db");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "cweeta.telkomsel.util.Db", exc);
          }
          pageContext.setAttribute("Db", Db, PageContext.PAGE_SCOPE);
        }
      }
      cweeta.telkomsel.util.HttpGet HTTP = null;
      synchronized (pageContext) {
        HTTP = (cweeta.telkomsel.util.HttpGet) pageContext.getAttribute("HTTP", PageContext.PAGE_SCOPE);
        if (HTTP == null){
          try {
            HTTP = (cweeta.telkomsel.util.HttpGet) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "cweeta.telkomsel.util.HttpGet");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "cweeta.telkomsel.util.HttpGet", exc);
          }
          pageContext.setAttribute("HTTP", HTTP, PageContext.PAGE_SCOPE);
        }
      }
      cweeta.telkomsel.util.Session Ses = null;
      synchronized (application) {
        Ses = (cweeta.telkomsel.util.Session) pageContext.getAttribute("Ses", PageContext.APPLICATION_SCOPE);
        if (Ses == null){
          try {
            Ses = (cweeta.telkomsel.util.Session) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "cweeta.telkomsel.util.Session");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "cweeta.telkomsel.util.Session", exc);
          }
          pageContext.setAttribute("Ses", Ses, PageContext.APPLICATION_SCOPE);
        }
      }


	String msg = "dlr-sms-inout";
	String msgid = request.getParameter("msgid");
	String status = request.getParameter("state");
	String msisdn = request.getParameter("msisdn");
	DLR.record("DLR-SMS" + msgid + "|" + status + "|" + msisdn);	
	out.print("DLR-SMS" + msgid + "|" + status + "|" + msisdn);	

      out.write("\n\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
