package com.telkomsel.cp.sms;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.*;
import java.net.*;
import com.telkomsel.cp.util.*;

public class SubmitSMDirect
{
	private DbCP db;

    public SubmitSMDirect()
    {
    	db = new DbCP();
    }

    public String submitSM(String cp_name, String charge_type, String from, String oa, String msisdn, String msg, String status, String trid, String smsid, String contentid, String ip_smsgw, String port_smsgw, String id_smsgw, String pwd_smsgw) throws IOException
    {
		String a = "";
        try
        {
			String dlrURL = URLEncoder.encode("http://10.1.89.197:7000/cp/dlr.jsp?smsid=" + smsid + "&trxid=" + trid + "&cp_name=" + cp_name + "&ctype=" + charge_type, "ISO-8859-1");
   			//String url = "http://" + ip_smsgw + ":" + port_smsgw + "/cgi-bin/sendsms?username=" + id_smsgw + "&password=" + pwd_smsgw + "&from=" + from + "&to=" + msisdn + "&text=" + msg + "&dlrmask=1&dlrurl=" + dlrURL;
			String url = "http://" + ip_smsgw + ":" + port_smsgw + "/cgi-bin/sendsms?username=" + id_smsgw + "&password=" + pwd_smsgw + "&from=" + from + "&to=" + msisdn + "&text=" + msg + "&dlrmask=1&dlrurl=" + dlrURL;
			String s = send(url);
   			if(s.equals("Sent."))
   			{
   				System.out.println(new java.util.Date() + " : INFO : SEND " + cp_name + " FROM " + from + " TO " + msisdn + " CONTENT: " + msg);
				String sql = "";
				if(charge_type.equals("MT"))
					sql = "INSERT INTO cp_report VALUES('" + cp_name + "','" + msisdn + "','" + smsid + "','" + contentid + "'," + oa + ",now(),'N','','" + trid + "','" + status + "')";
				else
					sql = "INSERT INTO cp_reportMO VALUES('" + cp_name + "','" + msisdn + "','" + smsid + "','" + contentid + "'," + oa + ",now(),'N','','" + trid + "','" + status + "')";
   				db.doUpdate(sql);
   				a = "OK";
   				db.closeDBConnection();
			}
			else
			{
				System.out.println(new java.util.Date() + " : ERROR : CANNOT SEND " + cp_name + " FROM " + from + " TO " + msisdn + " CONTENT: " + msg + " BECAUSE SMSC LINK IS DOWN");
				a = "NOK";
			}
		}
        catch(SQLException e)
        {
            System.out.println(new java.util.Date() + " : " + cp_name + " ERROR : " + e.getMessage());
            a = "NOK";
        }
        //finally
        //{
			//db.closeDBConnection();
		//}
        return a;
    }

	public String submitNotif(String cp_name, String from, String msisdn, String msg) throws IOException
    {
		String a = "";
		String url = "http://10.1.81.19:15077/cgi-bin/sendsms?username=aqsreg&password=telkomsel&from=" + from + "&to=" + msisdn + "&text=" + msg;
		String s = send(url);
		if(s.equals("Sent."))
		{
			System.out.println(new java.util.Date()+" : INFO : SEND " + cp_name + " FROM "+from+" TO "+msisdn+" CONTENT: "+msg);
			a = "OK";
		}
		else
		{
			System.out.println(new java.util.Date()+" : ERROR : CANNOT SEND " + cp_name + " FROM "+from+" TO "+msisdn+" CONTENT: "+msg+" BECAUSE SMSC LINK IS DOWN");
			a = "NOK";
		}
        return a;
    }

    public static String send(String address) throws MalformedURLException, IOException, ClassCastException
    {
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in1 = url.openStream();
			BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
			String line1 = "";
			while((line1 = br1.readLine()) != null)
			{
				res = res.concat(line1);
			}
			br1.close();
			in1.close();
		}
		catch(Exception e)
		{
			res = "dodol";
		}

        return res;
    }
}
