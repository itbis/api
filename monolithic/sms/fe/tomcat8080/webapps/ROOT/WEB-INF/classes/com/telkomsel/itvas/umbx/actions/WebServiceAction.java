package com.telkomsel.itvas.umbx.actions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.DeserializerFactory;
import org.apache.axis.encoding.SerializerFactory;

import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;

import net.n3.nanoxml.IXMLElement;

public class WebServiceAction extends Action {
	private String wsdl;
	private String method;
	private String namespace;
	private ArrayList<WsParam> params = new ArrayList<WsParam>();
	private WsResponse response;

	public WebServiceAction(IXMLElement element) throws UmbAutoException {
		method = element.getAttribute("method", null);
		namespace = element.getAttribute("namespace", null);

		wsdl = element.getAttribute("wsdl", null);
		if (method == null) {
			throw new UmbAutoException(
					"[WebServiceFlow] attr not complete : method");
		}
		if (wsdl == null) {
			throw new UmbAutoException(
					"[WebServiceFlow] attr not complete : wsdl");
		}
		if (namespace == null) {
			throw new UmbAutoException(
					"[WebServiceFlow] attr not complete : namespace");
		}
		
		Enumeration<IXMLElement> eParams = element.enumerateChildren();
		while (eParams.hasMoreElements()) {
			IXMLElement param = eParams.nextElement();
			if (param.getName().equalsIgnoreCase("params")) {
				params.add(new WsParam(param));
			} else if (param.getName().equalsIgnoreCase("response")) {
				response = new WsResponse(param);
			}
		}
		
		if (response == null) {
			throw new UmbAutoException(
			"[WebServiceFlow] attr not complete : response");
		}
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		// HTTP Get URL
		String myWsdl = formatState(wsdl, state);
		String myMethod = formatState(method, state);
		String myNamespace = formatState(namespace, state);
		String strFirst = (first) ? "Yes" : "No";
		try {
			Service service = new Service();
			Call call = (Call) service.createCall();
			
			if (response.getClassName().equalsIgnoreCase("string")) {
				call.setReturnType(XMLType.XSD_STRING);	
			} else if (response.getClassName().equalsIgnoreCase("integer")) {
				call.setReturnType(XMLType.XSD_INTEGER);	
			} else if (response.getClassName().equalsIgnoreCase("boolean")) {
				call.setReturnType(XMLType.XSD_BOOLEAN);	
			} else {
				// Register custom type for WS-Response
				Class c = Class.forName(response.getClassName());
				Class cSerFactory = Class.forName(response.getClassName() + "SerFactory");
				Class cDeserFactory = Class.forName(response.getClassName() + "DeserFactory");
				QName qn = new QName(namespace, response.getClassName());
				call.registerTypeMapping(c.getClass(), qn,
		                (SerializerFactory) cSerFactory.newInstance(),        
		                (DeserializerFactory) cDeserFactory.newInstance());
				call.setReturnType(qn);
				System.out.println("register cuk");
			}
			

			call.setTargetEndpointAddress(new java.net.URL(myWsdl));
			call.setOperationName(new QName(
					myNamespace,
					myMethod));
			
			
			for (WsParam p : params) {
				System.out.println("haiya debug : " + p.getVar());
				call.addParameter(new QName(
						myNamespace, p.getVar()),
						p.getType(), ParameterMode.IN);

			}
			
			Object[] objects = new Object[params.size()];
			for (int i=0; i<objects.length;i++) {
				objects[i] = formatState(params.get(i).getValue(), state);
			}
			
			Object ret = call.invoke(objects);
			System.out.println("retval : " + ret);
			System.out.println("retvalclass : " + ret.getClass());
			String retval = response.findMatchingResponse(ret.toString(), state);
			return formatResult("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
					+ "<data>"+formatState(retval, state)+"</data>").trim();
		} catch (Exception e) {
			System.err.println(e.toString());
			return formatResult("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
					+ "<data>"+formatState(response.getElseCase().getResponse(), state)+"</data>").trim();
		}
	}
}

class WsParam {
	private String var;
	private QName type;
	private String value;

	public WsParam(IXMLElement param) {
		String strtype = param.getAttribute("type", null);
		if (strtype.equalsIgnoreCase("string")) {
			type = XMLType.XSD_STRING;
		} else if (strtype.equalsIgnoreCase("integer")) {
			type = XMLType.XSD_INTEGER;
		} else if (strtype.equalsIgnoreCase("boolean")) {
			type = XMLType.XSD_BOOLEAN;
		} else if (strtype.equalsIgnoreCase("float")) {
			type = XMLType.XSD_FLOAT;
		} else {
			type = XMLType.XSD_STRING;
		}
		
		var = param.getAttribute("var", null);
		System.out.println("haiya test : " + var);
		value = param.getContent();
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public QName getType() {
		return type;
	}

	public void setType(QName type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

class WsResponse {
	private ArrayList<WsResponseCase> cases;
	private WsResponseCase elseCase;
	private String className;
	
	public WsResponse(IXMLElement element) throws UmbAutoException {
		className = element.getAttribute("class", null);
		if (className == null) {
			throw new UmbAutoException("[WsResponse] class is null");
		}
		Enumeration<IXMLElement> eParams = element.enumerateChildren();
		cases = new ArrayList<WsResponseCase>();
		while (eParams.hasMoreElements()) {
			IXMLElement param = eParams.nextElement();
			if (param.getName().equalsIgnoreCase("case")) {
				cases.add(new WsResponseCase(param));
			} else if (param.getName().equalsIgnoreCase("else")) {
				elseCase = new WsResponseCase(param);
			}
		}
		if (elseCase == null) {
			throw new UmbAutoException("[WsResponse] elseCase is null");
		}
	}
	
	public String findMatchingResponse(String s, CState state) {
		for (WsResponseCase c : cases) {
			if (c.matched(s, state)) {
				return c.getResponse();
			}
		}
		return elseCase.getResponse();
	}

	public ArrayList<WsResponseCase> getCases() {
		return cases;
	}

	public void setCases(ArrayList<WsResponseCase> cases) {
		this.cases = cases;
	}

	public WsResponseCase getElseCase() {
		return elseCase;
	}

	public void setElseCase(WsResponseCase elseCase) {
		this.elseCase = elseCase;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
}

class WsResponseCase {
	private Pattern pattern;
	private String regex;
	private String response;
	
	public WsResponseCase(IXMLElement param) throws UmbAutoException {
		regex = param.getAttribute("regex", null);
		response = param.getContent();
		if (!param.getName().equalsIgnoreCase("else") && regex == null) {
			throw new UmbAutoException("[WsResponseCase] value is null");
		}
		if (regex != null) {
			pattern = Pattern.compile(regex);
		}
		if (response == null) {
			throw new UmbAutoException("[WsResponseCase] content is null");
		}
	}

	public boolean matched(String message, CState state) {
		Matcher m = pattern.matcher(message);
		if (m.find()) {
			for (int i=1;i<=m.groupCount();i++) {
				state.pushSessionData("r" + i, m.group(i));
			}
			return true;
		} else {
			return false;
		}
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
