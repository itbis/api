package sms;

import java.util.Arrays;

public class message {

	private String msisdn,sender,msg,trxid;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMsg() {
                return msg;
        }
	public void setMsg(String msg) {
		this.msg = msg;
	}
        public String getTrxid() {
                return trxid;
        }
	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("***** Employee Details *****\n");
		sb.append("msisdn="+getMsisdn()+"\n");
		sb.append("sender="+getSender()+"\n");
		sb.append("message="+getMsg()+"\n");
		sb.append("trxid="+getTrxid());
		sb.append("\n*****************************");
		
		return sb.toString();
	}
}
