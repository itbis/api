package demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

	private static final String FILENAME = "/apps/service_canvas/api/monolithic/sms/fe/tomcat8080/webapps/ROOT/WEB-INF/classes/demo/message.txt";
	private static String sString = "";

	public static String readFile(String FILENAME) {

		try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				sString = sString + sCurrentLine;
			}


		} catch (IOException e) {
			e.printStackTrace();
		}

		return sString;

	}

}
