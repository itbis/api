package com.telkomsel.itvas.ws;

import org.apache.axis.encoding.DeserializerFactory;

import  org.apache.axis.Constants;
import java.util.Iterator;
import  java.util.Vector;

/**
 * *
 *
 */
public  class UMBServiceResponseDeserFactory implements DeserializerFactory {
         private  Vector mechanisms;

         public  UMBServiceResponseDeserFactory() {
         }
         public  javax.xml.rpc.encoding.Deserializer getDeserializerAs(String mechanismType) {
                  return  new UMBServiceResponseDeserializer();
         }
         public  Iterator getSupportedMechanismTypes() {
                  if  (mechanisms == null) {
                           mechanisms = new  Vector();
                           mechanisms.add(Constants.AXIS_SAX);
                  }
                  return  mechanisms.iterator();
         }
}