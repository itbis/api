package com.telkomsel.cp.util;

import java.io.*;
import java.util.*;
import java.text.*;

public class RequestUtility
{
	java.util.Date skr;
	SimpleDateFormat formatter;
	String tdate;

	public RequestUtility()
	{
	}

	public String GeneratePassword()
	{
		Random r = new Random();
		String id = String.valueOf(r.nextLong());
		id = id.substring(6, 12);

		return id;
	}

	public String GetTrxDate()
	{
		skr = new java.util.Date();
		formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		tdate = formatter.format(skr);
		return tdate;
	}

	public String GetCurrentTime()
	{
		skr = new java.util.Date();
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		tdate = formatter.format(skr);
		return tdate;
	}
}