package demo;

import javax.servlet.http.*;  
import javax.servlet.*;  
import java.io.*;  
public class DemoServlet extends HttpServlet{  

//GET Method
public void doGet(HttpServletRequest req,HttpServletResponse res)
throws ServletException,IOException
{
        res.setContentType("text/html");//setting the content type
        PrintWriter out=res.getWriter();//get the stream to write the data
	out.println("SMS Apps");
}

//POST Method
public void doPost(HttpServletRequest req,HttpServletResponse res)  
throws ServletException,IOException  
{
	/*
	String msisdn = req.getParameter("msisdn");
        String sender = req.getParameter("sender");
        String msg = req.getParameter("msg");
        String trxid = req.getParameter("trxid");

	res.setContentType("text/html");//setting the content type  
	*/

	PrintWriter out=res.getWriter();//get the stream to write the data  

	try {
		String Result = getBody(req);
		out.println(Result);
		//out.println(msisdn + "|" + sender + "|" + msg + "|" + trxid);
	}
	catch(Exception e) {
		out.println(e.getMessage());
	}  
}

//READ BODY
public static String getBody(HttpServletRequest request) throws IOException {

    String body = null;
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader bufferedReader = null;

    try {
        InputStream inputStream = request.getInputStream();
        if (inputStream != null) {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            char[] charBuffer = new char[128];
            int bytesRead = -1;
            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                stringBuilder.append(charBuffer, 0, bytesRead);
            }
        } else {
            stringBuilder.append("");
        }
    } catch (IOException ex) {
        throw ex;
    } finally {
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                throw ex;
            }
        }
    }

    body = stringBuilder.toString();
    return body;
}
}
