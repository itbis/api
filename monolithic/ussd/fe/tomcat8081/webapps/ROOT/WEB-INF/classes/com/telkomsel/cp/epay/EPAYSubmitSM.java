package com.telkomsel.cp.epay;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.*;
import java.net.*;
import com.telkomsel.cp.util.*;

public class EPAYSubmitSM
{
    public EPAYSubmitSM()
    {
    }

    public String submitSM(String from, String msisdn, String msg) throws IOException
    {
		String a = "";
		String url = "http://10.1.89.197:19002/cgi-bin/sendsms?username=tester&password=foobar&from=" + from + "&to=" + msisdn + "&text=" + msg;
		String s = send(url);
		if(s.equals("Sent."))
		{
			System.out.println(new java.util.Date()+" : INFO : SEND EPAY FROM "+from+" TO "+msisdn+" CONTENT: "+msg);
			a = "OK";
		}
		else
		{
			System.out.println(new java.util.Date()+" : ERROR : CANNOT SEND EPAY FROM "+from+" TO "+msisdn+" CONTENT: "+msg+" BECAUSE SMSC LINK IS DOWN");
			a = "NOK";
		}
        return a;
    }

    public static String send(String address) throws MalformedURLException, IOException, ClassCastException
    {
		String res = "";
		try
		{
			URL url = new URL(address);
			InputStream in1 = url.openStream();
			BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
			String line1 = "";
			while((line1 = br1.readLine()) != null)
			{
				res = res.concat(line1);
			}
			br1.close();
		}
		catch(Exception e)
		{
			res = "dodol";
		}

        return res;
    }
}
