package com.telkomsel.itvas.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import com.telkomsel.itvas.database.MysqlFacade;

public class Util {
	public static final int ROW_PER_PAGE = 50;
	
	private static final int HTTP_TIMEOUT = 5000;
	
	public static String getFilename(String absolutePath) {
		String separator = System.getProperty("file.separator");
		return absolutePath.substring(absolutePath.lastIndexOf(separator) + 1);
	}
	
	public static boolean isTselNumber(String msisdn) {
		if (msisdn == null || msisdn.trim().equals("")) {
			return false;
		}
		String myMsisdn = Util.formatMsisdn(msisdn);
		return (myMsisdn.startsWith("62811") || myMsisdn.startsWith("62812") || myMsisdn.startsWith("62813") || myMsisdn.startsWith("62852") || myMsisdn.startsWith("62853"));
	}
	
	private static boolean isNumeric(String str){
		try {
		Long.parseLong(str);
		return true;
		} catch (NumberFormatException nfe){
		return false;
		}
		}
	
	public static boolean isValidMsisdn(String msisdn) {
		msisdn = msisdn.trim();
		return (isNumeric(msisdn) && (msisdn.startsWith("8") || msisdn.startsWith("628") || msisdn.startsWith("0")));
	}

	public static String formatMsisdn(String msisdn) {
		if (msisdn == null) {
			return "";
		}
		msisdn = msisdn.trim();
		String retval;
		if (msisdn.startsWith("0")) {
			retval = "62" + msisdn.substring(1);
		} else {
			retval = msisdn;
		}
		return retval;
	}
	
	public static String HttpGet(String url) {
		long start = System.currentTimeMillis();
		url = url.replaceAll(" " , "+");
		String response = null;
		HttpClient http = new HttpClient();
		HttpMethod method = new GetMethod(url);
		method.getParams().setParameter("http.socket.timeout", HTTP_TIMEOUT);
		try {
			int statusCode = http.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK && statusCode != 202) {
				response = "HTTP_FAILED|" + statusCode;
			} else {
				response = method.getResponseBodyAsString().trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = "FAILED|" + e.getMessage();
		} finally {
			method.releaseConnection();
		}
		Logger.getLogger(Util.class).debug("HTTP-GET|" + url + "|Response|" + response + "|" + (System.currentTimeMillis() - start) + "ms");
		return response;
	}
	
	public static String HttpPost(String url, NameValuePair[] data) {		
		long start = System.currentTimeMillis();
		String response = null;
		HttpClient http = new HttpClient();
		HttpMethod method = new PostMethod(url);
		method.getParams().setParameter("http.socket.timeout", HTTP_TIMEOUT);
		try {
			int statusCode = http.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK && statusCode != 202) {
				response = "HTTP_FAILED|" + statusCode;
			} else {
				response = method.getResponseBodyAsString().trim();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = "FAILED|" + e.getMessage();
		} finally {
			method.releaseConnection();
		}
		Logger.getLogger(Util.class).debug("HTTP-POST|" + url + "|Response|" + response + "|" + (System.currentTimeMillis() - start) + "ms");
		return response;
	}
	
	public static String formatMysqlDate(String dt) {
		String[] p = dt.split("-");
		if (p.length != 3) {
			return dt;
		}
		return p[2] + "-" + p[1] + "-" + p[0];
	}

	public static void writeEvent(String event) {
		Connection conn = null;
		try {
			conn = MysqlFacade.getConnection();
			String q = "INSERT INTO system_event (ts, event) VALUES (NOW(), ?)";
			QueryRunner qr = new QueryRunner();
			qr.update(conn, q, event);
		} catch (Exception e) {
			Logger.getLogger(Util.class).error("Err in writeEvent() : " + e.getMessage(), e);
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}
	
	public static String nullSafeString(Object obj) {
		if (obj == null) {
			return "";
		} else {
			return obj.toString();
		}
	}
	
	public static String getSecurityCode(int n) {
	    char[] pw = new char[n];
	    int c  = 'A';
	    int  r1 = 0;
	    for (int i=0; i < n; i++)
	    {
	      r1 = (int)(Math.random() * 2);
	      switch(r1) {
	        case 0: c = '0' +  (int)(Math.random() * 10); break;
	        case 1: c = 'A' +  (int)(Math.random() * 26); break;
	      }
	      pw[i] = (char)c;
	    }
	    return new String(pw);
//		return "1";
	  }

	public static void sendSMS(String msisdn, String message) {
		System.out.println("dest : " + msisdn);
		System.out.println("message : " + message);
	}
	
	public static int getNbMessage(String message) {
		int current = message.length();
		if (current <= 160) {
        	return 1;
        } else if (current <= 306) {
        	return 2;
        } else if (current <= 459) {
        	return 3;
        } else if (current <= 612) {
        	return 4;
        } else if (current <= 765) {
        	return 5;
        } else {
        	return 5;
        }
	}
	
	public static int getAmount(String appsid) {
		try {
			String stramount = appsid.substring(9);
			return Integer.parseInt(stramount);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return 250;
		}
	}
}
