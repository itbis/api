package com.telkomsel.cp.util;

import javax.naming.*;
import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public class DB
{
	private Context ctx;
	private DataSource ds;
    private Connection dbConn;
    private Statement stmt;
    private ResultSet rs;
	private String query;
	private PreparedStatement pstmt;
	private boolean b;
	private Vector v;
	private String smsid;
	private int id;
	private String trxid;
	private String keyword;

	java.util.Date skr;
	SimpleDateFormat formatter;
	String tdate;

    public DB()
    {
		try
    	{
      		ctx = new InitialContext();
      		if(ctx == null)
          		throw new Exception("Boom - No Context");

      		ds = (DataSource)ctx.lookup("java:comp/env/jdbc/umbKacrut");
	    }
	    catch(Exception e)
	    {
      		e.printStackTrace();
    	}
    }

    public Connection createDBConnection() throws IOException, SQLException
    {
		if(ds != null)
		{
	   		dbConn = ds.getConnection();
        }
		else
		{
			System.out.println(" ds null");
		}
        return dbConn;
    }

    public void closeDBConnection()
    {
        try
        {
            if(dbConn != null)
			{
				dbConn.close();
				dbConn = null;
				System.out.println(" Close CP DB Connection");
            }
        }
        catch(Exception exception)
        {
            System.out.println("!!!!!! [ ERROR ] Close CP DB Connection Exception: " + exception.getMessage());
        }
    }

    public ResultSet doQuery(String s) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DB Connection Insert : "+dbConn);
        stmt = dbConn.createStatement();
        rs = stmt.executeQuery(s);

        return rs;
    }

    public void doUpdate(String s) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DB Connection Update : "+dbConn);
        stmt = dbConn.createStatement();
        stmt.executeUpdate(s);
        stmt.close();
    }

    public String isValid(String ky, String ad, String cn) {
	String hasil = "0";
	try {
		if (dbConn == null || dbConn.isClosed()) 
			createDBConnection();
		String a = "Select response from cp_directReg Where keyword = ? and adn = ? and cpname = ?";
		PreparedStatement ps = dbConn.prepareStatement(a);
		ps.setString(1,ky);
		ps.setString(2,ad);
		ps.setString(3,cn);

		ResultSet rs = ps.executeQuery();

		if(rs.next()){
			hasil = rs.getString("response");
		} 
		
		rs.close();
		ps.close();

	}
	catch(Exception e) {
		e.printStackTrace(System.out);
		hasil = e.getMessage();
	}

	return hasil;
    }

   public String isValidResp(String ky, String ad, String cn) {
        String hasil = "0";
        try {
                if (dbConn == null || dbConn.isClosed())
                        createDBConnection();
                String a = "Select response from cp_directReg Where keyword = ? and adn = ? and cpname = ?";
                PreparedStatement ps = dbConn.prepareStatement(a);
                ps.setString(1,ky);
                ps.setString(2,ad);
                ps.setString(3,cn);

                ResultSet rs = ps.executeQuery();

                if(rs.next()){
                        //bo = true;
                        hasil = rs.getString("response");
                }

                rs.close();
                ps.close();

        }
        catch(Exception e) {
                e.printStackTrace(System.out);
                hasil = "0";
        }

        return hasil;
    }

	public Vector selectContentProviderProfile(String app, String pwd, String sid)
    {
		v = new Vector();
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT cp_name, contentid, charge_type, dID, dPWD, ip_smsgw, port_smsgw, id_smsgw, pwd_smsgw, bsc, adn, fr FROM cp_indirect WHERE appsid = ? AND pwd = ? AND sid = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, app);
			pstmt.setString(2, pwd);
			pstmt.setString(3, sid);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				v.add(0, rs.getString("cp_name"));
				v.add(1, rs.getString("contentid"));
				v.add(2, rs.getString("charge_type"));
				v.add(3, rs.getString("dID"));
				v.add(4, rs.getString("dPWD"));
				v.add(5, rs.getString("ip_smsgw"));
				v.add(6, rs.getString("port_smsgw"));
				v.add(7, rs.getString("id_smsgw"));
				v.add(8, rs.getString("pwd_smsgw"));
				v.add(9, rs.getString("bsc"));
				v.add(10, rs.getString("adn"));
				v.add(11, rs.getString("fr"));
			}
			else
			{
				v = null;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			v = null;
		}

		return v;
    }

	public boolean selectCheckTrxIDIsItAlreadyBeenUsedOrNot(String charge_type, String trid, String cp_name)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			if(charge_type.equals("MT"))
				query = "SELECT transactionid FROM cp_report WHERE transactionid = ? AND cp_name = ?";
			else
				query = "SELECT transactionid FROM cp_reportMO WHERE transactionid = ? AND cp_name = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, trid);
			pstmt.setString(2, cp_name);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				b = false;
			}
			else
			{
				b = true;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			b = false;
		}

		return b;
    }

	public boolean selectCheckTrxIDIsItAlreadyReserved(String msisdn, String trid)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT id FROM reserve_tid WHERE msisdn = ? AND trx_id = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, trid);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				b = true;
			}
			else
			{
				b = false;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			b = false;
		}

		return b;
    }

	public boolean selectCheckAQSSubscriber(String msisdn)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT msisdn FROM aqs_subs WHERE msisdn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				b = true;
			}
			else
			{
				b = false;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			b = false;
		}

		return b;
    }

	public void insertCPReport(String charge_type, String cp_name, String msisdn, String smsid, String contentid, String oa, String trid, String status)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			if(charge_type.equals("MT"))
				query = "INSERT INTO cp_report VALUES(?,?,?,?,?,now(),'N','',?,?)";
			else
				query = "INSERT INTO cp_reportMO VALUES(?,?,?,?,?,now(),'N','',?,?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, msisdn);
			pstmt.setString(3, smsid);
			pstmt.setString(4, contentid);
			pstmt.setString(5, oa);
			pstmt.setString(6, trid);
			pstmt.setString(7, status);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void insertAQSSubscriber(String msisdn, String card_type)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO aqs_subs VALUES(?,?,NOW())";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, card_type);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public String selectKeywordFromContentReq(String cp_name, String adn, String k)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT keyword FROM content_req WHERE cp_name = ? AND adn = ? and keyword = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, adn);
			pstmt.setString(3, k);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				keyword = rs.getString("keyword");
			}
			else
			{
				keyword = "dodol";
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			keyword = "dodol";
		}

		return keyword;
    }

	public String selectKeywordFromCPReqDirect(String cp_name, String adn, String keyword)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT keyword FROM cp_req_direct WHERE keyword = ? AND cp_name = ? AND adn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, keyword);
			pstmt.setString(2, cp_name);
			pstmt.setString(3, adn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				keyword = rs.getString("keyword");
			}
			else
			{
				query = "SELECT keyword FROM cp_req_direct WHERE cp_name = ? AND adn = ?";
				pstmt = dbConn.prepareStatement(query);
				pstmt.clearParameters();
				pstmt.setString(1, cp_name);
				pstmt.setString(2, adn);
				rs = pstmt.executeQuery();
				if(rs.next())
					keyword = rs.getString("keyword");
				else
					keyword = "dodol";
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			keyword = "dodol";
		}

		return keyword;
    }

	public String selectKeywordFromCPReqIndirect(String cp_name, String adn, String keyword)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT keyword FROM cp_req_indirect WHERE keyword = ? AND cp_name = ? AND adn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, keyword);
			pstmt.setString(2, cp_name);
			pstmt.setString(3, adn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				keyword = rs.getString("keyword");
			}
			else
			{
				query = "SELECT keyword FROM cp_req_indirect WHERE cp_name = ? AND adn = ?";
				pstmt = dbConn.prepareStatement(query);
				pstmt.clearParameters();
				pstmt.setString(1, cp_name);
				pstmt.setString(2, adn);
				rs = pstmt.executeQuery();
				if(rs.next())
					keyword = rs.getString("keyword");
				else
					keyword = "dodol";
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			keyword = "dodol";
		}

		return keyword;
    }

	public Vector selectHostFileFromContentReq(String tag, String cp_name, String a)
    {
		v = new Vector();

		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT host, file FROM content_req WHERE keyword = ? AND cp_name = ? and adn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, tag);
			pstmt.setString(2, cp_name);
			pstmt.setString(3, a);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				v.add(0, rs.getString("host"));
				v.add(1, rs.getString("file"));
			}
			else
			{
				v = null;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			v = null;
		}

		return v;
    }

	public Vector selectHostFileFromCPReqDirect(String cp_name, String tag, String adn)
    {
		v = new Vector();
		
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT host, file FROM cp_req_direct WHERE cp_name = ? AND keyword = ? AND adn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, tag);
			pstmt.setString(3, adn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				v.add(0, rs.getString("host"));
				v.add(1, rs.getString("file"));
			}
			else
			{
				v = null;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			v = null;
		}

		return v;
    }

	public Vector selectHostFileFromCPReqIndirect(String cp_name, String tag, String adn)
    {
		v = new Vector();
		
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT host, file FROM cp_req_indirect WHERE cp_name = ? AND keyword = ? AND adn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, tag);
			pstmt.setString(3, adn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				v.add(0, rs.getString("host"));
				v.add(1, rs.getString("file"));
			}
			else
			{
				v = null;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			v = null;
		}

		return v;
    }

	public Vector selectContentProviderProfileFromCPDirect(String cp_name, String tag, String sid)
    {
		v = new Vector();
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT contentid, charge_type, dID, dPWD, ip_smsgw, port_smsgw, id_smsgw, pwd_smsgw, bsc, fr FROM cp_direct WHERE cp_name = ? AND keyword = ? AND sid = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, cp_name);
			pstmt.setString(2, tag);
			pstmt.setString(3, sid);
			System.out.println("embeer :"+cp_name+":"+tag+":"+sid);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				v.add(0, rs.getString("contentid"));
				v.add(1, rs.getString("charge_type"));
				v.add(2, rs.getString("dID"));
				v.add(3, rs.getString("dPWD"));
				v.add(4, rs.getString("ip_smsgw"));
				v.add(5, rs.getString("port_smsgw"));
				v.add(6, rs.getString("id_smsgw"));
				v.add(7, rs.getString("pwd_smsgw"));
				v.add(8, rs.getString("bsc"));
				v.add(9, rs.getString("fr"));
			}
			else
			{
				v = null;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			v = null;
		}

		return v;
    }

	public String selectGetTrxID(String cp_name)
    {
		try
		{
			skr = new java.util.Date();
			formatter = new SimpleDateFormat("ddMMyyHHmmssS");
			tdate = formatter.format(skr);

			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "INSERT INTO tid VALUES ('','" + cp_name + "')";
			stmt = dbConn.createStatement();
			stmt.executeUpdate(query);
			rs = stmt.getGeneratedKeys();
			if(rs.next())
			{
				id = rs.getInt(1);
				trxid = tdate + "" + id;
			}
			else
				trxid = "dodol";
			stmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			trxid = "dodol";
		}

		return trxid;
    }

	public String selectGetSMSID(String cp_name)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "INSERT INTO smsid(smsid, cpid) VALUES('','" + cp_name + "')";
			stmt = dbConn.createStatement();
			stmt.executeUpdate(query);
			rs = stmt.getGeneratedKeys();
			if(rs.next())
			{
				id = rs.getInt(1);
				smsid = "" + id;
			}
			else
				smsid = "dodol";
			stmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
			smsid = "dodol";
		}

		return smsid;
    }

	public boolean isTerdaftarPadaProgramSimpatiAwalTahun(String msisdn)
    {
		boolean boo = true;
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT id FROM msisdn_spat WHERE msisdn = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			rs = pstmt.executeQuery();
			if(rs.next())
			{
				boo = true;
			}
			else
			{
				boo = false;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			System.out.println("Errormsisdnspat: " + e.getMessage());
			closeDBConnection();
			boo = true;
		}

		return boo;
    }

	public void insertMMS6789(String msisdn, String text, String image_name, InputStream image)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO mms6789 VALUES('', NOW(), CURDATE(), ?, 'N', ?, ?, ?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, text);
			pstmt.setString(3, image_name);
			pstmt.setBinaryStream(4, image, image.available());
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void InsertMMS_RECEIVER_DATA(String ADN, String msisdn, String text, String image_name, InputStream image)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO mms_receiver_data VALUES('', NOW(), CURDATE(), ?, ?, 'N', ?, ?, ?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, ADN);
			pstmt.setString(2, msisdn);
			pstmt.setString(3, text);
			pstmt.setString(4, image_name);
			pstmt.setBinaryStream(5, image, image.available());
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void InsertMMS_RECEIVER_LOG(String ADN, String msisdn, String text, String image_name, String image)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO mms_receiver_log VALUES('', NOW(), CURDATE(), ?, ?, ?, ?, ?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, ADN);
			pstmt.setString(2, msisdn);
			pstmt.setString(3, text);
			pstmt.setString(4, image_name);
			pstmt.setString(5, image);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public ResultSet getKey(String sql) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

		System.out.println("CP DB Connection GetKey : "+dbConn);
		stmt = dbConn.createStatement();
		stmt.executeUpdate(sql);
		rs = stmt.getGeneratedKeys();

        return rs;
    }

	public boolean isReachLimitForOneNumberPerDay(String anum, String bnum)
    {
		boolean boo = false;
		int ioo = 0;
		String soo = "";
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT count(id) FROM cc_limit WHERE anum = ? AND bnum = ? AND req_date = CURDATE()";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, anum);
			pstmt.setString(2, bnum);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				soo = rs.getString(1);
			}

			ioo = Integer.parseInt(soo);
			if(ioo >= 5)
			{
				boo = true;
			}
			else
			{
				boo = false;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			System.out.println("isReachLimitForOneNumberPerDay: " + e.getMessage());
			closeDBConnection();
			boo = true;
		}

		return boo;
    }

	public boolean isReachLimitPerDay(String anum)
    {
		boolean boo = false;
		int ioo = 0;
		String soo = "";
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();

			query = "SELECT count(id) FROM cc_limit WHERE anum = ? AND req_date = CURDATE()";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, anum);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				soo = rs.getString(1);
			}

			ioo = Integer.parseInt(soo);
			if(ioo >= 10)
			{
				boo = true;
			}
			else
			{
				boo = false;
			}
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			System.out.println("isReachLimitPerDay: " + e.getMessage());
			closeDBConnection();
			boo = true;
		}

		return boo;
    }

	public void InsertCCLimit(String anum, String bnum)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO cc_limit VALUES('', CURDATE(), ?, ?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, anum);
			pstmt.setString(2, bnum);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void ReserveTID(String msisdn, String trxid)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO reserve_tid VALUES('', CURDATE(), ?, ?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, trxid);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void InsertMO(String msisdn, String cpname, String content_id, String msg, String trx_id, String laid, String mo_mt, String type, String status, String msisdn_type, String account)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO MO VALUES('',NOW(),?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, cpname);
			pstmt.setString(3, content_id);
			pstmt.setString(4, msg);
			pstmt.setString(5, trx_id);
			pstmt.setString(6, laid);
			pstmt.setString(7, mo_mt);
			pstmt.setString(8, type);
			pstmt.setString(9, status);
			pstmt.setString(10, msisdn_type);
			pstmt.setString(11, account);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void InsertMT(String msisdn, String cpname, String content_id, String msg, String trx_id, String laid, String mo_mt, String type, String status_submit, String status_sms, String msisdn_type, String account)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO MT VALUES('',NOW(),?,?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, cpname);
			pstmt.setString(3, content_id);
			pstmt.setString(4, msg);
			pstmt.setString(5, trx_id);
			pstmt.setString(6, laid);
			pstmt.setString(7, mo_mt);
			pstmt.setString(8, type);
			pstmt.setString(9, status_submit);
			pstmt.setString(10, status_sms);
			pstmt.setString(11, msisdn_type);
			pstmt.setString(12, account);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void InsertChargingLog(String msisdn, String cpname, String content_id, String trx_id, String tcg_resp, String tcg_trxid)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "INSERT INTO charging_log VALUES('',CURDATE(),NOW(),?,?,?,?,?,?)";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, cpname);
			pstmt.setString(3, content_id);
			pstmt.setString(4, trx_id);
			pstmt.setString(5, tcg_resp);
			pstmt.setString(6, tcg_trxid);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void RegisterThisDLR(String charge_type, String smsid)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			if(charge_type.equals("MT"))
				query = "UPDATE cp_report SET status = 'D', deliver_time = now() WHERE smsid = ?";
			else
				query = "UPDATE cp_reportMO SET status = 'D', deliver_time = now() WHERE smsid = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, smsid);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }

	public void DeleteThisTrxID(String msisdn, String trxid)
    {
		try
		{
			if(dbConn == null || dbConn.isClosed())
				createDBConnection();
			
			query = "DELETE FROM reserve_tid WHERE msisdn = ? AND trx_id = ?";
			pstmt = dbConn.prepareStatement(query);
			pstmt.clearParameters();
			pstmt.setString(1, msisdn);
			pstmt.setString(2, trxid);
			pstmt.executeUpdate();
			pstmt.close();
			closeDBConnection();
		}
		catch(Exception e)
		{
			closeDBConnection();
		}
    }
}
