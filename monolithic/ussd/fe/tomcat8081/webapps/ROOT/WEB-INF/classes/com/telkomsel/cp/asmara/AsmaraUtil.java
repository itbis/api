package com.telkomsel.cp.asmara;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.telkomsel.cp.util.HttpGet;

public class AsmaraUtil {
	/** Seq number stuff */
	private static int sequenceNumber;
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	
	public static boolean validateCP(Connection conn, String cpid, String pwd) throws SQLException {
		String q = "SELECT cpid FROM asm_parameter WHERE cpid=? AND pwd=?";
		PreparedStatement ps = conn.prepareStatement(q);
		ps.setString(1, cpid);
		ps.setString(2, pwd);
		
		boolean retval = false;
		ResultSet rs = ps.executeQuery();
		retval = rs.next();
		return retval;
	}
	
	public static String formatMsisdn(String msisdn) {
		msisdn = msisdn.trim();
		String retval;
		if (msisdn.startsWith("+62")) {
			retval = msisdn.substring(1);
		} else if (msisdn.startsWith("0")) {
			retval = "62" + msisdn.substring(1);
		} else if (!msisdn.startsWith("62")) {
			retval = "62" + msisdn;
		} else {
			retval = msisdn;
		}
		return retval;
	}
	
	public static String simpleXmlRetrieve(String content, String tag) throws Exception {
		int idxStart = content.indexOf("<" + tag + ">");
		if (idxStart == -1) {
			throw new Exception("Start Tag not found");
		}
		
		int idxEnd = content.indexOf("</" + tag + ">");
		if (idxEnd == -1) {
			throw new Exception("End Tag not found");
		}
		return content.substring(idxStart + tag.length() + 2, idxEnd).trim();
	}
	
	public static String generateTrxId(String msisdn) {
		int len = msisdn.length();
		if (len < 2) {
			return sdf.format(new Date()) + getSequenceNumber();
		} else {
			return sdf.format(new Date()) + getSequenceNumber()
					+ msisdn.substring(len - 2, len);
		}
	}
	
	public static synchronized int getSequenceNumber() {
		int tmp = sequenceNumber;
		sequenceNumber++;
		if (sequenceNumber >= 10000) {
			sequenceNumber = 1000;
		}
		return tmp;
	}
	
	public static boolean charge(String cgHost, String cgPath, String msisdn, String cpId, String cgId, String cgPwd) {
		HttpGet HTTP = new HttpGet();
		String url = cgPath;
		url = url.replaceAll("\\{cp_id\\}", cpId);
		url = url.replaceAll("\\{cg_pwd\\}", cgPwd);
		url = url.replaceAll("\\{cg_id\\}", cgId);
		url = url.replaceAll("\\{msisdn\\}", msisdn);
		
		String response = HTTP.Get(cgHost, url, 10000);
		System.out.println("CHARGE : " + cgHost + url + ", response : " + response);
		String[] p = response.split(" ");
		if (p.length < 3) {
			return false;
		}
		return (p[2].equals("2") || p[2].equals("16") || p[2].equals("3:0"));
	}
}
