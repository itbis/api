package com.telkomsel.itvas.umbx.actions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;
import com.telkomsel.itvas.util.Util;

import net.n3.nanoxml.IXMLElement;

public  class HttpGetInputAction extends Action {
	private String uri;
	private String content;
	private String var;
	
	public HttpGetInputAction(IXMLElement element) throws UmbAutoException {
		uri = element.getAttribute("uri", null);
		var = element.getAttribute("var", null);
		content = element.getContent();
		if (uri == null) {
			throw new UmbAutoException("[HttpGetInputAction] attr not complete : uri");
		}
		if (var == null) {
			throw new UmbAutoException("[HttpGetInputAction] attr not complete : var");
		}
		if (content == null) {
			throw new UmbAutoException("[HttpGetInputAction] attr not complete : content");
		}
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		String myUri = uri;
		state.pushSessionData("httpinput", command);
		myUri = formatState(myUri, state);
		log.info("URL : " + myUri);
		String strresponse = Util.HttpGet(myUri);
		log.info("response http : " + strresponse);
		state.pushSessionData(var, strresponse);
		
		return 1;
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		String strFirst = (first) ? "Yes" : "No";
		return formatResult("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
		+ "<data>"+content+"</data>").trim();
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
}


