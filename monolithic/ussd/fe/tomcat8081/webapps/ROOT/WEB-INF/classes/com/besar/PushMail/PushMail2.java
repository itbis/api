/*
 * PushMail.java
 *
 * Created on 18 Mei 2007, 9:59
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author besarwic
 */
package com.besar.PushMail;

import com.besar.PushMail.EasySSLProtocolSocketFactory;
import com.besar.PushMail.EasyX509TrustManager;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.HttpState;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.params.HttpClientParams;
import java.util.*;
import java.net.*; 
import java.security.*;


public class PushMail2 {
    
    /** Creates a new instance of PushMail */
    public boolean SendSMS(String msisdn,String message,String urlSMSGateway)
    {
    	boolean status = false;
    	HttpClient httpclient = new HttpClient();
    	httpclient.setConnectionTimeout(5000);
    	httpclient.setTimeout(5000);
    	
    	/*
    	 * "http://10.1.89.197:19002/cgi-b
in/sendsms?username=tester&password=foobar&from=6789&to=" + msisdn + "&text= " + URLEncoder.encode("Rute Jakarta-Surabaya Jam 05.00 GA123 1 seat Ekonomi. Tel p 0811917671 Kode Booking Q45A1 Kode Bayar 65743 Rp. 1.500.000", "UTF-8"));

    	 * */
    	String msg="";
    	try
    	{
    		msg = URLEncoder.encode(message,"UTF-8");
    	}
    	catch(Exception ex)
    	{
    		status = false;
    		return status;
    	}
    	String url = urlSMSGateway+"&to="+msisdn+"&text="+msg;
    	GetMethod method = new GetMethod(url);
    	
    	try
    	{
    		httpclient.executeMethod(method);
    		status = true;
    	}
    	catch(Exception ex)
    	{
    		status = false;
    	}
    	return status;
    }
    public String[] SendRequest(String msisdn, String msg, String url,String host, int port, String username, String password)
    {
        String[] out= new String[2];
		//ignore certificate
	PostMethod httppost = null;
	try {
/*
        Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 8084);
        Protocol.registerProtocol("https", easyhttps);
*/


        HttpClient httpclient = new HttpClient();
  httpclient.setConnectionTimeout(5000);
  httpclient.setTimeout(5000);

		//Authenticator.setDefault(new MyAuthenticator());
		//ni kaga bisa2
//		HttpClientParams params = httpclient.getParams();
//		params.setAuthenticationPreemptive(true);
       
/*
	List authPrefs = new ArrayList(3);
	authPrefs.add(AuthPolicy.BASIC);
	authPrefs.add(AuthPolicy.DIGEST);
	authPrefs.add(AuthPolicy.NTLM);
	params.setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, 
	authPrefs);
*/
	//password = URLEncoder.encode(password, "UTF-8");
	System.out.println("pushmail url:"+url+" username:"+username+" pass:"+password+" msg:"+msg+ " msisdn:"+msisdn);
	Credentials credentials = new UsernamePasswordCredentials(username,password);
	AuthScope authScope = new AuthScope("198.165.96.34",8084, "realm");
	HttpState state = httpclient.getState();
	//state.setAuthenticationPreemptive(true); 
	//state.setCredentials("","https://198.165.96.34:8084",credentials);
	state.setCredentials(AuthScope.ANY,credentials);
	//state.setCredentials(authScope,credentials);
	//state.setCredentials(null,host,credentials);
//	httpclient.getState().setCredentials(new AuthScope(host,port),new UsernamePasswordCredentials(username,password));
        

	//out[0] ="asasd";
	//return out;
	
	//GetMethod httpget = new GetMethod("https://10.2.224.101:9443/index.jsp"); 
        httppost = new PostMethod(url);
       //GetMethod httppost = new GetMethod(urlstr);
	httppost.setHttp11(false);
	httppost.setDoAuthentication( true );

            NameValuePair[] data = {
          new NameValuePair("msisdn", msisdn),
          new NameValuePair("msg", msg)
            };
            httppost.setRequestBody(data);
            httpclient.executeMethod(httppost);

            out[0] = httppost.getResponseBodyAsString();
            out[1] = httppost.getStatusLine().toString();
          } 
          catch(Exception ex)
          {
              out[0]=ex.getMessage();
	      out[1]="<br>no status";
          }
          finally {
            httppost.releaseConnection();
		}
	return out;

     }
          
}
