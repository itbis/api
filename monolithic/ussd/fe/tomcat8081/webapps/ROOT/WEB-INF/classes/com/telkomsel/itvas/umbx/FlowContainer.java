package com.telkomsel.itvas.umbx;

import java.util.Hashtable;

import org.apache.log4j.Logger;

public class FlowContainer {
	private Hashtable<String, Flow> container = new Hashtable<String, Flow>();
	private static FlowContainer instance;
	private Logger log = Logger.getLogger(this.getClass());
	
	private FlowContainer() {}
	
	public static FlowContainer getInstance() {
		if (instance == null) {
			instance = new FlowContainer();
		}
		return instance;
	}
	
	public Flow get(String flowId) throws Exception {
		Flow retval = null;
		if (container.containsKey(flowId)) {
			retval = container.get(flowId);
		} else {
			try {
				retval = new Flow(flowId);
				container.put(flowId, retval);
			} catch (Exception e) {
				Throwable reason = e.getCause();
				if (reason == null) {
					log.error("[XMLUMB] Err in processing : " + flowId + ", reason : "+ e.getMessage(), e);
					e.printStackTrace();
				} else {
					log.error("[XMLUMB] Err in processing : " + flowId + ", reason : "+ reason.getMessage(), reason);
				}
				throw e;
			}
		}
		return retval;
	}
	
	public Flow get(String xml, boolean refresh) throws Exception {
		if (refresh) {
			container.remove(xml);
		}
		return get(xml);
	}
}
