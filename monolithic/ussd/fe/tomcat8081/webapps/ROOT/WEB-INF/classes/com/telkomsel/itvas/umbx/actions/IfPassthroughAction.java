package com.telkomsel.itvas.umbx.actions;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.telkomsel.itvas.umbx.CState;
import com.telkomsel.itvas.umbx.UmbAutoException;
import com.telkomsel.itvas.util.Util;

import net.n3.nanoxml.IXMLElement;

public  class IfPassthroughAction extends Action {
	private String operator;
	private String a;
	private String b;
	private String falseResponse;
	private String falseRoute;
	private String id;
	
	public IfPassthroughAction(IXMLElement element) throws UmbAutoException {
		operator = element.getAttribute("operator", null);
		if (operator == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : operator");
		}
		
		a = element.getAttribute("a", null);
		if (a == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : a");
		}
		
		b = element.getAttribute("b", null);
		if (b == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : b");
		}
		
		id = element.getAttribute("id", null);
		if (id == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : id");
		}
		
		falseRoute = element.getAttribute("falseRoute", null);
		if (falseRoute == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : falseRoute");
		}
		
		falseResponse = element.getAttribute("falseResponse", null);
		if (falseResponse == null) {
			throw new UmbAutoException("[IfPassthroughAction] attr not complete : falseResponse");
		}
	}

	@Override
	public int postProcess(CState state, String command, boolean first) {
		int i = 99;
		try {
			i = Integer.parseInt(state.getSession("IfPassthroughAction" + id));
		} catch (NumberFormatException e) {
			log.error(e);
		}
		return i;
	}

	@Override
	public String preProcess(CState state, String command, boolean first) {
		String stra = formatState(a, state);
		String strb = formatState(b, state);
		boolean result = false;
		if (operator.equals("=")) {
			result = stra.trim().equals(strb.trim());
		}
		if (result) {
			state.popSessionData("IfPassthroughAction" + id);
			return "PASSTHROUGH";
		} else {
			state.pushSessionData("IfPassthroughAction" + id, falseRoute);
			String strFirst = (first) ? "Yes" : "No";
			return formatResult(formatState("<type>Content</type>"+ NEWLINE +"<first>"+strFirst+"</first>"+ NEWLINE  
					+ "<data>"+falseResponse+"</data>", state)).trim();
		}
	}
}


