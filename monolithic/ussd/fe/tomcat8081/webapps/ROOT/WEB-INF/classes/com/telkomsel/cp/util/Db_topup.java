package com.telkomsel.cp.util;

import javax.naming.*;
import javax.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.Math;

public class Db_topup
{
	private Context ctx;
	private DataSource ds;
    private Connection dbConn;
    private Statement stmt;
    private ResultSet rs;

    public Db_topup()
    {
		try
    	{
      		ctx = new InitialContext();
      		if(ctx == null)
          		throw new Exception("Boom - No Context");

      		ds = (DataSource)ctx.lookup("java:comp/env/jdbc/topup_halo");
	    }
	    catch(Exception e)
	    {
      		e.printStackTrace();
    	}
    }

    public Connection createDBConnection() throws IOException, SQLException
    {
		if(ds != null)
		{
	   		dbConn = ds.getConnection();
        }
		else
		{
			System.out.println(" ds null");
		}
        return dbConn;
    }

    public void closeDBConnection()
    {
        try
        {
            if(dbConn != null)
			{
				dbConn.close();
				dbConn = null;
				System.out.println(" Close CP DB Connection");
            }
        }
        catch(Exception exception)
        {
            System.out.println("!!!!!! [ ERROR ] Close CP DB Connection Exception: " + exception.getMessage());
        }
    }

    public ResultSet doQuery(String s) throws IOException, SQLException
    {
		//ResultSet rs = null;
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DB Connection Insert : "+dbConn);
        stmt = dbConn.createStatement();
        rs = stmt.executeQuery(s);

		//if(dbConn != null)
		//	closeDBConnection();

        return rs;
    }

    public void doUpdate(String s) throws IOException, SQLException
    {
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

       	System.out.println("CP DB Connection Update : "+dbConn);
        stmt = dbConn.createStatement();
        stmt.executeUpdate(s);
        stmt.close();

		//if(dbConn != null)
		//	closeDBConnection();
    }

    public ResultSet getKey(String sql) throws IOException, SQLException
    {
		//ResultSet rs = null;
    	if(dbConn == null || dbConn.isClosed())
    	   	createDBConnection();

		System.out.println("CP DB Connection GetKey : "+dbConn);
		stmt = dbConn.createStatement();
		stmt.executeUpdate(sql);
		rs = stmt.getGeneratedKeys();

		//if(dbConn != null)
		//	closeDBConnection();

        return rs;
    }

    public String randomNy() {
	String hasil = null;
	int array[];

	array = new int[] {0,0,0,0};

	Random rnd = new Random(System.currentTimeMillis());
	int n = 1;

	 //generate the first number

       int i = Math.abs(rnd.nextInt() % 56);

     //If number is 0 keep generate numbers until it is not 0; there is no 0 in Lottery

     while(i==0)
       i = Math.abs(rnd.nextInt() % 56);

       //add number to array
       array[0]= i;

       //execute loop to slow down system
     for(int j=0;j<1000000; j++)
       n = n +1/n;

       //generate the second unique number
       int a = Math.abs(rnd.nextInt() % 56);

       //make sure the number is unique
       while((a==0)||(a== array[0]))
       a = Math.abs(rnd.nextInt() % 56);

       //add number to array
       array[1]= a;

     //Keep system busy for time to pass. If not enough time passed since generating

     //the first number, the second number will be the same.We want it different
       for(int j=0;j<1000000; j++)
        n = n +1/n;

       //generate the 3rd unique number
       int b = Math.abs(rnd.nextInt() % 56);

      //make sure the number is unique
      while((b==0)||(b== array[0])||(b==array[1]))
       b = Math.abs(rnd.nextInt() % 56);

       //add number to array
       array[2]= b;

       for(int j=0;j<1000000; j++)
         n = n +1/n;

	//generate the 4th unique number
       int c = Math.abs(rnd.nextInt() % 56);

       //make sure the number is unique
       while((c==0)||(c== array[0])||(c==array[1])||(c==array[2]))
       c = Math.abs(rnd.nextInt() % 56);

       //add number to array
       array[3]= c;

       for(int j=0;j<1000000; j++)
          n = n +1/n;

	hasil = Integer.toString(array[2])+Integer.toString(array[3]);

	return hasil;

    }		

    public String cek_user(String username, String password, String msisdn, String msg){
	
	String hasil = "0";
	String sql = null;
        try {
		if(dbConn == null || dbConn.isClosed())
			createDBConnection();	

		sql = "Select count(*) as jml From account Where username = ? and password = ?";
		PreparedStatement pstmt = dbConn.prepareStatement(sql);
		pstmt.setString(1,username); 
		pstmt.setString(2,password);

		ResultSet rs = pstmt.executeQuery();
		if(rs.next()) {
			hasil = rs.getString("jml");
		}

		rs.close();
		pstmt.close();

		if (hasil.equals("1")) {

			if (msg.equals("1")) {
				//cek apakah sudah ada msisdnnya
				sql = "Select count(*) as jml From subs Where msisdn = ?";
                		pstmt = dbConn.prepareStatement(sql);
                		pstmt.setString(1,msisdn);
                		rs = pstmt.executeQuery();
                		if(rs.next()) {
                        		hasil = rs.getString("jml");
                		}

                		rs.close();
                		pstmt.close();

				if (hasil.equals("0")) {

					double randNumber = Math.random() * 89999 + 10000;
					randNumber = Math.floor(randNumber);
					String random = Double.toString(randNumber);
					random = random.substring(0,4);
					sql = "insert into subs values(?,'B',?,'A',now(),0,?) ";
					pstmt = dbConn.prepareStatement(sql);
					pstmt.setString(1,msisdn);
					pstmt.setString(2,random);
					pstmt.setString(3,username);
					pstmt.executeUpdate();
					pstmt.close(); 
					hasil = "1";
				}
				else {
					hasil = "2";
				}
			}
			else {
				//cek apakah sudah ada msisdnnya
                                sql = "Select count(*) as jml From subs Where msisdn = ?";
                                pstmt = dbConn.prepareStatement(sql);
                                pstmt.setString(1,msisdn);
                                rs = pstmt.executeQuery();
                                if(rs.next()) {
                                        hasil = rs.getString("jml");
                                }

                                rs.close();
                                pstmt.close();

                                if (hasil.equals("1")) {
					sql = "Delete from subs Where msisdn = ?";
                                	pstmt = dbConn.prepareStatement(sql);
                                	pstmt.setString(1,msisdn);
                                	pstmt.executeUpdate();
					pstmt.close();
					hasil = "3";
				}
				else {
					hasil = "4";
				}
			}
		}
		else {
			hasil = "0";
		}
		closeDBConnection();

	}
	catch(Exception e) {
		e.printStackTrace();
		closeDBConnection();
		hasil = e.getMessage();
	}

	return hasil;

    }
}
